﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android.Services;
using Xamarin.Auth;

[assembly: Dependency(typeof(AuthService))]
namespace Xamarin.Forms.Pria.Android.Services
{
	public class AuthService : IAuth
	{
		#region IAuth implementation

		public Task<Account> LoginFacebook (string appId, string secret, string redirectUrl, string scope)
		{
			TaskCompletionSource<Account> tcs = new TaskCompletionSource<Account> ();
			var auth = new OAuth2Authenticator (
				clientId: appId,
				clientSecret:secret,
				scope: scope,
				authorizeUrl: new Uri ("https://m.facebook.com/dialog/oauth"),
				accessTokenUrl:new Uri ("https://graph.facebook.com/oauth/access_token"),
				redirectUrl: new Uri (redirectUrl));
			
			auth.Completed += (object sender, AuthenticatorCompletedEventArgs e) => {
				if(e.IsAuthenticated)
				{
					AccountStore.Create(Forms.Context).Save(e.Account,"Facebook");
					string token = e.Account.Properties["access_token"];
					tcs.SetResult(new Account(){Site = Site.Facebook, AccessToken = token});
				}
				else
				{
					tcs.SetResult(null);
				}

			};

			auth.Error += (object sender, AuthenticatorErrorEventArgs e) => {
				tcs.SetResult(null);
			};

			Forms.Context.StartActivity (auth.GetUI (Forms.Context));

			return tcs.Task;
		}

		public Task<Account> LoginGooglePlus (string appId, string secret, string redirectUrl, string scope)
		{
			TaskCompletionSource<Account> tcs = new TaskCompletionSource<Account> ();

			var auth = new OAuth2Authenticator (
				appId, 
				secret, 
				scope, 
				new Uri ("https://accounts.google.com/o/oauth2/auth"), 
				new Uri (redirectUrl), 
				new Uri ("https://accounts.google.com/o/oauth2/token")
			);	    

			auth.Completed += (object sender, AuthenticatorCompletedEventArgs e) => {

				if(e.IsAuthenticated)
				{
					AccountStore.Create(Forms.Context).Save(e.Account,"GPlus");
					string token = e.Account.Properties["access_token"];
					tcs.SetResult(new Account(){Site = Site.GPlus, AccessToken = token});
				}
				else
				{
					tcs.SetResult(null);
				}

			};

			auth.Error += (object sender, AuthenticatorErrorEventArgs e) => {
				tcs.SetResult(null);
			};

			Forms.Context.StartActivity (auth.GetUI (Forms.Context));

			return tcs.Task;
		}

		#endregion
	}
}

