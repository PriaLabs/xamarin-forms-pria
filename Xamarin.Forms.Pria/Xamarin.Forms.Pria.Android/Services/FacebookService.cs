﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;
using System.Threading.Tasks;
using System.Threading;
using Android.Content.PM;
using Java.Security;
using Android.Util;

using Xamarin.Facebook;
using Xamarin.Facebook.Share.Model;
using Xamarin.Facebook.Share.Widget;
using Xamarin.Facebook.Login;

[assembly: Dependency(typeof(FacebookService))]
namespace Xamarin.Forms.Pria.Android
{
	public class FacebookService:IFacebookService,IActivityLifecycleSubscriber
	{
		class FacebookStatusCallBack: Java.Lang.Object, IFacebookCallback{
			FacebookService service;

			public FacebookStatusCallBack (FacebookService service)
			{
				this.service = service;
			}

			#region IFacebookCallback implementation

			public void OnCancel ()
			{
				service.RaiseSessionStatusChanged ();
			}

			public void OnError (FacebookException error)
			{
				service.RaiseSessionStatusChanged ();
			}

			public void OnSuccess (Java.Lang.Object result)
			{
				service.RaiseSessionStatusChanged ();
			}

			#endregion
		}
		Xamarin.Facebook.ICallbackManager callbackManager;
		FacebookStatusCallBack callback;

		public FacebookService(){
			try {
				PackageInfo info = Forms.Context.PackageManager.GetPackageInfo(Forms.Context.PackageName,global::Android.Content.PM.PackageInfoFlags.Signatures);
				foreach (global::Android.Content.PM.Signature signature in info.Signatures) {
					MessageDigest md = MessageDigest.GetInstance("SHA");
					md.Update(signature.ToByteArray());
					//Log.LogDebug("KeyHash: " + Base64.EncodeToString(md.Digest(),Base64Flags.Default));
				}

			} catch (global::Android.Content.PM.PackageManager.NameNotFoundException e) {

			} catch (NoSuchAlgorithmException e) {

			}


		}



		#region IFacebookService implementation
		public string AccessToken {
			get {
				
				if (Xamarin.Facebook.AccessToken.CurrentAccessToken != null) {
					return Xamarin.Facebook.AccessToken.CurrentAccessToken.Token;
				}
				return null;
			}
		}
		public void RaiseSessionStatusChanged(){
			if (OnSessionStatusChanged != null) {
				OnSessionStatusChanged (this, EventArgs.Empty);
			}
		}
		public event EventHandler OnSessionStatusChanged;

		public bool IsLogged {
			get {
				return Xamarin.Facebook.Profile.CurrentProfile != null;
			}
		}

		public void Setup (string appId,string appName)
		{
			FacebookSdk.SdkInitialize(Forms.Context);

			CoreActivity ca = Forms.Context as CoreActivity;
			if (ca != null) {
				ca.ActivityLifecycleObserver.RegisterSubscriber (this);
			}
			callbackManager = Xamarin.Facebook.CallbackManagerFactory.Create ();
			callback = new FacebookStatusCallBack (this);
			Xamarin.Facebook.Login.LoginManager.Instance.RegisterCallback (callbackManager, callback);		
		}

		// https://developers.facebook.com/docs/sharing/android
		public void Share (string title, string description, string url, string imageUrl = null)
		{
			ShareLinkContent.Builder builder = (ShareLinkContent.Builder)new ShareLinkContent.Builder ()
				.SetContentTitle (title)
				.SetContentDescription (description)
				.SetContentUrl (global::Android.Net.Uri.Parse (url));

			if (!string.IsNullOrEmpty (imageUrl)) {
				builder.SetImageUrl (global::Android.Net.Uri.Parse (imageUrl));
			}
				
			ShareDialog.Show ((global::Android.App.Activity)Forms.Context, builder.Build ());
		}

		public void Logout ()
		{
			LoginManager.Instance.LogOut ();
		}

		#endregion

		#region IActivityLifecycleSubscriber implementation

		public void OnActivityResult (int requestCode, global::Android.App.Result resultCode, global::Android.Content.Intent data)
		{
			callbackManager.OnActivityResult(requestCode,(int) resultCode, data);
		}

		public void OnCreate (global::Android.OS.Bundle savedInstanceState)
		{
			//uiLifecycleHelper.OnCreate (savedInstanceState);
		}

		public void OnStart ()
		{

		}

		public void OnStop ()
		{
			//uiLifecycleHelper.OnStop ();
		}

		public void OnPause ()
		{
			//uiLifecycleHelper.OnPause ();
			Xamarin.Facebook.AppEvents.AppEventsLogger.DeactivateApp(Forms.Context);
		}

		public void OnResume ()
		{
			//uiLifecycleHelper.OnResume ();
			Xamarin.Facebook.AppEvents.AppEventsLogger.ActivateApp(Forms.Context);
		}

		public void OnDestroy ()
		{
			//uiLifecycleHelper.OnDestroy ();

		}

		public void OnRestart ()
		{
		}

		public void OnRequestPermussionsResult (int requestCode, string[] permissions, global::Android.Content.PM.Permission[] grantResults)
		{
			//throw new NotImplementedException ();
		}

		#endregion
	}
}
