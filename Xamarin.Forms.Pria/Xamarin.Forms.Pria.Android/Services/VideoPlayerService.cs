﻿using System;
using Xamarin.Forms.Pria.Services;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;
using Java.Util.Regex;

[assembly: Dependency(typeof(VideoPlayerService))]
namespace Xamarin.Forms.Pria.Android
{
	public class VideoPlayerService:IVideoPlayer
	{
		public VideoPlayerService ()
		{
		}

		public void Play(Uri uri)
		{
			Intent i = new Intent (Intent.ActionView);
			i.SetDataAndType (global::Android.Net.Uri.Parse (uri.ToString ()), "video/*");
			Forms.Context.StartActivity (i);
		}
	}
}

