﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Services;
using System.IO;
using System.Diagnostics;
using Xamarin.Forms.Pria.Android;


[assembly: Dependency(typeof(IsolatedStorageService))]
namespace Xamarin.Forms.Pria.Android
{
	public class IsolatedStorageService:IIsolatedStorage
	{
		public IsolatedStorageService ()
		{
		}
		string documentsFolder = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);

		public string GetFullPath(string name)
		{
			string strFinalPath = string.Empty;
			string normalizedFirstPath = documentsFolder.TrimEnd (Path.DirectorySeparatorChar);
			string normalizedSecondPath = (name ?? string.Empty).TrimStart (Path.DirectorySeparatorChar);
			strFinalPath =  Path.Combine(normalizedFirstPath, normalizedSecondPath);
			return strFinalPath;
		}

		#region IIsolatedStorage implementation

		public string[] GetNames (string folder)
		{
			return Directory.GetFiles (GetFullPath (folder));
		}

		public Stream Create (string file)
		{
			string dir = Path.GetDirectoryName (GetFullPath(file));
			if (!Directory.Exists (dir)) {
				Directory.CreateDirectory (dir);
			}
			return File.Create (GetFullPath(file));
		}

		public Stream OpenWrite (string file)
		{
			return File.OpenWrite (GetFullPath (file));
		}

		public Stream OpenRead (string file)
		{
			return File.OpenRead (GetFullPath (file));
		}

		public Stream OpenReadFullPath(string path)
		{
			return File.OpenRead (path);
		}

		public bool Exists (string file)
		{
			return File.Exists (GetFullPath (file));
		}

		public bool Delete (string file)
		{
			try{
				File.Delete (GetFullPath (file));
				return true;
			}
			catch(IOException ex){
				Debug.WriteLine (ex.ToString ());
				return false;
			}
		}

		#endregion
	}
}

