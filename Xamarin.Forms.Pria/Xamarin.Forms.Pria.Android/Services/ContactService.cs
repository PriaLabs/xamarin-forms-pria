﻿using System;
using System.Diagnostics;
using Android.Content;
using Android.Util;
using System.IO;
using Android.Webkit;
using Android.Content.PM;
using System.Collections.Generic;
using System.Linq;
using Android.Provider;
using Android.Telephony;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;

[assembly: Dependency (typeof(ContactService))]
namespace Xamarin.Forms.Pria.Android
{
	public class ContactService:IContactService
	{
		Context context;

		public ContactService ()
		{
			this.context = Xamarin.Forms.Forms.Context;
		}

		#region IContactService implementation

		public void CallNumber (string phoneNumber)
		{
			var uri = global::Android.Net.Uri.Parse (string.Format ("tel:{0}", phoneNumber));
			var intent = new Intent (Intent.ActionView, uri); 
			context.StartActivity (intent);  
		}

		public void ComposeSms (string phoneNumber)
		{
			var uri = global::Android.Net.Uri.Parse (string.Format ("sms:{0}", phoneNumber));
			var intent = new Intent (Intent.ActionView, uri); 
			context.StartActivity (intent);
		}

		public void ComposeEmail (string email, string subject, string body)
		{
			try {
				var emailIntent = new Intent (Intent.ActionSend);
				emailIntent.PutExtra (Intent.ExtraEmail, new string[]{email});
				emailIntent.PutExtra (Intent.ExtraSubject, subject);
				emailIntent.PutExtra (Intent.ExtraText, body);
				emailIntent.SetType ("message/rfc822");
				context.StartActivity (emailIntent);
			} catch (Exception ex) {
				Debug.WriteLine (ex.ToString ());
			}
		}

		public bool CanMakePhoneCalls ()
		{
			return context.PackageManager.HasSystemFeature (PackageManager.FeatureTelephony);
		}

		public bool CanSendSMS ()
		{
			return context.PackageManager.HasSystemFeature (PackageManager.FeatureTelephony);
		}

		#endregion
	

	}
}
