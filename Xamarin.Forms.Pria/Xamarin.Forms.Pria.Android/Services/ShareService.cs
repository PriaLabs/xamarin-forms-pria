﻿using System;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms;
using Android.Content;


[assembly: Dependency(typeof(ShareService))]
namespace Xamarin.Forms.Pria.Android
{
	public class ShareService:IShareService
	{
		#region IShareService implementation

		public void Share (string subject, string description, string url)
		{
			Intent share = new Intent (Intent.ActionSend);
			share.SetType("text/plain");
			share.PutExtra (Intent.ExtraTitle, subject);
			share.PutExtra (Intent.ExtraText, string.Format("{0} {1}",description,url));
			Forms.Context.StartActivity(Intent.CreateChooser(share,subject));
		}

		#endregion


	}
}

