﻿using System;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms;
using Android.Content;
using System.Globalization;


[assembly: Dependency (typeof(MapService))]
namespace Xamarin.Forms.Pria.Android
{
	public class MapService:IMapService
	{
		public void ShowMap (string title, double latitude, double longitude, bool requestNavigation)
		{
			try {
				string latInvariant = latitude.ToString (CultureInfo.InvariantCulture);
				string longInvariant = longitude.ToString (CultureInfo.InvariantCulture);

				var geoUri = global::Android.Net.Uri.Parse (string.Format ("geo:{0},{1}?q={2},{3}({4})", latInvariant, longInvariant, latInvariant, longInvariant, global::Android.Net.Uri.Encode (title)));
				var mapIntent = new Intent (Intent.ActionView, geoUri);
				Forms.Context.StartActivity (mapIntent);
			} catch (Exception ex) {
				Log.LogException (ex);
			}
		}

		public void ShowMap (string address)
		{
			var geoUri = global::Android.Net.Uri.Parse (string.Format ("geo:0,0?q={0}", address));
			var mapIntent = new Intent (Intent.ActionView, geoUri);
			Forms.Context.StartActivity (mapIntent);
		}
	}
}

