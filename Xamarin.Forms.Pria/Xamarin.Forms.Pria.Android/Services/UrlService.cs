﻿using System;
using Android.Webkit;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;

[assembly: Dependency(typeof(UrlService))]
namespace Xamarin.Forms.Pria.Android
{
	public class UrlService:IUrlService
	{
		public UrlService ()
		{
		}

		#region IUrlService implementation

		public void OpenUrl (string url)
		{
			if (URLUtil.IsValidUrl (url)) {
				var uri = global::Android.Net.Uri.Parse (url);
				var intent = new Intent (Intent.ActionView, uri); 
				Forms.Context.StartActivity (intent);   
			}
		}

		public void OpenStore(string appId)
		{
			Intent intent = new Intent(Intent.ActionView);
			intent.SetData(global::Android.Net.Uri.Parse(string.Format("market://details?id={0}", Forms.Context.PackageName)));
			Forms.Context.StartActivity(intent);
		}

		#endregion
	}
}

