﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Xamarin.Forms.Pria.Android.Services;

[assembly:UsesPermission (Name = Android.Manifest.Permission.ReceiveBootCompleted)]
namespace Xamarin.Forms.Pria.Android
{
	[BroadcastReceiver]
	[IntentFilter(new[] { global::Android.Content.Intent.ActionBootCompleted } )]
	public class AlarmServiceBootReceiver : BroadcastReceiver
	{
		public AlarmServiceBootReceiver ()
		{
		}

		#region implemented abstract members of BroadcastReceiver

		public override void OnReceive (Context context, Intent intent)
		{
			global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### BootReceiver OnReceive");

			new AlarmService ().RescheduleNotifications (context);
		}

		#endregion
	}
}

