﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;
using Android.Gms.Analytics;

[assembly: Dependency(typeof(GoogleAnalyticsService))]
namespace Xamarin.Forms.Pria.Android
{
	public class GoogleAnalyticsService : IGoogleAnalyticsService
	{
		GoogleAnalytics GAInstance;
		Tracker GATracker;

		public GoogleAnalyticsService ()
		{
		}

		#region IGoogleAnalyticsService implementation

		public void Init (string trackingID, string appName)
		{
			GAInstance = GoogleAnalytics.GetInstance(Forms.Context);
			GAInstance.SetLocalDispatchPeriod(10);
			//GAInstance.SetDryRun (true);

			GATracker = GAInstance.NewTracker(trackingID);
			GATracker.EnableExceptionReporting(true);
			GATracker.EnableAdvertisingIdCollection(true);
			GATracker.EnableAutoActivityTracking(false);

			//GAInstance.Logger.LogLevel = LoggerLogLevel.Verbose;
		}

		public void TrackPage (string pageNameToTrack)
		{
			GATracker.SetScreenName(pageNameToTrack);
			GATracker.Send(new HitBuilders.ScreenViewBuilder().Build());
		}

		public void TrackEvent (string category, string action, string label)
		{
			HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
			builder.SetCategory(category);
			builder.SetAction(action);
			builder.SetLabel(label);

			GATracker.Send(builder.Build());
		}

		public void TrackException (string exceptionMessageToTrack, bool isFatalException)
		{
			HitBuilders.ExceptionBuilder builder = new HitBuilders.ExceptionBuilder();
			builder.SetDescription(exceptionMessageToTrack);
			builder.SetFatal(isFatalException);

			GATracker.Send(builder.Build());
		}

		#endregion
	}
}

