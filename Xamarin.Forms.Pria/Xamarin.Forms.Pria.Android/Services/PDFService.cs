﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;
using Android.Content;
using Java.IO;
using Java.Net;
using System.Diagnostics;
using Android.Content.PM;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Linq;
using Java.Nio.Channels;
using Android.OS;
using Xamarin.Forms.Pria.Utils;


[assembly: Dependency(typeof(PDFService))]
namespace Xamarin.Forms.Pria.Android
{
	public class PDFService : IPDFService
	{
		#region IPDFService implementation

		public void OpenLocalPDF (string path, string title = null)
		{
			File destinationFile = new File(Forms.Context.ExternalCacheDir, string.Format("{0}.pdf", Guid.NewGuid().ToString()));

			CopyFile (new File (path), destinationFile);

			try{
				Intent intent = new Intent();
				intent.SetAction(Intent.ActionView);

				intent.SetDataAndType(global::Android.Net.Uri.FromFile(destinationFile), "application/pdf");
				Forms.Context.StartActivity(intent);
			} 
			catch(ActivityNotFoundException e)
			{
				global::Android.Net.Uri marketUri = global::Android.Net.Uri.Parse("market://details?id=com.adobe.reader");
				Intent marketIntent = new Intent(Intent.ActionView, marketUri);
				Forms.Context.StartActivity(marketIntent);
			}
		}

		public async Task OpenWebPDF (string url, string title = null)
		{
			File destinationFile = new File(Forms.Context.ExternalCacheDir, string.Format("{0}.pdf", Guid.NewGuid().ToString()));

			await DownloadFile (url, destinationFile.AbsolutePath);

			try{
				Intent intent = new Intent();
				intent.SetAction(Intent.ActionView);

				intent.SetDataAndType(global::Android.Net.Uri.FromFile(destinationFile), "application/pdf");
				Forms.Context.StartActivity(intent);
			} 
			catch(ActivityNotFoundException e)
			{
				global::Android.Net.Uri marketUri = global::Android.Net.Uri.Parse("market://details?id=com.adobe.reader");
				Intent marketIntent = new Intent(Intent.ActionView, marketUri);
				Forms.Context.StartActivity(marketIntent);
			}
		}

		#endregion

		private async Task DownloadFile(string fileURL, string destinationFile)
		{
			using (System.IO.Stream f = System.IO.File.OpenWrite(destinationFile)) {
				await new HttpApi ().Call (fileURL, destinationStream: f);
			}
		}

		private void CopyFile(File src, File dst)
		{
			FileInputStream inStream = new FileInputStream(src);
			FileOutputStream outStream = new FileOutputStream(dst);
			FileChannel inChannel = inStream.Channel;
			FileChannel outChannel = outStream.Channel;
			inChannel.TransferTo(0, inChannel.Size(), outChannel);
			inStream.Close();
			outStream.Close();
		}
	}
}

