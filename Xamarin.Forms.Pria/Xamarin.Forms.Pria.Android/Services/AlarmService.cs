﻿using System;
using Xamarin.Forms.Pria.Services;
using Android.App;
using Android.Content;
using Android.OS;
using System.Collections.Generic;
using Xamarin.Forms.Pria.Android.Services;
using Xamarin.Forms;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Android.Util;

[assembly: Dependency(typeof(AlarmService))]
[assembly:UsesPermission (Name = Android.Manifest.Permission.WakeLock)]
namespace Xamarin.Forms.Pria.Android.Services
{
	[BroadcastReceiver]
	public class AlarmService : BroadcastReceiver, IAlarmService
	{
		List<LocalNotification> notifications;

		public AlarmService ()
		{
			
		}

		public override void OnReceive (Context context, Intent intent)
		{
			global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### AlarmService OnReceive 1");

			PowerManager pm = (PowerManager) context.GetSystemService(Context.PowerService);
			PowerManager.WakeLock wl = pm.NewWakeLock(WakeLockFlags.Partial, "");
			wl.Acquire();

			global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### AlarmService OnReceive 2");

			Intent resultIntent = new Intent(context, context.GetType());
			resultIntent.PutExtra ("Notification", true);
			//global::Android.Support.V4.App.TaskStackBuilder stackBuilder = global::Android.Support.V4.App.TaskStackBuilder.Create(context);
			//stackBuilder.AddParentStack(Java.Lang.Class.FromType(context.GetType()));
			//stackBuilder.AddNextIntent(resultIntent);

			global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### AlarmService OnReceive 3");

			IList<string> values = intent.GetStringArrayListExtra ("NotificationStrings");
			//DependencyService.Get<ILocalNotificationService> ().Notify (values [0], values [1], values [2], values[3]);
			new LocalNotificationService(context).Notify (values [0], values [1], values [2], values[3], values[4]);

			global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### AlarmService OnReceive 4");

			wl.Release();		
		}
		PendingIntent pi = null;
		public void SetAlarm(DateTime? dateTime, int requestCode, Context context)
		{
			Intent intent = new Intent (context, typeof(AlarmService));
			intent.SetAction (notifications [requestCode].Id ?? Guid.NewGuid ().ToString ()); // DB: has to be set unless extras won't get propagated
			intent.PutStringArrayListExtra ("NotificationStrings", new List<string> () {
				notifications [requestCode].Title,
				notifications [requestCode].Body,
				notifications [requestCode].AlertAction,
				notifications [requestCode].UserData,
				notifications [requestCode].SoundName,
				notifications [requestCode].Id
			});

			AlarmManager am = (AlarmManager)context.GetSystemService(Context.AlarmService);

			pi = PendingIntent.GetBroadcast(context, 0, intent, 0);

			am.Set (AlarmType.RtcWakeup, (long)(Java.Lang.JavaSystem.CurrentTimeMillis () + (dateTime.Value.ToUniversalTime () - DateTime.Now.ToUniversalTime ()).TotalMilliseconds), pi); // Millisec * Second * Minute
		}

		public void ClearAlarm()
		{
			AlarmManager am = (AlarmManager)Forms.Context.GetSystemService(Context.AlarmService);

			if (pi != null) {
				am.Cancel (pi);
			}
		}

		#region IAlarmService implementation

		public void RegisterNotifications (List<LocalNotification> notifications)
		{
			RegisterNotifications (notifications, Forms.Context);
		}

		public void RegisterNotifications (List<LocalNotification> notifications, Context context)
		{
			this.notifications = notifications;

			PersistNotifications (this.notifications, context);

			int i = 0;
			notifications.ForEach (n => SetAlarm (n.Date, i++, context));
		}

		public void ClearAll ()
		{
			ClearAlarm ();
		}

		private void PersistNotifications(List<LocalNotification> notifications, Context context)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<LocalNotification>));

			string xml = null;

			using(StringWriter textWriter = new StringWriter())
			{
				xmlSerializer.Serialize(textWriter, notifications);
				xml = textWriter.ToString();
			}

			ISharedPreferences prefs = global::Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(context); 
			ISharedPreferencesEditor editor = prefs.Edit();
			editor.PutString(string.Format ("{0}.localnotifications", context.PackageName), xml);
			editor.Apply();
		}

		public void ClearNotification(LocalNotification notification)
		{
			Intent intent = new Intent (Forms.Context, typeof(AlarmService));
			intent.SetAction (notification.Id ?? Guid.NewGuid ().ToString ());
			intent.PutStringArrayListExtra ("NotificationStrings", new List<string>() {
				notification.Title,
				notification.Body,
				notification.AlertAction,
				notification.UserData,
				notification.Id
			});

			PendingIntent pi = PendingIntent.GetBroadcast (Forms.Context, 0, intent, 0);

			AlarmManager am = (AlarmManager)Forms.Context.GetSystemService(Context.AlarmService);
			am.Cancel (pi);
		}

		public void RescheduleNotifications(Context context)
		{
			ISharedPreferences prefs = global::Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(context); 
			string xml = prefs.GetString (string.Format ("{0}.localnotifications", context.PackageName), null);

			global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### RescheduleNotifications deserializing");

			if (!string.IsNullOrEmpty (xml)) {
				XmlSerializer serializer = new XmlSerializer (typeof(List<LocalNotification>));

				List<LocalNotification> result;
				using (TextReader reader = new StringReader (xml)) {
					result = serializer.Deserialize (reader) as List<LocalNotification>;
				}
					
				if (result != null) {
					global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### RescheduleNotifications rescheduling: " + result.FindAll(n => n.Date > DateTime.Now).Count);

					RegisterNotifications (result.FindAll(n => n.Date > DateTime.Now), context);
				}
			} else {
				global::Android.Util.Log.WriteLine (LogPriority.Debug, "BootTest", "### RescheduleNotifications nothing to reschedule");
			}
		}

		#endregion
	}
}
