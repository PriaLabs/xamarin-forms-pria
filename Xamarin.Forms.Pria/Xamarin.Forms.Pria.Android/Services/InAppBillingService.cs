﻿using System;
using Xamarin.InAppBilling;
using Xamarin.InAppBilling.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

[assembly: Dependency(typeof(InAppBillingService))]
namespace Xamarin.Forms.Pria.Android
{
	public class InAppBillingService:IInAppBilling,IActivityLifecycleSubscriber
	{
		private InAppBillingServiceConnection _serviceConnection;

		bool isAvailable = false;

		public InAppBillingService ()
		{
			((CoreActivity)Forms.Context).ActivityLifecycleObserver.RegisterSubscriber (this);
		}

		#region IInAppBilling implementation

		public bool IsAvailable ()
		{
			return isAvailable;
		}

		public bool SupportsManualRestore ()
		{
			return false;
		}

		public Task<List<Xamarin.Forms.Pria.Purchase>> RestorePurchases ()
		{
			throw new NotSupportedException ("Manual restore is not supported on Android!");
		}

		private Task<Purchase> Purchase(Product product, string itemType){

			ManualResetEvent reset = new ManualResetEvent(false);
			Purchase result = null;
			InAppBillingHandler.OnProductPurchasedDelegate onProductPurchased = (int response, Xamarin.InAppBilling.Purchase purchase, string purchaseData, string purchaseSignature) => {
				if(purchase.ProductId == product.ID){
					result = new  Xamarin.Forms.Pria.Purchase(){ID = purchase.ProductId, Token = purchase.PurchaseToken,RecieptData = purchaseData,Signature = purchaseSignature};
					ConsumeProduct(result);
					reset.Set();
				}
			};

			InAppBillingHandler.OnUserCanceledDelegate onUserCancelled= () => {
				reset.Set();
			};

			InAppBillingHandler.OnProductPurchaseErrorDelegate onPurchaseError = (int responseCode, string sku) => {
				if(sku == product.ID){
					reset.Set();
				}
			};

			InAppBillingHandler.OnPurchaseFailedValidationDelegate onValidationError = (purchase, purchaseData, purchaseSignature) => {
				if(purchase.ProductId == product.ID){
					reset.Set();
				}
			};
			InAppBillingHandler.InAppBillingProcessingErrorDelegate onBillingError = (m) => {
				reset.Set();
			};

			return new TaskFactory<Purchase> ().StartNew (() => {
				_serviceConnection.BillingHandler.OnProductPurchased +=onProductPurchased;
				_serviceConnection.BillingHandler.OnProductPurchasedError += onPurchaseError;
				_serviceConnection.BillingHandler.OnPurchaseFailedValidation += onValidationError;
				_serviceConnection.BillingHandler.OnUserCanceled += onUserCancelled;
				_serviceConnection.BillingHandler.InAppBillingProcesingError += onBillingError;

				_serviceConnection.BillingHandler.BuyProduct (product.ID, itemType, null);
				reset.WaitOne();

				_serviceConnection.BillingHandler.OnProductPurchased -=onProductPurchased;
				_serviceConnection.BillingHandler.OnProductPurchasedError -= onPurchaseError;
				_serviceConnection.BillingHandler.OnPurchaseFailedValidation -= onValidationError;
				_serviceConnection.BillingHandler.OnUserCanceled -= onUserCancelled;
				_serviceConnection.BillingHandler.InAppBillingProcesingError -= onBillingError;

				return result;
			});
		}

		public async Task<bool> ConsumeProduct (Purchase purchase)
		{
			return _serviceConnection.BillingHandler.ConsumePurchase (purchase.Token);
		}

		public Task<Purchase> PurchaseProduct (Product product)
		{
			if (!isAvailable) {
				return null;
			}

			return Purchase (product, ItemType.Product);
		}

		public Task<Purchase> PurchaseSubscription (Product product)
		{
			if (!isAvailable) {
				return null;
			}
			return Purchase (product, ItemType.Subscription);
		}

		public async Task<List<Product>> QueryInventoryForSubscriptions (List<string> productIds)
		{
			if (!isAvailable) {
				return null;
			}
			return (await _serviceConnection.BillingHandler.QueryInventoryAsync (productIds, ItemType.Product)).ToList ().ConvertAll (p => new Product () {
				ID = p.ProductId
					, Title = p.Title
					, Description = p.Description
					, Price = p.Price
			});
		}

		public async Task<List<Product>> QueryInventoryForProducts (List<string> productIds)
		{
			if (!isAvailable) {
				return null;
			}
			var items = await _serviceConnection.BillingHandler.QueryInventoryAsync (productIds, ItemType.Product);
			if (items != null) {
				return items.ToList ().ConvertAll (p => new Product () {
					ID = p.ProductId
						, Title = p.Title
						, Description = p.Description
						, Price = p.Price

				});
			}
			return null;
		}

		public async Task<List<Purchase>> GetPurchasedProducts ()
		{
			if (!isAvailable) {
				return null;
			}
			return _serviceConnection.BillingHandler.GetPurchases (ItemType.Product).ToList().ConvertAll(p=>new Purchase(){ID = p.ProductId});
		}
		public async Task<List<Purchase>> GetPurchasedSubscriptions ()
		{
			if (!isAvailable) {
				return null;
			}
			return _serviceConnection.BillingHandler.GetPurchases (ItemType.Subscription).ToList().ConvertAll(p=>new Purchase(){ID = p.ProductId});
		}


		public void Init ()
		{
			_serviceConnection = new InAppBillingServiceConnection (((CoreActivity)Forms.Context), ((CoreActivity)Forms.Context).InAppPublicKey);
			_serviceConnection.OnConnected += () => {
				isAvailable = true;
			};

			_serviceConnection.OnInAppBillingError += (InAppBillingErrorType error, string message) => {
				isAvailable = false;
				Log.LogWarning("InAppBillingService:" + message);
			};
			_serviceConnection.Connect ();
		}

		#endregion

		#region IActivityLifecycleSubscriber implementation

		public void OnActivityResult (int requestCode, global::Android.App.Result resultCode, global::Android.Content.Intent data)
		{
			if (_serviceConnection != null) {
				_serviceConnection.BillingHandler.HandleActivityResult (requestCode, resultCode, data);
			}
		}

		public void OnCreate (global::Android.OS.Bundle savedInstanceState)
		{

		}

		public void OnStart ()
		{

		}

		public void OnStop ()
		{

		}

		public void OnPause ()
		{

		}

		public void OnResume ()
		{

		}

		public void OnDestroy ()
		{
			if (_serviceConnection != null && isAvailable) {
				_serviceConnection.Disconnect ();
			}
		}

		public void OnRestart ()
		{

		}
			
		public void OnRequestPermussionsResult (int requestCode, string[] permissions, global::Android.Content.PM.Permission[] grantResults)
		{
		}

		#endregion
	}
}

