﻿using System;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Java.Lang;
using Android.App;
using Android.Media;
using Xamarin.Forms.Pria.Services;


[assembly: Dependency(typeof(LocalNotificationService))]

namespace Xamarin.Forms.Pria.Android
{
	public class LocalNotificationService : ILocalNotificationService
	{
		Context context;

		public LocalNotificationService (Context context)
		{
			this.context = context;
		}

		public LocalNotificationService ()
		{
			this.context = Forms.Context;
		}

		public void RequestLocalNotifications ()
		{
			// iOS only
		}

		public void RequestRemoteNotifications ()
		{
			// iOS only
		}

		public void Notify(string title,string text,string action,string userData = null,string soundName = null){

			// Create the PendingIntent with the back stack, when the user clicks the notification, activity will start up.
			Intent resultIntent = context.PackageManager.GetLaunchIntentForPackage (context.ApplicationInfo.PackageName);
			resultIntent.PutExtra ("NotificationIntent", true);
			if (userData != null) {
				resultIntent.PutExtra ("UserData", userData);
			}

			var stackBuilder = global::Android.Support.V4.App.TaskStackBuilder.Create(context);
			stackBuilder.AddParentStack(resultIntent.Component);
			stackBuilder.AddNextIntent(resultIntent);

			PendingIntent resultPendingIntent = stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

			var soundUri = RingtoneManager.GetDefaultUri (RingtoneType.Notification);
			if (!string.IsNullOrEmpty (soundName)) {
				soundUri = AndroidFileUtils.GetRawResourceUri (soundName);
			}

			NotificationCompat.Builder b = new NotificationCompat.Builder (context)
				.SetTicker (title)
				.SetAutoCancel(true)
				.SetSmallIcon (context.ApplicationInfo.Icon)
				.SetContentTitle (title)
				.SetContentText(text)
				.SetSound(soundUri)
				.SetContentIntent(resultPendingIntent);

			NotificationManager nm = (NotificationManager)context.GetSystemService(Context.NotificationService);

			nm.Notify (0, b.Build());
		}
	}
}

