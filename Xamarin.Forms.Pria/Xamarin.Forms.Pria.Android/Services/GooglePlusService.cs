﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Android;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Gms.Plus;
using Android.Widget;
using System.Threading.Tasks;
using Android.OS;
using Android.Gms.Auth;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;
using Android.Support.V4.App;


//TODO: Refactor this entire service sometime in the future to make it work with new dependencies, currently I didn't need them and didn't have time to bother. Sorry!
[assembly: Dependency(typeof(GooglePlusService))]
namespace Xamarin.Forms.Pria.Android
{
    public class GooglePlusService : Java.Lang.Object//, IGooglePlusService, IActivityLifecycleSubscriber, GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
    { }
	//	GoogleApiClient apiClient;
	//	bool shouldPromptForResolve = true; // prevents multiple dialogs
	//	const int GetAccountsRequestCode = 100;

	//	public void OnConnected (global::Android.OS.Bundle connectionHint)
	//	{
	//		//Toast.MakeText (Forms.Context, "Connected!", ToastLength.Long).Show ();

	//		ResolveAccessToken ();
	//	}

	//	async void ResolveAccessToken()
	//	{
	//		await Task.Factory.StartNew (() => {
				
	//			if (ContextCompat.CheckSelfPermission (Forms.Context, Manifest.Permission.GetAccounts) == Permission.Denied)
	//			{
	//				ActivityCompat.RequestPermissions ((global::Android.App.Activity)Forms.Context, new []{ Manifest.Permission.GetAccounts }, GetAccountsRequestCode); 
	//			} 
	//			else
	//			{
	//				mAccessToken = GoogleAuthUtil.GetToken (Forms.Context, PlusClass.AccountApi.GetAccountName (apiClient), "oauth2:" + PlusClass.ScopePlusLogin);
	//				//Console.WriteLine(mAccessToken);
	//				OnLoginStatusChanged?.Invoke (this, EventArgs.Empty);
	//			}
	//		});
	//	}

	//	public void OnConnectionSuspended (int cause)
	//	{
	//		//Toast.MakeText (Forms.Context, "OnConnectionSuspended!", ToastLength.Long).Show ();
	//	}

	//	public void OnConnectionFailed (ConnectionResult result)
	//	{
	//		//Toast.MakeText (Forms.Context, "OnConnectionFailed!", ToastLength.Long).Show ();

	//		if(result.HasResolution && shouldPromptForResolve){
	//			shouldPromptForResolve = false;
	//			result.StartResolutionForResult ((global::Android.App.Activity)Forms.Context, 0);
	//		}
	//	}

	//	#region IGooglePlusService implementation

	//	public event EventHandler OnLoginStatusChanged;

	//	public void SignIn ()
	//	{
	//		shouldPromptForResolve = true;

	//		if (!apiClient.IsConnected) {
	//			apiClient?.Connect ();
	//		} else {
	//			ResolveAccessToken();
	//		}
	//	}

	//	/// <summary>
	//	/// Add to manifest: INTERNET, USE_CREDENTIALS, GET_ACCOUNTS
	//	/// </summary>
	//	/// <param name="clientId">Client identifier.</param>
	//	public void Setup (string clientId)
	//	{
	//		GoogleApiClient.Builder builder = new GoogleApiClient.Builder(Forms.Context)
	//			.AddConnectionCallbacks (this)
	//			.AddOnConnectionFailedListener(this)
	//			.AddApi (PlusClass.API)
	//			.AddScope(PlusClass.ScopePlusLogin)
	//			.AddScope (new Scope ("https://www.googleapis.com/auth/userinfo.email"))
	//			.AddScope(PlusClass.ScopePlusProfile);

	//		apiClient = builder.Build ();

	//		CoreActivity ca = Forms.Context as CoreActivity;
	//		if (ca != null) {
	//			ca.ActivityLifecycleObserver.RegisterSubscriber (this);
	//		}
	//	}

	//	public bool IsLogged {
	//		get {
	//			return apiClient.IsConnected;
	//		}
	//	}

	//	private string mAccessToken;

	//	public string AccessToken {
	//		get {
	//			return apiClient.IsConnected ? mAccessToken : null;
	//		}
	//	}

	//	#endregion

	//	#region IActivityLifecycleSubscriber implementation

	//	public void OnActivityResult (int requestCode, global::Android.App.Result resultCode, global::Android.Content.Intent data)
	//	{
	//		if (requestCode == GetAccountsRequestCode || requestCode == 0)
	//		{
	//			// trigged OnConnected to be called
	//			if (!apiClient.IsConnected)
	//			{
	//				apiClient.Connect ();
	//			}
	//		}
	//	}


	//	public void OnRequestPermussionsResult (int requestCode, string[] permissions, Permission[] grantResults)
	//	{
	//		if (requestCode == GetAccountsRequestCode)
	//		{
	//			if (grantResults [0] == Permission.Granted)
	//			{
	//				ResolveAccessToken ();
	//			}
	//		}
	//	}

	//	public void OnCreate (global::Android.OS.Bundle savedInstanceState)
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	public void OnStart ()
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	public void OnStop ()
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	public void OnPause ()
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	public void OnResume ()
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	public void OnDestroy ()
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	public void OnRestart ()
	//	{
	//		//throw new NotImplementedException ();
	//	}

	//	#endregion
	//}
}
	