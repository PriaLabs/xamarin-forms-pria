﻿using System;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using Java.Security;
using Java.IO;
using Android.Content;
using Javax.Crypto;
using Android.App.Backup;
using Android.Util;

[assembly: Dependency(typeof(GlobalPersistor))]
namespace Xamarin.Forms.Pria.Android
{
	public class GlobalPersistor : BackupAgentHelper,IGlobalPersistor
	{
		public const string GLOBAL_PREFS = "global_prefs";
		public const string GLOBAL_PREFS_BACKUP = "global_prefs_backup";

		public GlobalPersistor ()
		{
		}
		public override void OnCreate ()
		{
			base.OnCreate ();

			SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper (this, GLOBAL_PREFS);
			AddHelper (GLOBAL_PREFS_BACKUP, helper);

		}
		#region IGlobalPersistor implementation


		public string Load (string key)
		{
			var prefs = Forms.Context.GetSharedPreferences (GLOBAL_PREFS, FileCreationMode.Private);
			string value = prefs.GetString (key, null);
			return value;
		}

		public void Save (string key, string value)
		{
			var prefs = Forms.Context.GetSharedPreferences (GLOBAL_PREFS, FileCreationMode.Private);
			var editor = prefs.Edit ();
			editor.PutString (key, value);
			editor.Commit ();

			//request backup
			BackupManager bm = new BackupManager (Forms.Context);
			bm.DataChanged ();
		}

		#endregion
	}
}

