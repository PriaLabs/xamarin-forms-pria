﻿using System;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms;
using System.IO;
using Android.Graphics;
using Android.Database;

[assembly: Dependency(typeof(ImageResizeService))]
namespace Xamarin.Forms.Pria.Android
{
	public class ImageResizeService : IImageResizeService
	{
		#region IImageResizeService implementation

		public Stream ResizeImage (MediaFile mediaFile, int maxPixelDimension)
		{
			byte[] imgData = StreamUtils.GetDataFromStream (mediaFile.Source);

			Bitmap originalBitmap = BitmapFactory.DecodeByteArray (imgData, 0, imgData.Length);

			int newWidth, newHeight;
			MediaUtils.ResizeBasedOnPixelDimension (maxPixelDimension, originalBitmap.Width, originalBitmap.Height, out newWidth, out newHeight);

			MemoryStream s = new MemoryStream ();

			//Bitmap.CreateScaledBitmap (originalBitmap, newWidth, newHeight, true).Compress (Bitmap.CompressFormat.Jpeg, 100, s);
			CreateRotatedBitmap (originalBitmap, newWidth*newHeight, mediaFile.Path).Compress(Bitmap.CompressFormat.Jpeg, 100, s);

			s.Seek (0, SeekOrigin.Begin);

			return s;

			//throw new NotImplementedException ();
		}

		#endregion

		private Bitmap CreateRotatedBitmap(Bitmap originalBitmap,int maxArea,string filePath)
		{
			global::Android.Media.ExifInterface exif = new global::Android.Media.ExifInterface(filePath);
			int orientation = exif.GetAttributeInt (global::Android.Media.ExifInterface.TagOrientation, 1);

			Matrix matrix = new Matrix();
			bool switchProportions = false;
			if (orientation == 6) {
				matrix.PostRotate (90);
				switchProportions = true;
			}
			else if (orientation == 3) {
				matrix.PostRotate(180);
			}
			else if (orientation == 8) {
				matrix.PostRotate(270);
				switchProportions = true;
			}

			BitmapFactory.Options o = new BitmapFactory.Options ();
			o.InJustDecodeBounds = true;

			Bitmap bitmap = BitmapFactory.DecodeFile (filePath,o);

			int area = o.OutWidth * o.OutHeight;
			int sampleSize = Math.Max (1, (int)Math.Sqrt (area / maxArea));
			o.InSampleSize = sampleSize;
			o.InJustDecodeBounds = false;

			int originalW = o.OutWidth;
			int originalH = o.OutHeight;

			bitmap = BitmapFactory.DecodeFile (filePath,o);
			Bitmap rotatedBitmap = Bitmap.CreateBitmap (bitmap, 0, 0, bitmap.Width, bitmap.Height, matrix, true);

			int w = originalW / sampleSize;
			int h = originalH / sampleSize;
			return  Bitmap.CreateScaledBitmap(rotatedBitmap, switchProportions ? h : w, switchProportions ? w : h, true);
		}
	}
}

