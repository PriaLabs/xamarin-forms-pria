﻿using System;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer (typeof (BarcodeView), typeof (BarcodeRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class BarcodeRenderer:ViewRenderer<BarcodeView,global::Android.Widget.ImageView>
	{
		public BarcodeRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<BarcodeView> e)
		{
			if (Control == null) {
				SetNativeControl (new global::Android.Widget.ImageView(Context));

				Control.SetScaleType (global::Android.Widget.ImageView.ScaleType.FitCenter);
			}

			base.OnElementChanged (e);
			if (e.OldElement == null) {
				UpdateBarcode ();
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (e.PropertyName == BarcodeView.BarcodeProperty.PropertyName || e.PropertyName == BarcodeView.BarcodeTypeProperty.PropertyName) {
				UpdateBarcode ();
			}
		}

		private void UpdateBarcode(){
			Control.SetImageBitmap(null);
			if (!string.IsNullOrEmpty (Element.Barcode)) {
				try{
					ZXing.Mobile.BarcodeWriter writer = new ZXing.Mobile.BarcodeWriter ();
					writer.Format = (ZXing.BarcodeFormat)Element.BarcodeType;
					writer.Options.Width = 800;
					writer.Options.Height = 600;
					Control.SetImageBitmap(writer.Write(Element.Barcode));
				}catch(Exception ex){
					Log.LogException (ex);
				}
			}				
		}
	}
}

