﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Views;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Support.V4.Graphics.Drawable;
using Android.Content.Res;
using Android.Graphics;
using Xamarin.Forms.Pria.Android;

[assembly:ExportRenderer (typeof(SegmentedControl), typeof(SegmentedControlRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class SegmentedControlRenderer : ViewRenderer<SegmentedControl, LinearLayout>
	{
		protected override void OnElementChanged (ElementChangedEventArgs<SegmentedControl> e)
		{
			base.OnElementChanged (e);

			LinearLayout group = new LinearLayout (Context);
			group.Orientation = global::Android.Widget.Orientation.Horizontal;
			group.LayoutParameters = new LayoutParams (LayoutParams.WrapContent, LayoutParams.WrapContent);

			Drawable background = ContextCompat.GetDrawable (Context, Resource.Drawable.SegmentControlBackground);
			background.SetColorFilter (Element.TintColor.ToAndroid (), PorterDuff.Mode.SrcAtop);
			group.SetBackground (background);

			int count = e.NewElement.Children.Count;
			for (var i = 0; i < count; i++)
			{
				ToggleButton b = new ToggleButton (Context);
				b.Text = b.TextOn = b.TextOff = e.NewElement.Children [i].Text;

				b.LayoutParameters = new TableLayout.LayoutParams (LayoutParams.WrapContent, LayoutParams.WrapContent, 1.0f);
				b.TransformationMethod = null; //disable all caps
				b.Typeface = Typeface.Default;
				b.Enabled = e.NewElement.IsEnabled;

				int index = i;
				b.CheckedChange += (sender, args) => {
					if (args.IsChecked) {
						e.NewElement.SelectedIndex = index;
						Toggle (group, index);
					} else if (e.NewElement.SelectedIndex == index) {
						b.Checked = true;
					}
				};

				StateListDrawable states = new StateListDrawable ();
				Drawable stateOn;
				Drawable stateOff = new ColorDrawable (global::Android.Graphics.Color.Transparent);

				if (i == 0) {
					stateOn = ContextCompat.GetDrawable (Context, Resource.Drawable.SegmentControlItemLeft);
				} else if (i == count - 1) {
					stateOn = ContextCompat.GetDrawable (Context, Resource.Drawable.SegmentControlItemRight);
				} else {
					stateOn = new ColorDrawable (Element.TintColor.ToAndroid ());
				}

				states.AddState (new int[]{ global::Android.Resource.Attribute.StatePressed }, stateOn);
				states.AddState (new int[]{ global::Android.Resource.Attribute.StateChecked }, stateOn);
				states.AddState (new int[]{ }, stateOff);

				b.SetBackground (states);

				ColorStateList textStates = new ColorStateList (new int[][] {
					new int[] { global::Android.Resource.Attribute.StatePressed },
					new int[]{ global::Android.Resource.Attribute.StateChecked },
					new int[]{ }
				}, new int[] {
					Element.TextColor.ToAndroid(),
					Element.TextColor.ToAndroid(),
					Element.TintColor.ToAndroid ()
				});

				b.SetTextColor (textStates);

				group.AddView (b);

				if (i < count - 1) {
					int widht = (int)Resources.GetDimension (Resource.Dimension.segment_separator_size);
					global::Android.Views.View separator = new global::Android.Views.View (Context);
					separator.LayoutParameters = new LayoutParams (widht, LayoutParams.MatchParent);
					separator.SetBackgroundColor (Element.TintColor.ToAndroid ());
					group.AddView (separator);
				}
			}

			Toggle (group, Element.SelectedIndex);

			SetNativeControl (group);
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == VisualElement.IsEnabledProperty.PropertyName) {
				LinearLayout rg = (LinearLayout)GetChildAt (0);
				for (int i = 0; i < rg.ChildCount; i++) {
					rg.GetChildAt (i).Enabled = Element.IsEnabled;
				}
			}
		}

		void Toggle (ViewGroup viewGroup, int checkedIndex)
		{
			int count = viewGroup.ChildCount;
			int index = 0;
			for (int i = 0; i < count; i++) {
				global::Android.Views.View view = viewGroup.GetChildAt (i);
				if (view is ToggleButton)
				{
					ToggleButton button = (ToggleButton)view;
					bool isChecked = checkedIndex == index++;

					button.Checked = isChecked;

					if (isChecked)
					{
						button.Background.SetColorFilter (Element.TintColor.ToAndroid (), PorterDuff.Mode.SrcAtop);

					}
				}
			}
		}
	}
}

