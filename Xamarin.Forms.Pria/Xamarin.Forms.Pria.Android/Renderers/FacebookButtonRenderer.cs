﻿using System;
using Android.Content;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;


[assembly: ExportRenderer (typeof(FacebookLoginButton), typeof(FacebookButtonRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class FacebookButtonRenderer:ViewRenderer<FacebookLoginButton,Xamarin.Facebook.Login.Widget.LoginButton>
	{
		public FacebookButtonRenderer ()
		{
		}
		protected override void OnElementChanged (ElementChangedEventArgs<FacebookLoginButton> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {
				Xamarin.Facebook.Login.Widget.LoginButton loginButton = new Xamarin.Facebook.Login.Widget.LoginButton (Context);
				loginButton.SetReadPermissions (Element.ReadPermissions ?? new string[0]);
				//loginButton.SetPublishPermissions (Element.PublishPermissions ?? new string[0]);
				loginButton.SetBackgroundResource(global::Xamarin.Forms.Pria.Android.Resource.Drawable.com_facebook_button_background);
				loginButton.SetCompoundDrawablesWithIntrinsicBounds(global::Xamarin.Forms.Pria.Android.Resource.Drawable.com_facebook_button_icon, 0, 0, 0);
				loginButton.CompoundDrawablePadding = Context.Resources.GetDimensionPixelSize(global::Xamarin.Forms.Pria.Android.Resource.Dimension.com_facebook_share_button_compound_drawable_padding);
				loginButton.SetTextColor (global::Android.Graphics.Color.White);

				loginButton.SetPadding(
					Context.Resources.GetDimensionPixelSize(global::Xamarin.Forms.Pria.Android.Resource.Dimension.com_facebook_share_button_padding_left),
					Context.Resources.GetDimensionPixelSize(global::Xamarin.Forms.Pria.Android.Resource.Dimension.com_facebook_share_button_padding_top),
					Context.Resources.GetDimensionPixelSize(global::Xamarin.Forms.Pria.Android.Resource.Dimension.com_facebook_share_button_padding_right),
					Context.Resources.GetDimensionPixelSize(global::Xamarin.Forms.Pria.Android.Resource.Dimension.com_facebook_share_button_padding_bottom)
				);

				SetNativeControl (loginButton);
			}
		}
	}
}
