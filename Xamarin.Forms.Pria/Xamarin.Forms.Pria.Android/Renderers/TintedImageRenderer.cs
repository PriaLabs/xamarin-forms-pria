﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;


[assembly: ExportRenderer (typeof (TintedImage), typeof (TintedImageRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class TintedImageRenderer:ImageRenderer
	{
		
		public TintedImageRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);

			UpdateTintColor ();
		}
		private void UpdateTintColor(){
			if (((TintedImage)Element).TintColor != null && Element!=null) {
				Control.SetColorFilter (((TintedImage)Element).TintColor.ToAndroid (), global::Android.Graphics.PorterDuff.Mode.Multiply);
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == TintedImage.TintColorProperty.PropertyName) {
				UpdateTintColor ();
			}
		}
	}
}

