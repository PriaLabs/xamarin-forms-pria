﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Views;
using Android.Graphics;
using Android.Content;
using Android.Util;
using Android.Graphics.Drawables;

//[assembly:ExportRenderer (typeof(SegmentedControl), typeof(SegmentedControlRadioRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class SegmentedControlRadioRenderer : ViewRenderer<SegmentedControl, RadioGroup>
	{
		protected override void OnElementChanged (ElementChangedEventArgs<SegmentedControl> e)
		{
			base.OnElementChanged (e);

			// generate drawables for styling: http://android-holo-colors.com

			var g = new RadioGroup (Context);
			g.Orientation = Orientation.Vertical;
			g.CheckedChange += (sender, eventArgs) => {
				var rg = (RadioGroup)sender;
				if (rg.CheckedRadioButtonId != -1) {
					var id = rg.CheckedRadioButtonId;
					var radioButton = rg.FindViewById (id);
					var radioId = rg.IndexOfChild (radioButton);
					e.NewElement.SelectedIndex = radioId;
				}
			};

			for (var i = 0; i < e.NewElement.Children.Count; i++) {
				RadioButton rb = new RadioButton (Context);
				rb.Text = e.NewElement.Children [i].Text;
				rb.Enabled = e.NewElement.IsEnabled;
				g.AddView (rb);

				/*rb.ButtonTintList = new global::Android.Content.Res.ColorStateList (
					new int[][]{ 
						new int[]{ global::Android.Resource.Attribute.StatePressed}, 
						new int[]{} 
					}, 
					new int[]{ 
						global::Android.Graphics.Color.Red,
						global::Android.Graphics.Color.Blue,
					});*/
				
				//rb.ButtonTintList = global::Android.Content.Res.ColorStateList.ValueOf (global::Android.Graphics.Color.Red);
				//ThemeHelper.ColorizeRadioButton(rb, Color.Red);
			}

			g.Check (g.GetChildAt(Element.SelectedIndex).Id);

			SetNativeControl (g);
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == SegmentedControl.IsEnabledProperty.PropertyName) {
				RadioGroup rg = (RadioGroup)GetChildAt (0);
				for (int i = 0; i < rg.ChildCount; i++) {
					rg.GetChildAt (i).Enabled = Element.IsEnabled;
				}
			}
		}
	}

	/*
	public class CustomRadioButton : RadioButton
	{
		public CustomRadioButton (Context context) : base (context)
		{
		}

		protected override bool VerifyDrawable (Drawable who)
		{
			who.SetColorFilter (global::Android.Graphics.Color.Red, PorterDuff.Mode.Multiply);

			return base.VerifyDrawable (who);;
		}
	}
	*/
}

