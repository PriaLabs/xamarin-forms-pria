﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables.Shapes;
using System.Linq;
using Android.Graphics.Drawables;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;

[assembly: ExportRenderer (typeof (RoundedContentView), typeof (RoundedContentRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class RoundedContentRenderer : ViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null)
			{
				SetStyle ();
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == VisualElement.BackgroundColorProperty.PropertyName)
			{
				SetStyle ();
			}
		}

		void SetStyle ()
		{
			RoundRectShape rect = new RoundRectShape (Enumerable.Repeat (((RoundedContentView)Element).Radius * Context.Resources.DisplayMetrics.Density, 8).ToArray (), null, null);
			ShapeDrawable shape = new ShapeDrawable (rect);
			shape.Paint.Color = Element.BackgroundColor.ToAndroid ();
			if (ViewGroup != null)
			{
				ViewGroup.Background = shape;
			}
		}
	}
}

