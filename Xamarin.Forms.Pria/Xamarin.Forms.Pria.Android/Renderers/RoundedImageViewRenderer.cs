﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Text;
using Android.Text.Style;
using Xamarin.Forms.Pria;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.Graphics;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Util;
using Xamarin.Forms.Pria.Android;
using System.Linq;

[assembly: ExportRenderer (typeof (RoundedImageView), typeof (RoundedImageViewRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class RoundedImageViewRenderer : ImageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			//Log.LogDebug ("######## TEST ##########");

			base.OnElementChanged (e);

			if (e.OldElement == null)
			{
				if ((int)global::Android.OS.Build.VERSION.SdkInt < 18) {
					SetLayerType(global::Android.Views.LayerType.Software, null);
				}
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			//if (e.PropertyName == Image.SourceProperty.PropertyName) {
				Invalidate ();
			//}

		}
		protected override bool DrawChild (Canvas canvas, global::Android.Views.View child, long drawingTime)
		{
			try{
				Path path = new Path ();
				path.AddRoundRect(new RectF(0, 0, Width, Height), Enumerable.Repeat(((RoundedImageView)Element).Radius, 8).ToArray(), Path.Direction.Ccw);
				canvas.Save ();
				canvas.ClipPath (path);

				var result = base.DrawChild (canvas, child, drawingTime);

				canvas.Restore ();
				canvas.DrawPath (path, new Paint(){Color = global::Android.Graphics.Color.Transparent});

				path.Dispose();

				return result;

			}catch(Exception ex) {
				Log.LogException (ex);
			}

			return base.DrawChild (canvas, child, drawingTime);
		}

		/*
		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			//if (e.PropertyName == Image.SourceProperty.PropertyName || e.PropertyName == Image.IsLoadingProperty.PropertyName && !this.Element.IsLoading && this.Control.Drawable != null)
			if(this.Control.Drawable != null)
			{
				//Should only be true right after an image is loaded
				if (this.Element.Aspect != Aspect.AspectFit)
				{
					if (!(this.Control.Drawable.IntrinsicWidth > 0 && this.Control.Drawable.IntrinsicHeight > 0)) {
						return;
					}
					using (var sourceBitmap = Bitmap.CreateBitmap(this.Control.Drawable.IntrinsicWidth/2, this.Control.Drawable.IntrinsicHeight/2, Bitmap.Config.Argb8888))
					{
						Canvas canvas = new Canvas(sourceBitmap);
						this.Control.Drawable.SetBounds(0, 0, canvas.Width, canvas.Height);
						this.Control.Drawable.Draw(canvas);
						this.ReshapeImage(sourceBitmap);
					}
				}
			}
		}

		protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
		{
			if (this.Element.Aspect == Aspect.AspectFit)
			{
				var radius = Math.Min(Width, Height)/2;
				var strokeWidth = 10;
				radius -= strokeWidth/2;

				Path path = new Path();
				path.AddCircle(Width/2, Height/2, radius, Path.Direction.Ccw);
				canvas.Save();
				canvas.ClipPath(path);

				var result = base.DrawChild(canvas, child, drawingTime);

				path.Dispose();

				return result;

			}

			return base.DrawChild(canvas, child, drawingTime);
		}

		/// <summary>
		/// Reshapes the image.
		/// </summary>
		/// <param name="sourceBitmap">The source bitmap.</param>
		private void ReshapeImage(Bitmap sourceBitmap)
		{
			//this.Control.SetImageBitmap(sourceBitmap);
			//return;

			if (sourceBitmap != null)
			{
				var sourceRect = GetScaledRect(sourceBitmap.Height, sourceBitmap.Width);
				var rect = this.GetTargetRect(sourceBitmap.Height, sourceBitmap.Width);
				if (!(rect.Width () > 0 && rect.Height () > 0)) {
					return;
				}
				using (var output = Bitmap.CreateBitmap(rect.Width(), rect.Height(), Bitmap.Config.Argb8888))
				{
					var canvas = new Canvas(output);

					var paint = new Paint();
					var rectF = new RectF(rect);
					var roundRx = ToPixels (Context, ((RoundedImageView)Element).Radius);
					var roundRy = ToPixels (Context, ((RoundedImageView)Element).Radius);

					paint.AntiAlias = true;
					canvas.DrawARGB(0, 0, 0, 0);
					paint.Color = global::Android.Graphics.Color.ParseColor("#ff424242");
					canvas.DrawRoundRect(rectF, roundRx, roundRy, paint);

					paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.SrcIn));
					canvas.DrawBitmap(sourceBitmap, sourceRect, rect, paint);

					//this.DrawBorder(canvas, rect.Width(), rect.Height());

					this.Control.SetImageBitmap(output);
					// Forces the internal method of InvalidateMeasure to be called.
					this.Element.WidthRequest = this.Element.WidthRequest;
				}
			}
		}

		/// <summary>
		/// Gets the scaled rect.
		/// </summary>
		/// <param name="sourceHeight">Height of the source.</param>
		/// <param name="sourceWidth">Width of the source.</param>
		/// <returns>Rect.</returns>
		/// <exception cref="System.NotImplementedException"></exception>
		private Rect GetScaledRect(int sourceHeight, int sourceWidth)
		{
			int height = 0;
			int width = 0;
			int top = 0;
			int left = 0;

			switch (this.Element.Aspect)
			{
			case Aspect.AspectFill:
				height = sourceHeight;
				width = sourceWidth;
				height = this.MakeSquare(height, ref width);
				left = (int)((sourceWidth - width) / 2);
				top = (int)((sourceHeight - height) / 2);
				break;
			case Aspect.Fill:
				height = sourceHeight;
				width = sourceWidth;
				break;
			case Aspect.AspectFit:
				height = sourceHeight;
				width = sourceWidth;
				height = this.MakeSquare(height, ref width);
				left = (int)((sourceWidth - width) / 2);
				top = (int)((sourceHeight - height) / 2);
				break;
			default:
				throw new NotImplementedException();
			}

			var rect = new Rect(left, top, width + left, height + top);

			return rect;
		}

		/// <summary>
		/// Makes the square.
		/// </summary>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <returns>System.Int32.</returns>
		private int MakeSquare(int height, ref int width)
		{
			if (height < width)
			{
				width = height;
			}
			else
			{
				height = width;
			}
			return height;
		}

		/// <summary>
		/// Gets the target rect.
		/// </summary>
		/// <param name="sourceHeight">Height of the source.</param>
		/// <param name="sourceWidth">Width of the source.</param>
		/// <returns>Rect.</returns>
		private Rect GetTargetRect(int sourceHeight, int sourceWidth)
		{
			int height = 0;
			int width = 0;

			height = this.Element.HeightRequest > 0
				? (int)System.Math.Round(this.Element.HeightRequest, 0)
				: sourceHeight; 
			width = this.Element.WidthRequest > 0
				? (int)System.Math.Round(this.Element.WidthRequest, 0)
				: sourceWidth; 

			// Make Square
			height = MakeSquare(height, ref width);

			return new Rect(0, 0, ToPixels(Context, width), ToPixels(Context, height));
		}

		private int ToPixels (Context ctx, float dp)
		{
			DisplayMetrics displayMetrics = new DisplayMetrics ();

			var wm = ctx.GetSystemService (Context.WindowService).JavaCast<IWindowManager> ();
			wm.DefaultDisplay.GetMetrics (displayMetrics);

			var density = displayMetrics.Density;
			return (int)(dp * density + 0.5f);
		}*/
	}
}

