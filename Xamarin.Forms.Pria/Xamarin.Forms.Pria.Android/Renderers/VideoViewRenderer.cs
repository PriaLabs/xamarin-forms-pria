﻿using System;
using Xamarin.Forms.Platform.Android;

using Xamarin.Forms;
using Android.Views;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms.Pria.Views;



[assembly: ExportRenderer (typeof(VideoView), typeof(VideoViewRenderer))]

namespace Xamarin.Forms.Pria.Android
{
	public class VideoViewRenderer : ViewRenderer<VideoView, global::Android.Widget.RelativeLayout>,global::Android.Media.MediaPlayer.IOnPreparedListener,global::Android.Media.MediaPlayer.IOnVideoSizeChangedListener,ISurfaceHolderCallback
	{

		ISurfaceHolder holder;
		global::Android.Widget.RelativeLayout rl;
		SurfaceView sv;
		global::Android.Media.MediaPlayer mp;

		protected override void OnElementChanged (ElementChangedEventArgs<VideoView> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) {
				rl = new global::Android.Widget.RelativeLayout (this.Context);
				rl.SetGravity(GravityFlags.Center);

				sv = new SurfaceView (this.Context);
				holder = sv.Holder;
				holder.AddCallback (this);

				mp = new global::Android.Media.MediaPlayer ();
				mp.SetOnPreparedListener (this);
				mp.SetOnVideoSizeChangedListener (this);

				rl.AddView (sv);
				SetNativeControl (rl);
			}
		}

		public void OnPrepared (global::Android.Media.MediaPlayer mp)
		{
			if (mp != null) {
				mp.Start ();
			}
		}

		public void SurfaceChanged (ISurfaceHolder holder, global::Android.Graphics.Format format, int width, int height)
		{

		}

		public void SurfaceCreated (ISurfaceHolder holder)
		{
			mp.Looping = Element.Repeat;
			mp.SetDataSource (Context, global::Android.Net.Uri.Parse (this.Element.Source.ToString ()));
			mp.SetDisplay (holder);

			mp.PrepareAsync ();
		}

		public void SurfaceDestroyed (ISurfaceHolder holder)
		{
			mp.Stop (); 
			mp.Reset (); 
		}

		public void OnVideoSizeChanged (global::Android.Media.MediaPlayer mp, int width, int height)
		{
			setFitToFillAspectRatio (width, height);
		}

		private void setFitToFillAspectRatio (int videoWidth, int videoHeight)
		{	
			if (videoWidth == 0 || videoHeight == 0) {
				return;
			}
			int screenWidth = rl.Width;
			int screenHeight = rl.Height;
			global::Android.Views.ViewGroup.LayoutParams videoParams = sv.LayoutParameters;


			if (videoWidth > videoHeight) {
				videoParams.Width = screenWidth;
				videoParams.Height = screenWidth * videoHeight / videoWidth;
			} else {
				videoParams.Width = screenHeight * videoWidth / videoHeight;
				videoParams.Height = screenHeight;
			}

			sv.LayoutParameters = videoParams;
		}
	
	}
}

