﻿using System;
using Xamarin.Forms;
using Android.Animation;
using Android.Views;
using Android.Util;
using Android.Graphics;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof(ZoomableScrollView), typeof(ZoomableScrollViewRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class CustomGestureDetector : GestureDetector.SimpleOnGestureListener
	{
		
		readonly ZoomableScrollViewRenderer renderer;

		public CustomGestureDetector (ZoomableScrollViewRenderer renderer)
		{
			this.renderer = renderer;
		}

		public override bool OnDown (MotionEvent e)
		{
			return true;
		}

		public override bool OnDoubleTap (MotionEvent e)
		{
			float X = e.GetX ();
			float Y = e.GetY ();
			float dScale;
			float currentScale = renderer.MatrixArray [Matrix.MscaleX];

			if (renderer.MatrixArray [Matrix.MscaleX] == 1) {
				dScale = 2.0f;
				renderer.FocusX = e.GetX ();
				renderer.FocusY = e.GetY ();
			} else {
				dScale = 1.0f;
				renderer.PosX = 0;
				renderer.PosY = 0;
				renderer.FocusX = renderer.ScreenWidth / 2;
				renderer.FocusY = renderer.ViewHeight / 2;
			}

			ValueAnimator animator = new ValueAnimator ();
			animator.SetFloatValues (currentScale, dScale);
			animator.SetDuration (200);
			animator.AddUpdateListener (new ScaleAnimator (renderer));
			animator.Start ();
			return true;
		}

		class ScaleAnimator : Java.Lang.Object, ValueAnimator.IAnimatorUpdateListener
		{

			ZoomableScrollViewRenderer renderer;

			public ScaleAnimator (ZoomableScrollViewRenderer renderer)
			{
				this.renderer = renderer;
			}

			public void OnAnimationUpdate (ValueAnimator animation)
			{
				
				float t = (float)animation.AnimatedValue;
				renderer.ScaleFactor = t;
				if (renderer.MatrixArray [Matrix.MtransY] > renderer.ScreenRect.Top) {
					renderer.FocusY = renderer.ScreenRect.Top;
				} else if (renderer.MatrixArray [Matrix.MtransY] + renderer.ScreenRect.Bottom * renderer.MatrixArray [Matrix.MscaleY] < renderer.ScreenRect.Bottom) {
					renderer.FocusY = renderer.ScreenRect.Bottom;
				}

				if (renderer.MatrixArray [Matrix.MtransX] > renderer.ScreenRect.Left) {
					renderer.FocusX = renderer.ScreenRect.Left;
				} else if (renderer.MatrixArray [Matrix.MtransX] + renderer.ScreenRect.Right * renderer.MatrixArray [Matrix.MscaleX] < renderer.ScreenRect.Right) {
					renderer.FocusX = renderer.ScreenRect.Right;
				}

				renderer.ScaleMatrix.SetScale (renderer.ScaleFactor, renderer.ScaleFactor, renderer.FocusX, renderer.FocusY);
				renderer.ScaleMatrix.GetValues (renderer.MatrixArray);

				renderer.Invalidate ();
				renderer.RequestLayout ();
			}
		}
	}

	public class ZoomableScrollViewRenderer : ViewRenderer
	{
		GestureDetector doubleTapGestureDetector;
	
		public float ScaleFactor = 1.0f;

		public Matrix ScaleMatrix = new Matrix ();
		public Matrix MatrixInverse = new Matrix ();
		public Matrix SavedMatrix = new Matrix ();

		Rect PageRect = new Rect ();
		public Rect ScreenRect = new Rect ();
		RectF PageRectF = new RectF ();

		const int NONE = 0;
		const int DRAG = 1;
		const int ZOOM = 2;

		const int PORTRAIT = 0;
		const int LANDSCAPE = 1;

		int Orientation = PORTRAIT;
		int Mode = NONE;

		PointF Start = new PointF ();
		PointF Mid = new PointF ();
		float OldDist = 1f;
		float[] LastEvent = null;

		public int ViewWidth;
		public int ViewHeight;
		public int ScreenWidth;
		public int ScreenHeight;

		public float PosX;
		public float PosY;

		public float FocusX;
		public float FocusY;

		float Left = 0f;
		float Right = 0f;
		float Top = 0f;
		float Bottom = 0f;
		float l;
		float r;
		float t;
		float b;
		float dx;
		float dy;

		public float[] MatrixArray = new float[9];
		private float[] InvalidateWorkingArray = new float[6];
		private float[] DispatchTouchEventWorkingArray = new float[2];
		private float[] OnTouchEventWorkingArray = new float[2];

		public ZoomableScrollViewRenderer ()
		{
			doubleTapGestureDetector = new GestureDetector (this.Context, new CustomGestureDetector (this));
			UpdateScaleFactor ();
			ScaleMatrix.GetValues (MatrixArray);
		}

		public void UpdateScaleFactor ()
		{
			this.ScaleMatrix.SetScale (this.ScaleFactor, this.ScaleFactor, FocusX, FocusY);
		}

		protected override void OnLayout (bool changed, int l, int t, int r, int b)
		{
			DisplayMetrics dm = Resources.DisplayMetrics;
			global::Android.Graphics.Point size = new global::Android.Graphics.Point ();
			Display.GetRealSize (size);
			ScreenWidth = size.X;
			ScreenHeight = size.Y;
			ScreenRect = new Rect (0, 0, ScreenWidth, ScreenHeight);

			
			int childCount = ChildCount;
			for (int i = 0; i < childCount; i++) {
				global::Android.Views.View child = GetChildAt (i);
				if (child.Visibility != ViewStates.Gone) {
					if (i == 0) {
						child.Layout (0, 0, l + child.MeasuredWidth, t + child.MeasuredHeight);
					} else {
						child.Layout (l, t, l + child.MeasuredWidth, t + child.MeasuredHeight);
					}

				}
			}
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			int OriginalOrientation = Orientation;
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);

			ViewWidth = MeasureSpec.GetSize (widthMeasureSpec);
			ViewHeight = MeasureSpec.GetSize (heightMeasureSpec);



			if (ViewWidth / ViewHeight < 1)
				Orientation = PORTRAIT;
			else
				Orientation = LANDSCAPE;

			if (OriginalOrientation != Orientation) {
				PosX = 0;
				PosY = 0;
				ScaleMatrix = new Matrix ();
			}

			int childCount = ChildCount;
			for (int i = 0; i < childCount; i++) {
				global::Android.Views.View child = GetChildAt (i);
				if (child.Visibility != ViewStates.Gone) {
					MeasureChild (child, widthMeasureSpec, heightMeasureSpec);
				}
			}
		}

		protected override void DispatchDraw (Canvas canvas)
		{
			canvas.Save ();
			canvas.Matrix = ScaleMatrix;
			canvas.Translate (PosX, PosY);
			base.DispatchDraw (canvas);
			canvas.Restore ();
		}

		public override bool DispatchTouchEvent (MotionEvent e)
		{
			DispatchTouchEventWorkingArray [0] = e.GetX ();
			DispatchTouchEventWorkingArray [1] = e.GetY ();
			DispatchTouchEventWorkingArray = ScreenPointsToScaledPoints (DispatchTouchEventWorkingArray);
			e.SetLocation (DispatchTouchEventWorkingArray [0], DispatchTouchEventWorkingArray [1]);

			return base.DispatchTouchEvent (e);
		}

		public void ResetPageRect ()
		{
			ScaleMatrix.Reset ();
			ScaleMatrix.MapRect (PageRectF);
			PageRect = RectF2Rect (PageRectF, PageRect);
			Invalidate ();
		}

		public override IViewParent InvalidateChildInParent (int[] location, Rect dirty)
		{
			InvalidateWorkingArray [0] = dirty.Left;
			InvalidateWorkingArray [1] = dirty.Top;
			InvalidateWorkingArray [2] = dirty.Right;
			InvalidateWorkingArray [3] = dirty.Bottom;

			InvalidateWorkingArray = ScaledPointsToScreenPoints (InvalidateWorkingArray);
			dirty.Set ((int)Math.Round (InvalidateWorkingArray [0]), (int)Math.Round (InvalidateWorkingArray [1]),
				(int)Math.Round (InvalidateWorkingArray [2]), (int)Math.Round (InvalidateWorkingArray [3]));
			location [0] *= (int)ScaleFactor;
			location [1] *= (int)ScaleFactor;
			return base.InvalidateChildInParent (location, dirty);
		}

		public bool AdjustView ()
		{
			
			Left = MatrixArray [Matrix.MtransX] + PosX * MatrixArray [Matrix.MscaleX];
			Right = Left + ScreenRect.Right * MatrixArray [Matrix.MscaleX];
			Top = MatrixArray [Matrix.MtransY] + PosY * MatrixArray [Matrix.MscaleY];
			Bottom = Top + ScreenRect.Bottom * MatrixArray [Matrix.MscaleY];

			l = Left + dx;
			r = Right + dx;
			t = Top + dy;
			b = Bottom + dy;



			if (l >= ScreenRect.Left) {
				dx = (float)ScreenRect.Left - Left;

			} else if (r <= ScreenRect.Right) {
				dx = (float)ScreenRect.Right - Right;

			}

			if (t >= ScreenRect.Top) {
				dy = (float)ScreenRect.Top - Top;

			} else if (b <= ScreenRect.Bottom) {
				dy = (float)ScreenRect.Bottom - Bottom;
			}

			if (dx == 0.0f && dy == 0.0f) {
				return false;
			}

			dx /= MatrixArray [Matrix.MscaleX];
			dy /= MatrixArray [Matrix.MscaleY];
			PosX += dx;
			PosY += dy;
			return true;
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			float NewScaleFactor = 2f;

			global::Android.Views.View child = GetChildAt (0);
			OnTouchEventWorkingArray [0] = e.GetX ();
			OnTouchEventWorkingArray [1] = e.GetY ();

			OnTouchEventWorkingArray = ScaledPointsToScreenPoints (OnTouchEventWorkingArray);
			e.SetLocation (OnTouchEventWorkingArray [0], OnTouchEventWorkingArray [1]);

			doubleTapGestureDetector.OnTouchEvent (e);


			MotionEventActions action = e.Action;
			switch (action & MotionEventActions.Mask) {
			case MotionEventActions.Down:
				{
					FocusX = e.GetX ();
					FocusY = e.GetY ();
					SavedMatrix.Set (ScaleMatrix);
					Start.Set (e.GetX (), e.GetY ());
					Mode = DRAG;
					LastEvent = null;
					break;
				}
			case MotionEventActions.PointerDown:
				{
					OldDist = Spacing (e);
					if (OldDist > 10f) {
						SavedMatrix.Set (ScaleMatrix);
						MidPoint (Mid, e);
						Mode = ZOOM;
					}
					LastEvent = new float[4];
					LastEvent [0] = e.GetX (0);
					LastEvent [1] = e.GetX (1);
					LastEvent [2] = e.GetY (0);
					LastEvent [3] = e.GetY (1);


					break;
				}
			case MotionEventActions.Move:
				{
					if (Mode == DRAG) {
						if (ScaleFactor == 1)
							return false;
						ScaleMatrix.Set (SavedMatrix);

						dx = e.GetX () - Start.X;
						dy = e.GetY () - Start.Y;

						Start.Set (e.GetX (), e.GetY ());

						AdjustView ();



					} else if (Mode == ZOOM) {
						float NewDist = Spacing (e);
						l = MatrixArray [Matrix.MtransX];
						r = l + ScreenRect.Right * MatrixArray [Matrix.MscaleX];
						
						if (NewDist > 10f) {
							ScaleMatrix.Set (SavedMatrix);

							NewScaleFactor = (NewDist / OldDist);
						
							if (ScaleFactor >= 1.0f) {
								dx = e.GetX () - Start.X;
								dy = e.GetY () - Start.Y;

								Start.Set (e.GetX (), e.GetY ());

								AdjustView ();
							}


							if (NewScaleFactor >= 1.0f) {
								FocusX = Mid.X;
								FocusY = Mid.Y;
							} else {
								FocusX = ViewWidth / 2;
								FocusY = ScreenHeight / 2;
							}

							ScaleMatrix.PostScale (NewScaleFactor, NewScaleFactor, FocusX, FocusY);
							ScaleMatrix.GetValues (MatrixArray);
							ScaleFactor = MatrixArray [Matrix.MscaleX];


							PageRectF.Set (PageRect);
							ScaleMatrix.MapRect (PageRectF);
							PageRect = RectF2Rect (PageRectF, PageRect);
							ScaleMatrix.Invert (MatrixInverse);
						}
					}
					break;
				}
			case MotionEventActions.Up:
			case MotionEventActions.PointerUp:
				{
					Mode = NONE;
					LastEvent = null;
					break;
				}
			}
				
			ScaleMatrix.GetValues (MatrixArray);
			Invalidate ();


			if (MatrixArray [Matrix.MscaleX] < 1.0f && Mode == NONE) {
				ResetPageRect ();
				PosX = 0;
				PosY = 0;
				FocusX = ScreenWidth / 2;
				FocusY = ViewHeight / 2;
				ValueAnimator animator = new ValueAnimator ();
				animator.SetFloatValues (ScaleFactor, 1.0f);
				animator.SetDuration (200);
				animator.AddUpdateListener (new ScaleAnimator (this));
				animator.Start ();
			}
			return true;
			
		}

		private float[] ScaledPointsToScreenPoints (float[] a)
		{
			ScaleMatrix.MapPoints (a);
			return a;
		}

		private float[] ScreenPointsToScaledPoints (float[] a)
		{
			MatrixInverse.MapPoints (a);
			return a;
		}

		private Rect RectF2Rect (RectF inRectF, Rect outRect)
		{
			outRect.Left = (int)inRectF.Left;
			outRect.Right = (int)inRectF.Right;
			outRect.Top = (int)inRectF.Top;
			outRect.Bottom = (int)inRectF.Bottom;
			return outRect;
		}

		private float Spacing (MotionEvent e)
		{
			float x = e.GetX (0) - e.GetX (1);
			float y = e.GetY (0) - e.GetY (1);
			return (float)Math.Sqrt (x * x + y * y);
		}

		private void MidPoint (PointF point, MotionEvent e)
		{
			float x = e.GetX (0) + e.GetX (1);
			float y = e.GetY (0) + e.GetY (1);
			point.Set (x / 2, y / 2);
		}


		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) {

			}
		}
	}

	class ScaleAnimator : Java.Lang.Object, ValueAnimator.IAnimatorUpdateListener
	{

		ZoomableScrollViewRenderer renderer;

		public ScaleAnimator (ZoomableScrollViewRenderer renderer)
		{
			this.renderer = renderer;
		}

		public void OnAnimationUpdate (ValueAnimator animation)
		{

			float t = (float)animation.AnimatedValue;
			renderer.ScaleFactor = t;
			renderer.UpdateScaleFactor ();
			renderer.Invalidate ();
			renderer.RequestLayout ();
		}



	}

}

