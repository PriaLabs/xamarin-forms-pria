﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;
using Xamarin.Forms.Platform.Android;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Widget;

[assembly: ExportRenderer (typeof (GooglePlusLoginButton), typeof (GooglePlusLoginButtonRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class GooglePlusLoginButtonRenderer : ViewRenderer<GooglePlusLoginButton,SignInButton>
	{
		protected override void OnElementChanged (ElementChangedEventArgs<GooglePlusLoginButton> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) 
			{
				SignInButton button = new SignInButton (Context);
				button.Click += (object sender, EventArgs args) => DependencyService.Get<IGooglePlusService> ().SignIn ();
				SetNativeControl (button);
			}
		}
	}
}

