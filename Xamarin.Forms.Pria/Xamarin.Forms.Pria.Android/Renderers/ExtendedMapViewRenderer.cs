﻿using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Gms.Maps;
using Android.OS;
using Android.Widget;
using System.Reflection;
using TwinTechs.Droid.Extensions;
using Android.Gms.Maps.Model;
using System.Collections.Generic;
using Java.Lang;
using Android.Graphics;
using System.Net;
using Java.Net;
using System.Threading.Tasks;
using Android.App;
using Android.Util;
using Android.Locations;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.Android;

[assembly: ExportRenderer(typeof(ExtendedMapView), typeof(ExtendedMapViewRenderer))]
namespace Xamarin.Forms.Pria.Android
{
	public class ExtendedMapViewRenderer : ViewRenderer<ExtendedMapView, MapView>, GoogleMap.IInfoWindowAdapter, GoogleMap.IOnCameraChangeListener, IOnMapReadyCallback, GoogleMap.IOnMyLocationChangeListener
	{
		GoogleMap GoogleMap { get; set; }
		readonly float pinImageSize;
		readonly float borderWidth;

		public ExtendedMapViewRenderer()
		{
			pinImageSize = 50 * Context.Resources.DisplayMetrics.Density;
			borderWidth = 6.0f * Context.Resources.DisplayMetrics.Density;
		}

		protected override void OnElementChanged(ElementChangedEventArgs<ExtendedMapView> e)
		{
			if (Control == null)
			{
				MapView m = new MapView(Context);
				m.OnCreate(null);
				m.OnResume();
				m.GetMapAsync(this);
				SetNativeControl(m);
			}
			base.OnElementChanged(e);

			UpdatePins();
			UpdatePolylines();
			UpdateVisibleRegion();
		}

		public void OnMapReady(GoogleMap googleMap)
		{
			GoogleMap = googleMap;

			GoogleMap.SetInfoWindowAdapter(this);
			GoogleMap.SetOnCameraChangeListener(this);
			GoogleMap.InfoWindowClick += InfoWindowClicked;
			GoogleMap.MarkerClick += GoogleMap_MarkerClick;
			GoogleMap.UiSettings.MyLocationButtonEnabled = true;
			GoogleMap.MyLocationEnabled = true;
			GoogleMap.MapClick+=GoogleMap_MapClick;
			GoogleMap.SetOnMyLocationChangeListener(this);

			if (Element != null)
			{
				UpdatePins();
				UpdateVisibleRegion();
			}
		}

		void GoogleMap_MapClick(object sender, GoogleMap.MapClickEventArgs e)
		{
			foreach (var p in Element.Pins) {
				p.IsSelected = false;
			}
		}

		void GoogleMap_MarkerClick(object sender, GoogleMap.MarkerClickEventArgs e)
		{
			ExtendedMapPin pin = MarkerToPin(e.Marker);
			pin.IsSelected = true;
			e.Handled = string.IsNullOrEmpty(pin.Title);
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (disposing)
			{
				if (Control != null)
				{
					Control.OnPause();
					Control.OnDestroy();
				}
			}
		}

		public void OnMyLocationChange(Location location)
		{
			CameraPosition camPos = new CameraPosition.Builder()
											.Target(new LatLng(location.Latitude, location.Longitude))
											.Zoom(13)
											.Bearing(location.Bearing)
											.Build();

			GoogleMap.AnimateCamera(CameraUpdateFactory.NewCameraPosition(camPos));
			GoogleMap.SetOnMyLocationChangeListener(null);
		}

		public void OnCameraChange(CameraPosition position)
		{
			if (GoogleMap == null)
			{
				return;
			}
			if (Element == null)
			{
				return;
			}
			ExtendedMapSpan s = ComputeVisibleRegion();
			if (!ExtendedMapSpan.Equals(s, Element.VisibleRegion))
			{
				Element.VisibleRegion = s;
			}
		}

		private ExtendedMapSpan ComputeVisibleRegion()
		{
			if (GoogleMap == null)
			{
				return null;
			}
			Projection proj = GoogleMap.Projection;
			double latitudeDegrees = System.Math.Abs(proj.VisibleRegion.LatLngBounds.Northeast.Latitude - proj.VisibleRegion.LatLngBounds.Southwest.Latitude);
			double longitudeDegrees = System.Math.Abs(proj.VisibleRegion.LatLngBounds.Northeast.Longitude - proj.VisibleRegion.LatLngBounds.Southwest.Longitude);
			ExtendedMapSpan s = new ExtendedMapSpan(new ExtendedPosition(proj.VisibleRegion.LatLngBounds.Center.Latitude, proj.VisibleRegion.LatLngBounds.Center.Longitude), latitudeDegrees, longitudeDegrees);
			return s;
		}

		private void UpdateVisibleRegion()
		{
			if (GoogleMap == null)
			{
				return;
			}
			if (Element.VisibleRegion == null)
			{
				return;
			}
			ExtendedMapSpan span = Element.VisibleRegion.ClampLatitude(85, -85);
			ExtendedMapSpan s = ComputeVisibleRegion();
			if (ExtendedMapSpan.Equals(span, s))
			{
				return;
			}

			LatLng latLng = new LatLng(span.Center.Latitude + span.LatitudeDegrees / 2, span.Center.Longitude + span.LongitudeDegrees / 2);
			CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngBounds(new LatLngBounds(new LatLng(span.Center.Latitude - span.LatitudeDegrees / 2, span.Center.Longitude - span.LongitudeDegrees / 2), latLng), 0);
			try
			{
				GoogleMap.MoveCamera(cameraUpdate);
			}
			catch (IllegalStateException)
			{
			}
		}

		void InfoWindowClicked(object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			ExtendedMapPin pin = MarkerToPin(e.Marker);
			if (pin.CalloutDetailCommand != null)
			{
				if (pin.CalloutDetailCommand.CanExecute(pin))
				{
					pin.CalloutDetailCommand.Execute(pin);
				}
			}
		}

		private ExtendedMapPin MarkerToPin(Marker marker)
		{
			return pinIds[marker.Id];   //TODO: Find a better way than matchin indexes
		}

		protected override async void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (ExtendedMapView.PinsProperty.PropertyName == e.PropertyName)
			{
				UpdatePins();
			}
			if (ExtendedMapView.VisibleRegionProperty.PropertyName == e.PropertyName)
			{
				UpdateVisibleRegion();
			}
			if (ExtendedMapView.PolylinesProperty.PropertyName == e.PropertyName)
			{
				UpdatePolylines();
			}
		}
		List<Marker> markers = new List<Marker>();
		Dictionary<string, ExtendedMapPin> pinIds = new Dictionary<string, ExtendedMapPin>();

		private async void UpdatePins()
		{
			if (GoogleMap == null)
			{
				return;
			}

			markers.ForEach(m => m.Remove());
			markers.Clear();
			pinIds.Clear();

			if (Element.Pins != null)
			{
				foreach (ExtendedMapPin p in Element.Pins)
				{
					await AddPin(p, markers, pinIds);
				}
			}
		}

		async Task AddPin(ExtendedMapPin p, List<Marker> markers, Dictionary<string, ExtendedMapPin> pinIds)
		{
			var mo = new global::Android.Gms.Maps.Model.MarkerOptions();
			mo.SetPosition(new global::Android.Gms.Maps.Model.LatLng(p.Position.Latitude, p.Position.Longitude));
			mo.SetTitle(p.Title);
			mo.SetSnippet(p.Subtitle);
			if (p.PinImage != null)
			{
				if (p.PinImage is FileImageSource)
				{
					Bitmap bitmap = await new FileImageSourceHandler().LoadImageAsync(p.PinImage, Context);
					mo.SetIcon(BitmapDescriptorFactory.FromBitmap(GetCroppedBitmapWithBorder(ResizeBitmap(bitmap, pinImageSize, pinImageSize), p.BorderColor)));
				}
				else
				{
					Bitmap image = await Task.Factory.StartNew(() =>
					{
						return GetImageBitmapFromUrl((p.PinImage as UriImageSource).Uri.AbsoluteUri);
					}).ConfigureAwait(true);

					if (image != null)
					{
						mo.SetIcon(BitmapDescriptorFactory.FromBitmap(GetCroppedBitmapWithBorder(ResizeBitmap(image, pinImageSize, pinImageSize), p.BorderColor)));
					}
				}
			}

			Marker m = GoogleMap.AddMarker(mo);

			pinIds.Add(m.Id, p);
			markers.Add(m);

			return;
		}

		private Bitmap ResizeBitmap(Bitmap bitmap, float newWidth, float newHeight)
		{

			int width = bitmap.Width;
			int height = bitmap.Height;

			float scaleWidth = newWidth / (float)width;
			float scaleHeight = newHeight / (float)height;

			Matrix matrix = new Matrix();
			matrix.PostScale(scaleWidth, scaleHeight);

			Bitmap output = Bitmap.CreateBitmap(bitmap, 0, 0, width, height, matrix, false);

			bitmap.Recycle();

			return output;
		}

		private Bitmap GetCroppedBitmapWithBorder(Bitmap bitmap, Xamarin.Forms.Color? color)
		{
			Bitmap output = Bitmap.CreateBitmap(bitmap.Width, bitmap.Height, Bitmap.Config.Argb8888);
			Canvas canvas = new Canvas(output);

			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, bitmap.Width, bitmap.Height);

			paint.AntiAlias = true;
			canvas.DrawARGB(0, 0, 0, 0);
			canvas.DrawCircle(bitmap.Width / 2, bitmap.Height / 2, bitmap.Width / 2, paint);
			paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.SrcIn));
			canvas.DrawBitmap(bitmap, rect, rect, paint);

			if (color.HasValue)
			{
				paint.Color = color.Value.ToAndroid();
				paint.SetStyle(Paint.Style.Stroke);
				paint.StrokeWidth = borderWidth;
				canvas.DrawRoundRect(new RectF(rect), bitmap.Width / 2, bitmap.Height / 2, paint);
			}

			return output;
		}

		private Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			try
			{
				using (var webClient = new WebClient())
				{
					var imageBytes = webClient.DownloadData(url);
					if (imageBytes != null && imageBytes.Length > 0)
					{
						imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
					}
				}
			}
			catch (System.Exception ex)
			{
				Log.LogDebug("GetImageBitmapFromUrl " + ex.Message);
			}

			return imageBitmap;
		}

		List<Polyline> polylines = new List<Polyline>();
		private void UpdatePolylines()
		{
			if (GoogleMap == null)
			{
				return;
			}
			polylines.ForEach(p => p.Remove());
			polylines.Clear();
			if (Element.Polylines != null)
			{
				Element.Polylines.ToList().ForEach(p =>
				{
					PolylineOptions po = new PolylineOptions();
					po.InvokeColor(p.Color.ToAndroid());
					foreach (ExtendedPosition pos in p.Points)
					{
						po.Add(new LatLng(pos.Latitude, pos.Longitude));
					}
					Polyline polyline = GoogleMap.AddPolyline(po);
					polylines.Add(polyline);
				});

			}
		}

		public global::Android.Views.View GetInfoContents(global::Android.Gms.Maps.Model.Marker marker)
		{
			int padding = 6; //TODO: default resource

			ExtendedMapPin pin = MarkerToPin(marker);

			LinearLayout mainContainer = new LinearLayout(Context) { Orientation = Orientation.Horizontal };
			mainContainer.LayoutParameters = new global::Android.Views.ViewGroup.LayoutParams(Context.Resources.DisplayMetrics.WidthPixels / 2, LayoutParams.WrapContent);
			mainContainer.SetPadding(padding, padding, padding, padding);

			ImageView imageView = new ImageView(Context);
			imageView.SetMinimumWidth(70);
			imageView.SetMinimumHeight(70);
			//imageView.SetPadding (padding, padding, padding, padding);
			imageView.LayoutParameters = new global::Android.Views.ViewGroup.LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent);

			TextView titleView = new TextView(Context);
			titleView.SetTextColor(Xamarin.Forms.Color.Black.ToAndroid());
			//titleView.SetPadding (padding, 0, padding, 0);
			titleView.SetTextSize(global::Android.Util.ComplexUnitType.Dip, 14.0f);
			titleView.LayoutParameters = new global::Android.Views.ViewGroup.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

			TextView subtitleView = new TextView(Context);
			subtitleView.SetTextColor(Xamarin.Forms.Color.Gray.ToAndroid());
			subtitleView.SetTextSize(global::Android.Util.ComplexUnitType.Dip, 12.0f);
			//subtitleView.SetPadding (padding, 0, padding, 0);
			subtitleView.LayoutParameters = new global::Android.Views.ViewGroup.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

			LinearLayout textContainer = new LinearLayout(Context) { Orientation = Orientation.Vertical };
			textContainer.LayoutParameters = new LinearLayout.LayoutParams(0, LayoutParams.WrapContent) { Weight = 1 };
			textContainer.SetPadding(2 * padding, 0, padding, 0);

			textContainer.AddView(titleView);
			textContainer.AddView(subtitleView);
			mainContainer.AddView(imageView);
			mainContainer.AddView(textContainer);

			Bitmap image;
			if (pin.PinImage is FileImageSource)
			{
				image = new FileImageSourceHandler().LoadImageAsync(pin.PinImage, Context).Result;
			}
			else
			{
				image = Task.Factory.StartNew(() => GetImageBitmapFromUrl((pin.PinImage as UriImageSource).Uri.AbsoluteUri)).Result;
			}

			if (image != null)
			{
				imageView.SetImageBitmap(ResizeBitmap(image, pinImageSize, pinImageSize));
			}

			titleView.SetText(pin.Title, TextView.BufferType.Normal);

			subtitleView.SetText(pin.Subtitle, TextView.BufferType.Normal);

			return mainContainer;
		}

		public global::Android.Views.View GetInfoWindow(global::Android.Gms.Maps.Model.Marker marker)
		{
			return null;
		}
	}
}
