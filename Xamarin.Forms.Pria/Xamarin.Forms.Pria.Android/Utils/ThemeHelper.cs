﻿using System;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace Xamarin.Forms.Pria.Android
{
	public static class ThemeHelper
	{
		public static void ColorizeCell(global::Android.Views.View cell,Color pressedColor,Color selectedColor){
			StateListDrawable states = new StateListDrawable();
			states.AddState(new int[] {global::Android.Resource.Attribute.StatePressed,global::Android.Resource.Attribute.StateFocused}, new ColorDrawable(pressedColor.ToAndroid()));
			states.AddState(new int[] {global::Android.Resource.Attribute.StateSelected}, new ColorDrawable(selectedColor.ToAndroid()));
			states.AddState(new int[] {}, new ColorDrawable(global::Android.Graphics.Color.Transparent)); 
		
			cell.SetBackgroundDrawable (states);
		}

		public static void ColorizeSwitch(global::Android.Widget.Switch switchControl,Color thumbColor,Color trackColor){
			Colorize (switchControl.ThumbDrawable,thumbColor);
			Colorize (switchControl.TrackDrawable,trackColor);
		}

		public static void ColorizeScrollView(global::Android.Widget.ScrollView scrollView,Color c){
			Colorize ("overscroll_glow", c);
			Colorize ("overscroll_edge", c);
		}

		public static void ColorizeListView(global::Android.Widget.ListView listView,Color c){
			Colorize ("overscroll_glow", c);
			Colorize ("overscroll_edge", c);
			//Colorize (global::Android.Resource.Drawable.lis, c);
		}

		public static void ColorizeEditText(global::Android.Widget.EditText editText,Color c){
			Colorize("text_select_handle_middle",c);
			Colorize("text_select_handle_left",c);
			Colorize("text_select_handle_right",c);
		}
		public static void ColorizeRadioButton(global::Android.Widget.RadioButton rb,Color c){
			Colorize("btn_radio_on",c);
			Colorize("btn_radio_off",c);
			Colorize("btn_radio_on_pressed",c);
			Colorize("btn_radio_off_pressed",c);
		}

		/*public static void SetOverscrollColor(Color c){
			
			Colorize("text_select_handle_middle",c);
			Colorize("text_select_handle_left",c);
			Colorize("text_select_handle_right",c);

			Colorize (global::Android.Resource.Drawable.ListSelectorBackground, c);

	


		}*/

		private static void Colorize(Drawable drawable,Color c){
			drawable.SetColorFilter(c.ToAndroid(), PorterDuff.Mode.SrcIn);
		}

		private static void Colorize(int drawableId,Color c){
			if (drawableId == 0) {
				Log.LogDebug ("drawableId == 0");
				return;
			}
			Drawable drawable = Forms.Context.Resources.GetDrawable(drawableId);
			Colorize (drawable,c);
		}

		private static void Colorize(string id,Color c){
			int drawableId = Forms.Context.Resources.GetIdentifier(id, "drawable", "android");
			Colorize (drawableId, c);
		}
	}
}

