﻿using System;
using Android.Content;
using System.IO;

namespace Xamarin.Forms.Pria.Android
{
	public static class AndroidFileUtils
	{
		public static global::Android.Net.Uri GetRawResourceUri(string fileName)
		{
			return global::Android.Net.Uri.Parse (
				string.Format ("{0}://{1}/raw/{2}", 
					ContentResolver.SchemeAndroidResource, 
					Forms.Context.PackageName, 
					Path.GetFileNameWithoutExtension (fileName)
				)
			);
		}
	}
}

