using System;
using Android.App;
using Android.OS;
using Android.Content;
using System.Reflection;
using System.Collections.Generic;
using Xamarin.Forms.Platform.Android;
using Android.Util;
using TwinTechs.Droid.Controls;
using TwinTechs.Gestures;
using Ninject;
using Android.Content.PM;

namespace Xamarin.Forms.Pria.Android
{
	/*[BroadcastReceiver(Enabled=true)]
	public class NotificationsBroadcastReceiver : BroadcastReceiver
	{
		CoreActivity activity;

		public NotificationsBroadcastReceiver ()
		{
		}

		public NotificationsBroadcastReceiver (CoreActivity activity)
		{
			this.activity = activity;	
		}

		public override void OnReceive (Context context, Intent intent)
		{
			activity.DeviceTokenRegistered ("abc");


		}
	}*/

	public abstract class CoreActivity : FormsAppCompatActivity
	{
		public ActivityLifecycleObserver ActivityLifecycleObserver{ get; private set; }

		protected CoreActivity()
		{
			ActivityLifecycleObserver = new ActivityLifecycleObserver ();
		}

		public BaseApp App{ get; protected set;}

		protected abstract bool InitFacebookSDK{get;}
		protected abstract List<Assembly> GetAssembliesForLocalization ();
		protected abstract BaseApp CreateApp ();
		protected internal abstract string InAppPublicKey{get;}
		protected abstract string GoogleAnalyticsTrackingID{ get; }
		protected abstract bool UsesGooglePlusLogin{ get; }
		//protected abstract bool UseRemoteNotifications{ get; }

		//BroadcastReceiver notificationsBroadcastReceiver;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			List<Assembly> localizations = GetAssembliesForLocalization () ?? new List<Assembly>();
			localizations.Insert(0,Assembly.Load("Xamarin.Forms.Pria"));
			localizations.Insert(1,Assembly.Load("Xamarin.Forms.Pria.Android"));
			LocalizationManager.Instance.LoadLocalizations (localizations);

			App = CreateApp ();
			App.Kernel.Bind (typeof(ILocalization)).To (typeof(LocalizationManagerIoC)).InSingletonScope ().WithConstructorArgument(typeof(List<Assembly>), localizations);

			if (InitFacebookSDK) {
				DependencyService.Get<IFacebookService> ().Setup (null,BaseApp.CurrentApp.Kernel.Get<ILocalization>().GetText("AppName"));
			}

			if (InAppPublicKey != null) {
				DependencyService.Get<IInAppBilling> ().Init ();
			}


			if (GoogleAnalyticsTrackingID != null) {
				DependencyService.Get<IGoogleAnalyticsService> ().Init (GoogleAnalyticsTrackingID, BaseApp.CurrentApp.Kernel.Get<ILocalization>().GetText("AppName"));
			}

			if (UsesGooglePlusLogin) {
				DependencyService.Get<IGooglePlusService> ().Setup (null);
			}

			/*if (UseRemoteNotifications) {
				PushClient.CheckDevice (this);
				PushClient.CheckManifest (this);
				PushClient.Register(this, PushHandlerBroadcastReceiver.SENDER_IDS);
			}*/

			ActivityLifecycleObserver.OnCreate (bundle);

			AppHelper.FastCellCache = FastCellCache.Instance;

			GestureRecognizerExtensions.Factory = new NativeGestureRecognizerFactory ();

			//notificationsBroadcastReceiver = new NotificationsBroadcastReceiver (this);

			App.MainPage = App.GetMainPage ();

			LoadApplication (App);
		}

		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			base.OnRequestPermissionsResult (requestCode, permissions, grantResults);
			ActivityLifecycleObserver.OnRequestPermissionsResult (requestCode, permissions, grantResults);
		}

		protected override void OnActivityResult (int requestCode, global::Android.App.Result resultCode, global::Android.Content.Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			ActivityLifecycleObserver.OnActivityResult (requestCode, resultCode, data);
		}
			
		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			ActivityLifecycleObserver.OnDestroy ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			ActivityLifecycleObserver.OnPause ();
		}

		protected override void OnRestart ()
		{
			base.OnRestart ();
			ActivityLifecycleObserver.OnRestart ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			ActivityLifecycleObserver.OnResume ();

			if (Intent.HasExtra ("NotificationIntent"))
			{
				if (this.Intent.HasExtra ("UserData"))
				{
					App.ReceivedLocalNotification (this.Intent.GetStringExtra ("UserData"));
				} 
				else
				{
					App.ReceivedLocalNotification (null);
				}
			}
		}

		protected override void OnStart ()
		{
			base.OnStart ();
			ActivityLifecycleObserver.OnStart ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			ActivityLifecycleObserver.OnStop ();

		}

		protected void ClearNotifications ()
		{
			NotificationManager notificationManager = (NotificationManager)GetSystemService (Context.NotificationService);
			notificationManager.Cancel (0);
		}
	}
}

