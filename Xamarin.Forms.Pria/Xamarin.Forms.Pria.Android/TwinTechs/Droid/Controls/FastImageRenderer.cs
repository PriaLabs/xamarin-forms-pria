﻿using System;
using Xamarin.Forms;
using TwinTechs.Controls;
using Xamarin.Forms.Platform.Android;
using MonoDroidToolkit.ImageLoader;
using TwinTechs.Droid.Controls;
using Xamarin.Forms.Pria.Services;
using System.IO;


[assembly: ExportRenderer (typeof(FastImage), typeof(FastImageRenderer))]
namespace TwinTechs.Droid.Controls
{
	public class FastImageRenderer : ImageRenderer
	{
		ImageLoader _imageLoader;
		int defaultResId;

		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);
			//			if (e.OldElement != null) {
			//				((FastImage)e.OldElement).ImageProvider = null;
			//			}
			if (e.NewElement != null) {
				var fastImage = e.NewElement as FastImage;
				_imageLoader = ImageLoaderCache.GetImageLoader (this);
				defaultResId = Control.Context.Resources.GetIdentifier (Path.GetFileNameWithoutExtension (((FastImage)Element).PlaceholderFilename), "drawable", Forms.Context.PackageName);
				SetImageUrl (fastImage.ImageUrl);
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (e.PropertyName == "ImageUrl") {
				var fastImage = Element as FastImage;
				SetImageUrl (fastImage.ImageUrl);
			}
		}


		public void SetImageUrl (string imageUrl)
		{
			if (Control == null) {
				return;
			}

			//Control.SetImageDrawable (null);
			Control.SetImageResource (defaultResId);

			if (imageUrl != null) {
				_imageLoader.DisplayImage (imageUrl, Control, defaultResId);
			} 
			/*else {
				Control.SetImageResource (defaultResId);
			}*/

		}
	}
}

