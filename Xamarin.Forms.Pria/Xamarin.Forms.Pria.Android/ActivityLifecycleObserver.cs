﻿using System;
using System.Collections.Generic;
using Android.Content.PM;

namespace Xamarin.Forms.Pria.Android
{
	public interface IActivityLifecycleSubscriber
	{
		void OnActivityResult (int requestCode, global::Android.App.Result resultCode, global::Android.Content.Intent data);
		void OnCreate (global::Android.OS.Bundle savedInstanceState);
		void OnStart();
		void OnStop ();
		void OnPause();
		void OnResume();
		void OnDestroy();
		void OnRestart ();
		void OnRequestPermussionsResult(int requestCode, string[] permissions, Permission[] grantResults);
	}

	public class ActivityLifecycleObserver
	{
		List<IActivityLifecycleSubscriber> subscribers = new List<IActivityLifecycleSubscriber>();

		public ActivityLifecycleObserver ()
		{
		}

		public void RegisterSubscriber(IActivityLifecycleSubscriber subscriber)
		{
			subscribers.Add (subscriber);
		}

		public void UnregisterSubscriber(IActivityLifecycleSubscriber subscriber)
		{
			subscribers.Remove (subscriber);
		}

		public void OnActivityResult (int requestCode, global::Android.App.Result resultCode, global::Android.Content.Intent data)
		{
			subscribers.ForEach(s=>s.OnActivityResult(requestCode,resultCode,data));
		}

		public void OnCreate (global::Android.OS.Bundle savedInstanceState)
		{
			subscribers.ForEach(s=>s.OnCreate(savedInstanceState));
		}

		public void OnDestroy ()
		{
			subscribers.ForEach(s=>s.OnDestroy());
		}

		public void OnStart ()
		{
			subscribers.ForEach(s=>s.OnStart());
		}

		public void OnStop ()
		{
			subscribers.ForEach(s=>s.OnStop());
		}

		public void OnPause ()
		{
			subscribers.ForEach(s=>s.OnPause());
		}

		public void OnResume ()
		{
			subscribers.ForEach(s=>s.OnResume());
		}

		public void OnRestart ()
		{
			subscribers.ForEach(s=>s.OnRestart());
		}

		public void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			subscribers.ForEach (s => s.OnRequestPermussionsResult (requestCode, permissions, grantResults));
		}
	}
}

