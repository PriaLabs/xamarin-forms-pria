using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using System.Threading.Tasks;
using UIKit;
using System.Threading;
using Facebook.CoreKit;
using Foundation;
using Facebook.ShareKit;
using Facebook.LoginKit;

[assembly: Dependency(typeof(FacebookService))]
namespace Xamarin.Forms.Pria.iOS
{
	[Preserve]
	public class FacebookService : IFacebookService
	{
		public FacebookService ()
		{
			Profile.Notifications.ObserveDidChange ((sender, e) => {
				if (OnSessionStatusChanged != null)
				{
					OnSessionStatusChanged (this, EventArgs.Empty);
				}
			});


			CoreAppDelegate d = UIApplication.SharedApplication.Delegate as CoreAppDelegate;
			if (d == null)
			{
				throw new InvalidCastException ("UIApplication delegate must inherit from CoreAppDelegate in order to use FacebookService");
			}

			d.OnAppActivated += (object sender, EventArgs e) => {
				AppEvents.ActivateApp ();
			};
			d.OnHandleOpenUrl += (object sender, CoreAppDelegate.OpenUrlEventArgs e) => {
				e.Handled = ApplicationDelegate.SharedInstance.OpenUrl (e.Application, e.Url, e.SourceApplication, e.Annotation);
			};
		}


		#region IFacebookService implementation

		public event EventHandler OnSessionStatusChanged;

		public void Setup (string appId, string appName)
		{
			Profile.EnableUpdatesOnAccessTokenChange (true);
			Settings.AppID = appId;
			Settings.DisplayName = appName;
		}

		public bool IsLogged
		{
			get {
				return Facebook.CoreKit.Profile.CurrentProfile != null;
			}
		}

		public string AccessToken
		{
			get {
				if (Facebook.CoreKit.AccessToken.CurrentAccessToken != null)
				{
					return Facebook.CoreKit.AccessToken.CurrentAccessToken.TokenString;
				}
				return null;
			}
		}

		// https://developers.facebook.com/docs/sharing/ios
		public void Share (string title, string description, string url, string imageUrl = null)
		{
			UIWindow window = UIApplication.SharedApplication.KeyWindow;
			UIViewController viewController = window.RootViewController;

			ShareLinkContent content = new ShareLinkContent ()
			{
				ContentTitle = title,
				ContentDescription = description
			};
			content.SetContentUrl (NSUrl.FromString (url));

			if (!string.IsNullOrEmpty (imageUrl))
			{
				content.ImageURL = NSUrl.FromString (imageUrl);
			}

			ShareDialog.Show (viewController, content, null);
		}

		public void Logout ()
		{
			new LoginManager ().LogOut();
		}

		#endregion
	}
}

