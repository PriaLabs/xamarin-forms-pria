﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using UIKit;
using Google.SignIn;
using Foundation;

[assembly: Dependency(typeof(GooglePlusService))]
namespace Xamarin.Forms.Pria.iOS
{
	[Preserve]
	public class GooglePlusService : UIViewController, ISignInDelegate, ISignInUIDelegate, IGooglePlusService
	{
		public void DidSignInForUser (SignIn signIn, GoogleUser user, Foundation.NSError error)
		{
			OnLoginStatusChanged?.Invoke (this, EventArgs.Empty);
		}

		public event EventHandler OnLoginStatusChanged;

		/// <summary>
		/// iOS: need to add two URL schemes to info.plist!
		/// </summary>
		/// <param name="clientId">Client identifier.</param>
		public void Setup (string clientId)
		{
			CoreAppDelegate d = UIApplication.SharedApplication.Delegate as CoreAppDelegate;
			if (d == null) {
				throw new InvalidCastException("UIApplication delegate must inherit from CoreAppDelegate in order to use FacebookService");
			}

			d.OnHandleOpenUrl += (object sender, CoreAppDelegate.OpenUrlEventArgs e) => {
				e.Handled = Google.SignIn.SignIn.SharedInstance.HandleURL (e.Url, e.SourceApplication, e.Annotation ?? new NSObject ());
			};

			Google.SignIn.SignIn.SharedInstance.ShouldFetchBasicProfile = true;
			Google.SignIn.SignIn.SharedInstance.AllowsSignInWithBrowser = true;
			Google.SignIn.SignIn.SharedInstance.AllowsSignInWithWebView = false;
			Google.SignIn.SignIn.SharedInstance.ClientId = clientId;
			Google.SignIn.SignIn.SharedInstance.Scopes = new []{ Constants.AuthScopeMe, Constants.AuthScopeLogin };
			Google.SignIn.SignIn.SharedInstance.Delegate = this;
			Google.SignIn.SignIn.SharedInstance.UiDelegate = this;
		}

		public void SignIn ()
		{
			Google.SignIn.SignIn.SharedInstance.Signin ();
		}

		public bool IsLogged {
			get {
				return Google.SignIn.SignIn.SharedInstance.CurrentUser != null;
			}
		}

		public string AccessToken {
			get {
				return IsLogged ? Google.SignIn.SignIn.SharedInstance.CurrentUser.Authentication.AccessToken : null;
			}
		}

		public override void PresentViewController (UIViewController viewControllerToPresent, bool animated, Action completionHandler)
		{
			UIViewController rootController = UIApplication.SharedApplication.Windows [0].RootViewController;

			rootController.PresentViewController (viewControllerToPresent, animated, completionHandler);
		}
	}
}

