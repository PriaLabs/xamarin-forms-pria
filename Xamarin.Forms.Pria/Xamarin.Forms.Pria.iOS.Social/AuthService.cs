﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using System.Threading.Tasks;
using Xamarin.Auth;
using UIKit;

[assembly: Dependency(typeof(AuthService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class AuthService : IAuth
	{
		#region IAuth implementation

		public Task<Account> LoginFacebook (string appId, string secret, string redirectUrl, string scope)
		{
			TaskCompletionSource<Account> tcs = new TaskCompletionSource<Account> ();

			var auth = new OAuth2Authenticator (
				           appId, 
				           secret, 
				           scope, 
				           new Uri ("https://m.facebook.com/dialog/oauth/"), 
						   new Uri (redirectUrl), 
				           new Uri ("https://graph.facebook.com/oauth/access_token")
			           );	    

			auth.Completed += (object sender, AuthenticatorCompletedEventArgs e) => {

				UIApplication.SharedApplication.KeyWindow.RootViewController.DismissViewController (true, null);

				if(e.IsAuthenticated)
				{
					AccountStore.Create().Save(e.Account,"Facebook");
					string token = e.Account.Properties["access_token"];
					tcs.SetResult(new Account(){Site = Site.Facebook, AccessToken = token});
				}
				else
				{
					tcs.TrySetResult(null);
				}

			};
				
			auth.Error += (object sender, AuthenticatorErrorEventArgs e) => {
				tcs.TrySetResult(null);
				auth.ShowUIErrors = false; // infinite error messages when offline: http://forums.xamarin.com/discussion/5866/xamarin-auth-and-infinite-error-alerts
			};

			UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController (auth.GetUI (), true, null);

			return tcs.Task;
		}


		public Task<Account> LoginGooglePlus (string appId, string secret, string redirectUrl, string scope)
		{
			TaskCompletionSource<Account> tcs = new TaskCompletionSource<Account> ();

			var auth = new OAuth2Authenticator (
				appId, 
				secret, 
				scope, 
				new Uri ("https://accounts.google.com/o/oauth2/auth"), 
				new Uri (redirectUrl), 
				new Uri ("https://accounts.google.com/o/oauth2/token")
			);	    

			auth.Completed += (object sender, AuthenticatorCompletedEventArgs e) => {

				UIApplication.SharedApplication.KeyWindow.RootViewController.DismissViewController (true, null);

				if(e.IsAuthenticated)
				{
					AccountStore.Create().Save(e.Account,"GPlus");
					string token = e.Account.Properties["access_token"];
					tcs.SetResult(new Account(){Site = Site.GPlus, AccessToken = token});
				}
				else
				{
					tcs.TrySetResult(null);
				}

			};

			auth.Error += (object sender, AuthenticatorErrorEventArgs e) => {
				tcs.TrySetResult(null);
				auth.ShowUIErrors = false; // infinite error messages when offline: http://forums.xamarin.com/discussion/5866/xamarin-auth-and-infinite-error-alerts
			};

			UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController (auth.GetUI (), true, null);

			return tcs.Task;
		}

		#endregion
	}
}

