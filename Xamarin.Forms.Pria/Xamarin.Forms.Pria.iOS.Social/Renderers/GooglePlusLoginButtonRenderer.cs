﻿using System;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using CoreGraphics;
using UIKit;
using Foundation;
using Google.SignIn;

[assembly: ExportRenderer (typeof (GooglePlusLoginButton), typeof (GooglePlusLoginButtonRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class GooglePlusLoginButtonRenderer : ViewRenderer<GooglePlusLoginButton,UIView>
	{
		public GooglePlusLoginButtonRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<GooglePlusLoginButton> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null)
			{
				UIButton b = new UIButton (UIButtonType.Custom);

				b.SetBackgroundImage(UIImage.FromBundle("Images/btn_google_signin_light_normal.png").CreateResizableImage(new UIEdgeInsets(40,100,40,100)), UIControlState.Normal);
				b.SetBackgroundImage(UIImage.FromBundle("Images/btn_google_signin_light_disabled.png").CreateResizableImage(new UIEdgeInsets(40,100,40,100)), UIControlState.Disabled);
				b.SetBackgroundImage(UIImage.FromBundle("Images/btn_google_signin_light_focus.png").CreateResizableImage(new UIEdgeInsets(40,100,40,100)), UIControlState.Focused);
				b.SetBackgroundImage(UIImage.FromBundle("Images/btn_google_signin_light_pressed.png").CreateResizableImage(new UIEdgeInsets(40,100,40,100)), UIControlState.Highlighted);
			
				b.SetTitle ("SIGN IN WITH GOOGLE", UIControlState.Normal);
				b.SetTitleColor (UIColor.FromRGBA (0, 0, 0, 138), UIControlState.Normal);
				b.TitleLabel.Font = UIFont.SystemFontOfSize (14.0f, UIFontWeight.Medium);

				b.TouchUpInside += (object sender, EventArgs args) => DependencyService.Get<IGooglePlusService> ().SignIn ();

				SetNativeControl (b);

				/* doesn't work - callback after login doesn't return signed user
				SignInButton button = new SignInButton ();

				//TouchUpInside for some reason doesn't work...
				button.AllTouchEvents += (object sender, EventArgs sade) => DependencyService.Get<IGooglePlusService> ().SignIn ();

				SetNativeControl (button);
				*/
			}
		}
	}
}

