using System;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
//using MonoTouch.FacebookConnect;
using Facebook.CoreKit;
using Facebook.LoginKit;
using CoreGraphics;
using UIKit;

[assembly: ExportRenderer (typeof (FacebookLoginButton), typeof (FacebookButtonRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class FacebookButtonRenderer:ViewRenderer<FacebookLoginButton,Facebook.LoginKit.LoginButton>
	{
		public FacebookButtonRenderer ()
		{

		}
		protected override void OnElementChanged (ElementChangedEventArgs<FacebookLoginButton> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {
				
				Facebook.LoginKit.LoginButton loginView = new Facebook.LoginKit.LoginButton (){
					ReadPermissions = Element.ReadPermissions ?? new string[0],
					//PublishPermissions = Element.PublishPermissions ?? new string[0],
					LoginBehavior = LoginBehavior.Native,
					Frame = new CGRect (85, 20, 151, 43)
				};

				SetNativeControl (loginView);
			}
		}

	}
}
