#!/bin/bash

OUTPUT_DIR="$HOME/nuget"

# remove old .nupkg
# rm *.nupkg

# create new packages
nuget pack Xamarin.Forms.Pria/xamarin.forms.pria.nuspec
nuget pack Xamarin.Forms.Pria.iOS/xamarin.forms.pria.ios.nuspec
nuget pack Xamarin.Forms.Pria.Android/xamarin.forms.pria.android.nuspec

# copy created packages to local NuGet repository
# cp *.nupkg ~/NuGet

# move created packages to local NuGet repository
mv *.nupkg $OUTPUT_DIR

#nuget setApiKey b58e5082-1b5f-4562-b7ce-281507c4cfaf
#nuget push *.nupkg

