﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Services;
using Xamarin.Forms.Pria.WP.Services;

[assembly: Dependency(typeof(VideoPlayerService))]
namespace Xamarin.Forms.Pria.WP.Services
{
    public class VideoPlayerService:IVideoPlayer
    {

        public void Play(Uri uri)
        {
            MediaPlayerLauncher mediaPlayerLauncher = new MediaPlayerLauncher();

            mediaPlayerLauncher.Media = uri;
            mediaPlayerLauncher.Location = MediaLocationType.None;
            mediaPlayerLauncher.Controls = MediaPlaybackControls.Pause | MediaPlaybackControls.Stop;
            mediaPlayerLauncher.Orientation = MediaPlayerOrientation.Landscape;

            mediaPlayerLauncher.Show();
        }
    }
}
