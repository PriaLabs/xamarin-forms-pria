﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Pria.WP.Services;

[assembly: Dependency(typeof(UrlService))]
namespace Xamarin.Forms.Pria.WP.Services
{
    public class UrlService : IUrlService
    {
        public void OpenUrl(string url)
        {
            WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri(url);
            task.Show(); 
        }


        public void OpenStore(string appId)
        {
            MarketplaceDetailTask task = new MarketplaceDetailTask();
            task.ContentIdentifier = appId;
            task.Show();
        }
    }
}
