﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Pria.WP.Services;

[assembly: Dependency(typeof(MapService))]
namespace Xamarin.Forms.Pria.WP.Services
{
    public class MapService : IMapService
    {
        public void ShowMap(string title, double latitude, double longitude, bool requestNavigation)
        {
            if (requestNavigation)
            {
                MapsDirectionsTask mapsDirectionsTask = new MapsDirectionsTask();
                LabeledMapLocation spaceNeedleLML = new LabeledMapLocation(title, new GeoCoordinate(latitude, longitude));
                mapsDirectionsTask.End = spaceNeedleLML;
                mapsDirectionsTask.Show();
            }
            else
            {
                MapsTask mapsTask = new MapsTask();
                mapsTask.Center = new GeoCoordinate(latitude, longitude);
                mapsTask.ZoomLevel = 10;
                mapsTask.Show();
            }
        }

        public void ShowMap(string address)
        {    
            MapsTask mapsTask = new MapsTask();
            mapsTask.SearchTerm = address;
            mapsTask.ZoomLevel = 10;
            mapsTask.Show();
        }
    }
}
