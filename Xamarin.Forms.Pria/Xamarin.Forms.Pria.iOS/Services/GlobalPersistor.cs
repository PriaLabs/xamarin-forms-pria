using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Foundation;
using Xamarin.Forms.Pria.iOS;

// iOS: pridat do Entitlement.plist key-value storage support a do provisioningu support pro iCloud

[assembly: Dependency(typeof(GlobalPersistor))]
namespace Xamarin.Forms.Pria.iOS
{
	public class GlobalPersistor : IGlobalPersistor
	{
		public GlobalPersistor ()
		{
		}

		#region IGlobalPersistor implementation

		public void Save (string key, string value)
		{
			NSUbiquitousKeyValueStore store = NSUbiquitousKeyValueStore.DefaultStore;
			store.SetString(key, value);
			store.Synchronize();
		}

		public string Load(string key)
		{
			NSUbiquitousKeyValueStore store = NSUbiquitousKeyValueStore.DefaultStore;
			return store.GetString(key);
		}

		#endregion
	}
}

