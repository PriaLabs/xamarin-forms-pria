using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms.Pria.Services;
using Xamarin.Forms.Pria.iOS.Services;
using ZXing.Mobile;

[assembly: Dependency(typeof(BarcodeService))]
namespace Xamarin.Forms.Pria.iOS.Services
{
	public class BarcodeService : IBarcodeService
	{
		public BarcodeService ()
		{
		}

		#region IBarcodeService implementation

		public bool IsAvailable ()
		{
			return UIImagePickerController.IsCameraDeviceAvailable (UIImagePickerControllerCameraDevice.Rear);
		}

		public async Task<BarcodeResult> Scan (CameraFacing cameraFacing)
		{
			var scanner = new ZXing.Mobile.MobileBarcodeScanner ();
			scanner.CancelButtonText = LocalizationManager.Instance.GetText ("Cancel");
			scanner.FlashButtonText = LocalizationManager.Instance.GetText ("Flash");
			var result = await scanner.Scan (new MobileBarcodeScanningOptions (){ UseFrontCameraIfAvailable = cameraFacing == CameraFacing.Front });
			if (result != null) {
				Console.WriteLine ("Scanned Barcode: " + result.Text);
				return new BarcodeResult (){ Code = result.Text, Type = (BarcodeType)result.BarcodeFormat };
			}
			return null;
		}

		#endregion
	}
}

