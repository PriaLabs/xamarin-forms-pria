using System;
using UIKit;
using Foundation;
using System.Text.RegularExpressions;
using MessageUI;
using AddressBook;
using AddressBookUI;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;

[assembly: Dependency(typeof(ContactService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class ContactService : IContactService
	{
		//UIApplicationDelegate appDelegate;

		public ContactService ()
		{
			
		}

		/*public ContactService (UIApplicationDelegate appDelegate)
		{
			this.appDelegate = appDelegate;
		}*/

		#region IContactService implementation

		public void CallNumber (string phoneNumber)
		{
			NSUrl urlToSend = new NSUrl ("tel:" + Regex.Replace (phoneNumber, @"\s+", string.Empty));

			if (UIApplication.SharedApplication.CanOpenUrl (urlToSend)) {
				UIApplication.SharedApplication.OpenUrl (urlToSend);
			} else {

			}
		}

		public void ComposeSms (string phoneNumber)
		{
			MFMessageComposeViewController messageComposer = new MFMessageComposeViewController ();
			messageComposer.Recipients = new string[]{ phoneNumber };
			messageComposer.Finished += (object sender, MFMessageComposeResultEventArgs e) => {
				UIApplication.SharedApplication.InvokeOnMainThread (() => {
					e.Controller.DismissViewController (true, null);
				});
			};

			UIViewController rootController = GetRootViewController ();
			if (rootController != null) {
				rootController.PresentViewController (messageComposer, true, null);
			}
		}

		public void ComposeEmail (string email, string subject, string body)
		{
			MFMailComposeViewController mailComposer = new MFMailComposeViewController ();
			mailComposer.SetToRecipients (new string[]{ email });
			mailComposer.SetSubject (subject);
			mailComposer.SetMessageBody (body, false);
			mailComposer.Finished += (object sender, MFComposeResultEventArgs e) => {
				UIApplication.SharedApplication.InvokeOnMainThread (() => {
					e.Controller.DismissViewController (true, null);
				});
			};

			UIViewController rootController = GetRootViewController ();
			if (rootController != null) {
				rootController.PresentViewController (mailComposer, true, null);
			}
		}

		public bool CanMakePhoneCalls ()
		{
			return UIApplication.SharedApplication.CanOpenUrl (new NSUrl ("tel://"));
		}

		public bool CanSendSMS ()
		{
			return MFMessageComposeViewController.CanSendText;
		}

		public void ImportVCard (string vCard)
		{
			if (vCard == null) {
				return;
			}

			NSError error;
			ABAddressBook addressBook = ABAddressBook.Create (out error);
			if (error == null) {
				if (ABAddressBook.GetAuthorizationStatus () == ABAuthorizationStatus.NotDetermined) {
					addressBook.RequestAccess ((bool granted, NSError err) => {
						if (granted) {
							// InvokeOnMainThread NOT TESTED
							UIApplication.SharedApplication.InvokeOnMainThread (() => {
								// this is not normally called on UI thread
								PresentSaveContactViewController (addressBook, CreateRecordFromVCard (vCard));
							});
						} else {
							// User denied access
							// Display an alert telling user the contact could not be added
						}
					});
				} else if (ABAddressBook.GetAuthorizationStatus () == ABAuthorizationStatus.Authorized) {
					PresentSaveContactViewController (addressBook, CreateRecordFromVCard (vCard));
				} else {
					// The user has previously denied access
					// Send an alert telling user to change privacy setting in settings app
				}
			}
		}

		#endregion

		private ABPerson CreateRecordFromVCard (string VCard)
		{
			if (VCard == null) {
				return null;
			}

			return null;

			/*VCard card = VCardReader.ParseText (VCard);

			// personal info
			ABPerson record = new ABPerson ();
			record.FirstName = card.GivenName ?? string.Empty;
			record.LastName = card.Surname ?? string.Empty;
			record.MiddleName = card.MiddleName ?? string.Empty;
			record.Prefix = card.Prefix ?? string.Empty;
			//record.Birthday = card.Birthday != null ? DateTimeToNSDate(card.Birthday) : string.Empty;

			// addresses
			ABMutableDictionaryMultiValue addresses = new ABMutableDictionaryMultiValue ();

			card.Addresses.ToList ().ForEach (a => {
				NSMutableDictionary addDict = new NSMutableDictionary ();
				addDict.Add (new NSString (ABPersonAddressKey.Country), new NSString (a.Country));
				addDict.Add (new NSString (ABPersonAddressKey.Street), new NSString (a.Street));
				addDict.Add (new NSString (ABPersonAddressKey.Zip), new NSString (a.Postcode));
				addDict.Add (new NSString (ABPersonAddressKey.Country), new NSString (a.Locality));

				addresses.Add (addDict, new NSString (string.Empty));
			}); 
			record.SetAddresses (addresses);

			// emails
			ABMutableStringMultiValue emails = new ABMutableStringMultiValue ();
			card.Emails.ToList ().ForEach (e => emails.Add (e.Address, new NSString ("e-mail")));
			record.SetEmails (emails);

			// phones
			ABMutableStringMultiValue phones = new ABMutableStringMultiValue ();
			card.Phones.ToList ().ForEach (p => phones.Add (p.Number, new NSString ("phone")));
			record.SetPhones (phones);

			return record;*/
		}

		private void PresentSaveContactViewController (ABAddressBook addressBook, ABPerson contact)
		{
			UIViewController rootController = GetRootViewController ();

			ABNewPersonViewController vc = new ABNewPersonViewController (){ DisplayedPerson = contact };

			vc.NewPersonComplete += (object sender, ABNewPersonCompleteEventArgs e) => {
				if (e.Completed) {
					addressBook.Add (contact);
					addressBook.Save ();
				}
				if (rootController != null) {
					rootController.DismissViewController (true, null);
				}
			};
			UINavigationController nav = new UINavigationController (vc);

			if (rootController != null) {
				rootController.PresentViewController (nav, true, null);
			}
		}

		private UIViewController GetRootViewController ()
		{
			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			while (topController.PresentedViewController != null) {
				topController = topController.PresentedViewController;
			}
			return topController;
		}

		private Foundation.NSDate DateTimeToNSDate (DateTime date)
		{
			return Foundation.NSDate.FromTimeIntervalSinceReferenceDate ((date - (new DateTime (2001, 1, 1, 0, 0, 0))).TotalSeconds);
		}
	}
}

