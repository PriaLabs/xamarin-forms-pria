﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using AVFoundation;
using Foundation;
using System.IO;

[assembly: Dependency(typeof(AudioService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class AudioService : IAudioService
	{
		AVAudioPlayer player;

		public AudioService ()
		{
		}

		#region IAudioService implementation

		public void PlayLocalFile (string filePath)
		{
			throw new NotImplementedException ("Implemented but not tested yet");

			//AVAudioPlayer player = AVAudioPlayer.FromUrl(NSUrl.FromFilename(filePath));
			//player.Play ();
		}

		public void PlayResourceFile (string resourceName)
		{
			string path = Path.Combine (NSBundle.MainBundle.BundlePath, resourceName);
			player = AVAudioPlayer.FromUrl (NSUrl.FromFilename (path));
			player.Play ();
		}

		#endregion
	}
}

