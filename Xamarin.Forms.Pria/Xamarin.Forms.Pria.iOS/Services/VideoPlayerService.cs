using System;
using Xamarin.Forms.Pria.Services;
using Xamarin.Forms.Pria.iOS;
using MediaPlayer;
using Foundation;
using Xamarin.Forms;
using UIKit;

[assembly: Dependency(typeof(VideoPlayerService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class VideoPlayerService : IVideoPlayer
	{
		MPMoviePlayerViewController controller;
		UIActivityIndicatorView spinner;
		NSObject loadStateObserver, didFinishObserver;

		public VideoPlayerService ()
		{
		}

		#region IVideoPlayer implementation

		public void Play (Uri uri)
		{
			controller = new MPMoviePlayerViewController(NSUrl.FromString(uri.ToString()));
			controller.MoviePlayer.ShouldAutoplay = true;

			spinner = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.WhiteLarge) {
				Center = controller.View.Center,
				HidesWhenStopped = true,
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins
			};
			spinner.StartAnimating ();
			controller.View.AddSubview (spinner);

			loadStateObserver = NSNotificationCenter.DefaultCenter.AddObserver (MPMoviePlayerController.LoadStateDidChangeNotification, LoadStateChanged);
			didFinishObserver = NSNotificationCenter.DefaultCenter.AddObserver (MPMoviePlayerController.PlaybackDidFinishNotification, DidFinished);

			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			while (topController.PresentedViewController != null) {
				topController = topController.PresentedViewController;
			}
			topController.PresentViewController (controller, true, null);
		}

		#endregion

		public void LoadStateChanged(NSNotification notification)
		{
			if (controller.MoviePlayer.LoadState != MPMovieLoadState.Stalled && controller.MoviePlayer.LoadState != MPMovieLoadState.Stalled) {
				spinner.StopAnimating ();
			} else {
				spinner.StartAnimating ();
			}
		}

		public void DidFinished(NSNotification notification)
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver (loadStateObserver);
			NSNotificationCenter.DefaultCenter.RemoveObserver (didFinishObserver);
		}
	}
}
	