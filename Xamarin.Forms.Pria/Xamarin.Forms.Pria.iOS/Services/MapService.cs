using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using CoreLocation;
using MapKit;
using Foundation;


[assembly: Dependency(typeof(MapService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class MapService:IMapService
	{
		#region IMapService implementation

		public void ShowMap (string title, double latitude, double longitude, bool requestNavigation)
		{
			CLLocationCoordinate2D endingCoord = new CLLocationCoordinate2D (latitude, longitude);
			MKPlacemark endLocation = new MKPlacemark (endingCoord,(NSDictionary) null);

			MKMapItem endItem = new MKMapItem (endLocation);
			endItem.Name = title;
			endItem.OpenInMaps ();
		}

		public void ShowMap (string address)
		{
			CLGeocoder geocoder = new CLGeocoder ();
			geocoder.GeocodeAddress (address, (CLPlacemark[] placemarks, NSError error) => {
				if(placemarks != null && placemarks.Length > 0){
					CLPlacemark topResult = placemarks[0];
					CLLocationCoordinate2D endingCoord = new CLLocationCoordinate2D (topResult.Location.Coordinate.Latitude, topResult.Location.Coordinate.Longitude);
					MKPlacemark placemark = new MKPlacemark(endingCoord,(NSDictionary)null);
					MKMapItem endItem = new MKMapItem (placemark);
					endItem.Name = address;
					endItem.OpenInMaps ();
				}
			});
		}

		#endregion


	}
}

