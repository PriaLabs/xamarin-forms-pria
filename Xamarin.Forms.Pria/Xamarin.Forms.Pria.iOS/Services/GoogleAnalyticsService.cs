﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using Foundation;
using Google.Analytics;

[assembly: Dependency(typeof(GoogleAnalyticsService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class GoogleAnalyticsService : IGoogleAnalyticsService
	{
		const string AllowTrackingKey = "AllowTracking";
		ITracker tracker;

		public GoogleAnalyticsService ()
		{
		}

		#region IGoogleAnalyticsService implementation

		public void Init(string trackingID, string appName)
		{
			//GAI.SharedInstance.Logger.SetLogLevel (GAILogLevel.Verbose);

			NSDictionary optionsDict = NSDictionary.FromObjectAndKey(new NSString("YES"), new NSString(AllowTrackingKey));
			NSUserDefaults.StandardUserDefaults.RegisterDefaults(optionsDict);

			Gai.SharedInstance.OptOut = !NSUserDefaults.StandardUserDefaults.BoolForKey(AllowTrackingKey);

			Gai.SharedInstance.DispatchInterval = 10;
			Gai.SharedInstance.TrackUncaughtExceptions = true;
			//GAI.SharedInstance.DryRun = true;

			tracker = Gai.SharedInstance.GetTracker(appName, trackingID);
			tracker.SetAllowIdfaCollection (true);
		}

		public void TrackPage (string pageNameToTrack)
		{
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, pageNameToTrack);
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public void TrackEvent (string category, string action, string label)
		{
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateEvent(category, action, label, null).Build());
			Gai.SharedInstance.Dispatch(); // Manually dispatch the event immediately
		}

		public void TrackException (string exceptionMessageToTrack, bool isFatalException)
		{
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateException(exceptionMessageToTrack, isFatalException).Build());
		}

		#endregion
	}
}

