using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using System.IO;
using UIKit;
using CoreGraphics;
using Foundation;

[assembly: Dependency(typeof(ImageResizeService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class ImageResizeService : IImageResizeService
	{
		#region IImageResizeService implementation

		public Stream ResizeImage (MediaFile mediaFile, int maxPixelDimension)
		{
			byte[] imageData = StreamUtils.GetDataFromStream (mediaFile.Source);
			UIImage originalImage = UIImage.LoadFromData (NSData.FromArray (imageData));

			int newWidth, newHeight;
			MediaUtils.ResizeBasedOnPixelDimension(maxPixelDimension,mediaFile.Exif.Width,mediaFile.Exif.Height,out newWidth, out newHeight);

			Stream s = new MemoryStream ();
			using(UIImage smallImage = ResizeImage(originalImage,newWidth,newHeight)){
				using (NSData data = smallImage.AsJPEG()){
					using (Stream stream = data.AsStream()){
						stream.CopyTo(s);
						s.Seek (0, SeekOrigin.Begin);
					}
				}
			}

			return s;
		}

		#endregion

		private UIImage ResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
		{
			var sourceSize = sourceImage.Size;
			var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
			if (maxResizeFactor > 1) return sourceImage;
			var width = maxResizeFactor * sourceSize.Width;
			var height = maxResizeFactor * sourceSize.Height;
			UIGraphics.BeginImageContext(new CGSize((float)width, (float)height));
			sourceImage.Draw(new CGRect(0, 0, (float)width, (float)height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

	}
}

