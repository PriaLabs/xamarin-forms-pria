using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using UIKit;
using QuickLook;
using Foundation;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using CoreGraphics;
using System.Linq;

[assembly: Dependency(typeof(PDFService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class PDFService : IPDFService 
	{
		UIActivityIndicatorView spinner;

		#region IPDFService implementation

		public void OpenLocalPDF (string path, string title = null)
		{
			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			while (topController.PresentedViewController != null) {
				topController = topController.PresentedViewController;
			}

			UIViewController controller = new UIViewController ();
			controller.View.AutosizesSubviews = true;

			UIWebView webView = new UIWebView (controller.View.Bounds) {
				ScalesPageToFit = true,
				//BackgroundColor = UIColor.Clear,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
			};
			NSData data = NSData.FromFile (path);
			webView.LoadData (data, "application/pdf", "UTF-8", NSUrl.FromFilename(path));

			controller.View.BackgroundColor = UIColor.Clear;
			controller.View.Add (webView);

			UINavigationController nc = new UINavigationController (controller);
			nc.NavigationBar.Translucent = false;
			nc.TopViewController.NavigationItem.Title = title ?? "PDF";
			nc.TopViewController.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIImage.FromBundle ("Icons/close.png"), UIBarButtonItemStyle.Done, (object sender, EventArgs e) => {
				topController.DismissViewController(true,null);
			});
				
		
			topController.PresentViewController (nc, true, null);
		}
			
		public async Task OpenWebPDF (string url, string title = null)
		{
			UIViewController rootController = UIApplication.SharedApplication.Windows [0].RootViewController;

			UIViewController controller = new UIViewController ();
			controller.View.AutosizesSubviews = true;
			controller.View.BackgroundColor = UIColor.Clear;

			NSString urlEscaped = new NSString (url).CreateStringByAddingPercentEscapes (NSStringEncoding.UTF8);

			UIWebView webView = new UIWebView (controller.View.Bounds) {
				ScalesPageToFit = true,
				//BackgroundColor = UIColor.Clear,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
			};
			webView.LoadRequest (NSUrlRequest.FromUrl (NSUrl.FromString(urlEscaped)));
			webView.LoadFinished += WebView_LoadFinished;
			webView.LoadError += WebView_LoadError;

			spinner = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
				Center = controller.View.Center,
				HidesWhenStopped = true,
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins
			};
			spinner.StartAnimating ();

			controller.View.Add (webView);
			controller.View.Add (spinner);

			UINavigationController nc = new UINavigationController (controller);
			NavigationBarTheme.StyleNavigationBar (nc.NavigationBar);

			nc.TopViewController.NavigationItem.Title = title ?? "PDF";
			nc.TopViewController.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIImage.FromBundle ("Icons/close.png"), UIBarButtonItemStyle.Done, (object sender, EventArgs e) => rootController.DismissViewController (true, null));

			rootController.PresentViewController (nc, true, null);
		}

		void WebView_LoadError (object sender, UIWebErrorArgs e)
		{
			spinner.StopAnimating ();
			((UIWebView)sender).LoadFinished -= WebView_LoadFinished;
			((UIWebView)sender).LoadError -= WebView_LoadError;
		}

		void WebView_LoadFinished (object sender, EventArgs e)
		{
			spinner.StopAnimating ();
			((UIWebView)sender).LoadFinished -= WebView_LoadFinished;
			((UIWebView)sender).LoadError -= WebView_LoadError;
		}

		#endregion

	}
}

