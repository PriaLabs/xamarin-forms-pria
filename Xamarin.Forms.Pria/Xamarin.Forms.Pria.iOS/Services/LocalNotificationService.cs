using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using Foundation;
using ObjCRuntime;

[assembly: Dependency(typeof(LocalNotificationService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class LocalNotificationService : ILocalNotificationService
	{
		public LocalNotificationService ()
		{
		}

		public void RequestLocalNotifications ()
		{
 			if (UIApplication.SharedApplication.RespondsToSelector (new Selector ("registerUserNotificationSettings:"))) {
				var settings = UIUserNotificationSettings.GetSettingsForTypes (UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, new NSSet ());
				UIApplication.SharedApplication.RegisterUserNotificationSettings (settings);
			}
		}

		public void RequestRemoteNotifications ()
		{
			// iOS 8.0
			if (UIApplication.SharedApplication.RespondsToSelector (new Selector ("registerForRemoteNotifications")))
			{
				UIApplication.SharedApplication.RegisterForRemoteNotifications ();
			} 
			// iOS 7.0: NOT TESTED (need device with iOS 7)
			else
			{
				const UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
				UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
			}
		}

		public void Notify(string title,string text,string action,string userData = null,string soundName = null)
		{
			UILocalNotification notification = new UILocalNotification();
			notification.FireDate = DateTime.Now.DateTimeToNSDate (); //System.DateTimeUtils.DateTimeToNSDate (DateTime.SpecifyKind (DateTime.Now, DateTimeKind.Utc));
			notification.TimeZone = NSTimeZone.FromName("UTC");
			notification.AlertBody = text;
			notification.AlertAction = action;
			notification.SoundName =  !string.IsNullOrEmpty(soundName) ? soundName : UILocalNotification.DefaultSoundName;
			if(userData != null){
				notification.UserInfo = NSDictionary.FromObjectAndKey(new NSString(userData), new NSString("UserData"));
			}

			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
		}

	}
}

