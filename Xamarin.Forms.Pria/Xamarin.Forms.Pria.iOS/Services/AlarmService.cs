using System;
using Xamarin.Forms;
using UIKit;
using System.Collections.Generic;
using Foundation;
using Xamarin.Forms.Pria.Services;
using Xamarin.Forms.Pria.iOS.Services;
using System.Linq;

[assembly:Dependency(typeof(AlarmService))]
namespace Xamarin.Forms.Pria.iOS.Services
{
	public class AlarmService : IAlarmService
	{
		#region IAlarmService implementation

		public void RegisterNotifications (List<LocalNotification> notifications)
		{
			notifications.ForEach (n => {

				UILocalNotification notification = new UILocalNotification();
				notification.FireDate = n.Date.DateTimeToNSDate ();
				notification.TimeZone = NSTimeZone.FromName("UTC");
				notification.AlertAction = n.AlertAction;
				notification.AlertBody = n.Body;
				notification.SoundName = !string.IsNullOrEmpty(n.SoundName) ? n.SoundName : UILocalNotification.DefaultSoundName;

				NSMutableDictionary dictionary = new NSMutableDictionary();
				if(n.UserData != null){
					dictionary.SetValueForKey(NSObject.FromObject(n.UserData), new NSString("UserData"));
				}
				if(n.Id != null){
					dictionary.SetValueForKey(NSObject.FromObject(n.Id), new NSString("Id"));
				}
				notification.UserInfo = dictionary;

				UIApplication.SharedApplication.ScheduleLocalNotification(notification);
			});
		}

		public void ClearAll ()
		{
			UIApplication.SharedApplication.CancelAllLocalNotifications();
		}

		public void ClearNotification(LocalNotification notification)
		{
			UILocalNotification[] scheduled = UIApplication.SharedApplication.ScheduledLocalNotifications;
			if (scheduled != null && scheduled.Count () > 0) {
				UILocalNotification not = scheduled.ToList ().Find (n => n.UserInfo.ObjectForKey (new NSString ("Id")).ToString () == notification.Id);
				if (not != null) {
					UIApplication.SharedApplication.CancelLocalNotification (not);
				}
			}
		}

		#endregion
	}
}

