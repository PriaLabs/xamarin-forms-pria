using System;
using Xamarin.Forms.Pria.iOS;
using Xamarin.Forms;
using Foundation;
using System.Collections.Generic;
using UIKit;

[assembly: Dependency(typeof(ShareService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class ShareService:IShareService
	{
		#region IShareService implementation

		public void Share (string subject, string description, string url)
		{
			List<NSObject> dataToShare = new List<NSObject> () {new NSString(subject),new NSString(description), new NSUrl(url)};

			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			while (topController.PresentedViewController != null) {
				topController = topController.PresentedViewController;
			}

			UIActivityViewController activityViewController = new UIActivityViewController(dataToShare.ToArray(), null);
			topController.PresentViewController(activityViewController, true, null);
		}

		#endregion


	}
}

