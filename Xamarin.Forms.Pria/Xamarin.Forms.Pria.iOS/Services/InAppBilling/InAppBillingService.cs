using System;
using StoreKit;
using System.Threading.Tasks;
using System.Collections.Generic;
using Foundation;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;
using System.Net;

[assembly: Dependency(typeof(InAppBillingService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class InAppBillingService : IInAppBilling
	{
		InAppPurchaseManager iap;

		public InAppBillingService ()
		{
			Init ();
		}


		ManualResetEvent purchaseProductResetEvent = new ManualResetEvent (false);
		Purchase productPurchaseResult = null;
		public void OnPurchaseFinished (Purchase purchase)
		{
			productPurchaseResult = purchase;
			purchaseProductResetEvent.Set ();
		}


		ManualResetEvent fetchProductResetEvent = new ManualResetEvent (false);
		List<Product> fetchedProducts = null;
		public void OnFetchProductsFinished(List<Product> products)
		{
			fetchedProducts = products;
			fetchProductResetEvent.Set ();
		}


		List<Purchase> purchasedProducts = null;
		ManualResetEvent manualRestoreResetEvent = new ManualResetEvent (false);
		public void OnRestoreFinished (List<Purchase> products)
		{
			purchasedProducts = products;
			manualRestoreResetEvent.Set ();
		}

		public void RequestFailed(NSError error)
		{
			fetchProductResetEvent.Set ();
		}

		#region IInAppBilling implementation

		public void Init ()
		{
			iap = new InAppPurchaseManager (this);
		}

		public bool IsAvailable ()
		{
			return iap.CanMakePayments ();
		}

		public bool SupportsManualRestore ()
		{
			return true;
		}

		public Task<Purchase> PurchaseProduct (Product product)
		{
			return Task.Factory.StartNew<Purchase> (delegate {
				purchaseProductResetEvent = new ManualResetEvent (false);

				iap.PurchaseProduct (product.ID);

				purchaseProductResetEvent.WaitOne();

				return productPurchaseResult;
			});
		}

		public Task<Purchase> PurchaseSubscription (Product product)
		{
			return Task.Factory.StartNew<Purchase> (delegate {
				purchaseProductResetEvent = new ManualResetEvent (false);

				iap.PurchaseProduct (product.ID);

				purchaseProductResetEvent.WaitOne();

				return productPurchaseResult;
			});
		}

		public Task<List<Product>> QueryInventoryForSubscriptions (List<string> productIds)
		{
			throw new NotImplementedException ();
		}

		public Task<List<Product>> QueryInventoryForProducts (List<string> productIds)
		{
			return Task.Factory.StartNew<List<Product>> (delegate {
				fetchProductResetEvent = new ManualResetEvent (false);

				iap.RequestProductData (productIds);
			
				fetchProductResetEvent.WaitOne();

				return fetchedProducts;
			});
		}

		public Task<List<Purchase>> GetPurchasedProducts ()
		{
			return Task.Factory.StartNew<List<Purchase>> (delegate {

				return RestorePurchases().Result;

			});
		}

		public Task<List<Purchase>> GetPurchasedSubscriptions ()
		{
			throw new NotImplementedException ();
		}

		public Task<List<Purchase>> RestorePurchases ()
		{
			return Task.Factory.StartNew<List<Purchase>> (() => {
				manualRestoreResetEvent = new ManualResetEvent (false);

				iap.Restore ();

				manualRestoreResetEvent.WaitOne();

				return purchasedProducts;
			});
		}

		#endregion
	}
}

