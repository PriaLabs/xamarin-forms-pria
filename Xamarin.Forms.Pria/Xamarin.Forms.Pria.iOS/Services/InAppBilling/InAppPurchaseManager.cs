using System;
using StoreKit;
using Foundation;
using System.Collections.Generic;
using System.Linq;

namespace Xamarin.Forms.Pria.iOS
{
	public class InAppPurchaseManager : SKProductsRequestDelegate
	{
		SKProductsRequest productsRequest;
		CustomPaymentObserver theObserver;

		public List<Purchase> PurchasedProducts{ get; private set; }
		private const string userDefaultsKeyPrefix = "xamarin.forms.pria.ios.inappbilling.";

		public static Action Done {get;set;}
		public InAppBillingService Service{ get; private set; }

		public InAppPurchaseManager (InAppBillingService service)
		{
			Service = service;
			theObserver = new CustomPaymentObserver(this);
			SKPaymentQueue.DefaultQueue.AddTransactionObserver(theObserver);
		}

		// Verify that the iTunes account can make this purchase for this application
		public bool CanMakePayments()
		{
			return SKPaymentQueue.CanMakePayments;	
		}

		// request multiple products at once
		public void RequestProductData (List<string> productIds)
		{
			if (productIds == null) {
				Console.WriteLine("RequestProductData: productIds is null");
				Service.OnFetchProductsFinished (null);
				return;
			}

			var array = new NSString[productIds.Count];
			for (var i = 0; i < productIds.Count; i++) {
				array[i] = new NSString(productIds[i]);
			}
			NSSet productIdentifiers = NSSet.MakeNSObjectSet<NSString>(array);			

			//set up product request for in-app purchase
			productsRequest  = new SKProductsRequest(productIdentifiers);
			productsRequest.Delegate = this; // SKProductsRequestDelegate.ReceivedResponse
			productsRequest.Start();
		}

		// received response to RequestProductData - with price,title,description info
		public override void ReceivedResponse (SKProductsRequest request, SKProductsResponse response)
		{
			if (request == null) {
				Console.WriteLine("ReceivedResponse: request is null");
				Service.OnFetchProductsFinished (null);
				return;
			}

			if (response == null) {
				Console.WriteLine("ReceivedResponse: response is null");
				Service.OnFetchProductsFinished (null);
				return;
			}
				
			List<Product> products = response.Products.ToList ().ConvertAll (p => new Product () {
				ID = p.ProductIdentifier,
				Title = p.LocalizedTitle,
				Description = p.LocalizedDescription,
				Price = FormatCurrencyString(p)
			});



			/*foreach (string invalidProductId in response.InvalidProducts) {
				Console.WriteLine("Invalid product id: " + invalidProductId );
			}*/

			Service.OnFetchProductsFinished (products);
		}

		private string FormatCurrencyString(SKProduct product)
		{
			NSNumberFormatter formatter = new NSNumberFormatter () {
				NumberStyle = NSNumberFormatterStyle.Currency,
				Locale = product.PriceLocale
			};
			return formatter.StringFromNumber (product.Price);
		}

		public void PurchaseProduct(string appStoreProductId)
		{
			Console.WriteLine("PurchaseProduct " + appStoreProductId);

			SKPayment payment = SKPayment.PaymentWithProduct (appStoreProductId);	
			SKPaymentQueue.DefaultQueue.AddPayment (payment);
		}

		public void CompleteTransaction (SKPaymentTransaction transaction)
		{
			if (transaction == null) {
				Console.WriteLine("CompleteTransaction: transaction is null");
				FinishTransaction (transaction, false);
				return;
			}

			Console.WriteLine ("CompleteTransaction " + transaction.TransactionIdentifier);

			// Register the purchase, so it is remembered for next time
			SetPurchased (transaction.Payment.ProductIdentifier);

			FinishTransaction (transaction, true);
		}

		public void RestoreTransaction (SKPaymentTransaction transaction)
		{
			if (transaction == null) {
				Console.WriteLine("RestoreTransaction: transaction is null");
				FinishTransaction(transaction, false);
				return;
			}

			// Restored Transactions always have an 'original transaction' attached
			Console.WriteLine("RestoreTransaction " + transaction.TransactionIdentifier + "; OriginalTransaction " + transaction.OriginalTransaction.TransactionIdentifier);

			// Register the purchase, so it is remembered for next time
			SetPurchased(transaction.OriginalTransaction.Payment.ProductIdentifier); // it's as though it was purchased again

			PurchasedProducts.Add (new Purchase (){ ID = transaction.OriginalTransaction.Payment.ProductIdentifier });

			//Service.OnRestoreFinished (true);

			FinishTransaction(transaction, true);
		}

		public void FailedTransaction (SKPaymentTransaction transaction)
		{
			if (transaction == null) {
				Console.WriteLine("FailedTransaction: transaction is null");
				//FinishTransaction (transaction, false);
				return;
			}
			if (transaction.Error == null) {
				Console.WriteLine("FailedTransaction: transaction.Error is null");
				FinishTransaction (transaction, false);
				return;
			}

			//SKErrorPaymentCancelled == 2
			if (transaction.Error.Code == 2) // user cancelled
				Console.WriteLine("User CANCELLED FailedTransaction Code=" + transaction.Error.Code + " " + transaction.Error.LocalizedDescription);
			else // error!
				Console.WriteLine("FailedTransaction Code=" + transaction.Error.Code + " " + transaction.Error.LocalizedDescription);

			FinishTransaction (transaction, false);
		}

		public void FinishTransaction(SKPaymentTransaction transaction, bool wasSuccessful)
		{
			if (transaction == null) {
				Console.WriteLine("FinishTransaction: transaction is null");
				Service.OnPurchaseFinished (null);
				return;
			}

			Console.WriteLine("FinishTransaction " + wasSuccessful);
			// remove the transaction from the payment queue.
			SKPaymentQueue.DefaultQueue.FinishTransaction(transaction);		// THIS IS IMPORTANT - LET'S APPLE KNOW WE'RE DONE !!!!
			Service.OnPurchaseFinished (wasSuccessful ? new Purchase (){ ID = transaction.Payment.ProductIdentifier,Token = transaction.TransactionIdentifier } : null);
		}

		/// <summary>
		/// Probably could not connect to the App Store (network unavailable?)
		/// </summary>
		public override void RequestFailed (SKRequest request, NSError error)
		{
			if (request == null) {
				Console.WriteLine("RequestFailed: request is null");
				return;
			}

			Console.WriteLine (" ** InAppPurchaseManager RequestFailed() " + error.LocalizedDescription);

			Service.RequestFailed (error);
		}

		/// <summary>
		/// Restore any transactions that occurred for this Apple ID, either on 
		/// this device or any other logged in with that account.
		/// </summary>
		public void Restore()
		{
			Console.WriteLine (" ** InAppPurchaseManager Restore()");

			PurchasedProducts = new List<Purchase> ();

			// theObserver will be notified of when the restored transactions start arriving <- AppStore
			SKPaymentQueue.DefaultQueue.RestoreCompletedTransactions();			
		}

		private static void SetPurchased (string productId)
		{
			var key = new NSString(productId);
			NSUserDefaults.StandardUserDefaults.SetBool(true, userDefaultsKeyPrefix + key);
			NSUserDefaults.StandardUserDefaults.Synchronize ();
		}

		public static bool HasPurchased (string productId)
		{
			var key = new NSString(userDefaultsKeyPrefix + productId);
			bool result = NSUserDefaults.StandardUserDefaults.BoolForKey (key);
			return result;
		}

		/*
		public List<Purchase> GetPurchasedProducts()
		{
			List<Purchase> products = new List<Purchase> ();
			foreach (NSString key in NSUserDefaults.StandardUserDefaults.AsDictionary().Keys) {
				if (key.Contains(new NSString(userDefaultsKeyPrefix)) && NSUserDefaults.StandardUserDefaults.BoolForKey (key)) {
					products.Add (new Purchase (){ ID = key.ToString ().Replace (userDefaultsKeyPrefix, string.Empty) });
				}
			}
			return products;
		}
		*/
	}
}

