using System;
using System.Linq;
using StoreKit;
using Foundation;
using UIKit;

namespace Xamarin.Forms.Pria.iOS
{
	internal class CustomPaymentObserver : SKPaymentTransactionObserver
	{
		private InAppPurchaseManager theManager;

		public CustomPaymentObserver (InAppPurchaseManager manager)
		{
			theManager = manager;
		}

		// called when the transaction status is updated
		public override void UpdatedTransactions (SKPaymentQueue queue, SKPaymentTransaction[] transactions)
		{
			try {
				Console.WriteLine ("UpdatedTransactions");
				foreach (SKPaymentTransaction transaction in transactions) {
					switch (transaction.TransactionState) {
					case SKPaymentTransactionState.Purchased:
						theManager.CompleteTransaction (transaction);
						break;
					case SKPaymentTransactionState.Failed:
						theManager.FailedTransaction (transaction);
						break;
					case SKPaymentTransactionState.Restored:
						theManager.RestoreTransaction (transaction);
						break;
					default:
						break;
					}
				}
			} catch (Exception ex) {
				Console.WriteLine (ex.ToString());
			}
		}

		public override void PaymentQueueRestoreCompletedTransactionsFinished (SKPaymentQueue queue)
		{
			// Restore succeeded
			Console.WriteLine (" ** RESTORE PaymentQueueRestoreCompletedTransactionsFinished ");

			theManager.Service.OnRestoreFinished (theManager.PurchasedProducts);
		}

		public override void RestoreCompletedTransactionsFailedWithError (SKPaymentQueue queue, NSError error)
		{
			// Restore failed somewhere...
			Console.WriteLine (" ** RESTORE RestoreCompletedTransactionsFailedWithError " + error.LocalizedDescription);

			theManager.Service.OnRestoreFinished (null);
		}
	}
}

