using System;
using UIKit;
using Foundation;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS;

[assembly: Dependency(typeof(UrlService))]
namespace Xamarin.Forms.Pria.iOS
{
	public class UrlService : IUrlService
	{
		public UrlService ()
		{
		}

		#region IUrlService implementation

		public void OpenUrl (string url)
		{
			if (UIApplication.SharedApplication.CanOpenUrl (NSUrl.FromString (url))) {
				UIApplication.SharedApplication.OpenUrl (NSUrl.FromString (url));
			}
		}

		public void OpenStore(string appId)
		{
			NSUrl url = NSUrl.FromString (string.Format ("itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id={0}", appId));
			if (UIApplication.SharedApplication.CanOpenUrl (url)) {
				UIApplication.SharedApplication.OpenUrl (url);
			}
		}

		#endregion
	}
}

