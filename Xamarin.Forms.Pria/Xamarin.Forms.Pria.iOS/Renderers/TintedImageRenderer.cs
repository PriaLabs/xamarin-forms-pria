﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.iOS;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer (typeof (TintedImage), typeof (TintedImageRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class TintedImageRenderer:ImageRenderer
	{
		class CustomUIImageView:UIImageView{
			public CustomUIImageView (CGRect frame) : base (frame)
			{
			}
			
			public override UIImage Image {
				get {
					return base.Image;
				}
				set {
					base.Image = value?.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				}
			}

		}

		public TintedImageRenderer ()
		{
		}


		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			if (base.Control == null) {
				base.SetNativeControl (new CustomUIImageView (CGRect.Empty) {
					ContentMode = UIViewContentMode.ScaleAspectFit,
					ClipsToBounds = true
				});
			}
			base.OnElementChanged (e);

			UpdateTintColor ();
		}
		private void UpdateTintColor(){
			if (Element!=null) {
				Control.TintColor = ((TintedImage)Element).TintColor.ToUIColor ();
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == TintedImage.TintColorProperty.PropertyName) {
				UpdateTintColor ();
			}
		}
	}
}

