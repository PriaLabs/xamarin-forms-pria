﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;

/// <summary>
/// Just for example - copy this code to a renderer in a specific project
/// TODO: introduce some better solution than copy-paste code
/// </summary>
//[assembly: ExportRenderer (typeof (DateTimePicker), typeof (DateTimePickerRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class DateTimePickerRenderer : DatePickerRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<DatePicker> e)
		{
			base.OnElementChanged (e);

			DateTimePicker elementPicker = ((DateTimePicker)Element);

			if (Control != null && elementPicker != null)
			{
				Control.Text = elementPicker.DateTime.HasValue ? elementPicker.DateTime.Value.ToString (Element.Format) : null;
				Control.TextAlignment = UITextAlignment.Center;
				Control.Placeholder = elementPicker.Placeholder;

				UIDatePicker picker = ((UIDatePicker)Control.InputView);
				picker.Mode = UIDatePickerMode.DateAndTime;
				picker.ValueChanged += (sender, args) => {
					if (elementPicker != null) {
						elementPicker.DateTime = ((UIDatePicker)sender).Date.ToDateTime ();
						Control.Text = elementPicker.DateTime.HasValue ? elementPicker.DateTime.Value.ToString (Element.Format) : null;
					}
				};

				UIBarButtonItem closeButton = new UIBarButtonItem (
					                              elementPicker.CloseButtonText ?? LocalizationManager.Instance.GetText ("OK"), 
					                              UIBarButtonItemStyle.Plain, 
					                              ((sender, args) => {
						elementPicker.DateTime = ((UIDatePicker)Control.InputView).Date.ToDateTime ();
						Control.ResignFirstResponder ();
					}));
				
				closeButton.SetTitleTextAttributes (new UITextAttributes () { TextColor = elementPicker.TintColor.ToUIColor (), }, UIControlState.Normal);

				((UIToolbar)Control.InputAccessoryView).SetItems (new [] {
					new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
					closeButton
				}, false);
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == DateTimePicker.DateTimeProperty.PropertyName)
			{
				DateTimePicker elementPicker = ((DateTimePicker)Element);
				Control.Text = elementPicker.DateTime.HasValue ? elementPicker.DateTime.Value.ToString (Element.Format) : null;
			}
		}
	}
}

