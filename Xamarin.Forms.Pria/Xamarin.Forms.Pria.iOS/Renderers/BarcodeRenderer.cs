﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.iOS;
using Foundation;

[assembly: ExportRenderer (typeof (BarcodeView), typeof (BarcodeRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class BarcodeRenderer:ViewRenderer<BarcodeView,UIImageView>
	{
		public BarcodeRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<BarcodeView> e)
		{
			if (Control == null) {
				SetNativeControl (new UIImageView ());

				Control.ContentMode = UIViewContentMode.ScaleAspectFit;
			}

			base.OnElementChanged (e);
			if (e.OldElement == null) {
				UpdateBarcode ();
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (e.PropertyName == BarcodeView.BarcodeProperty.PropertyName || e.PropertyName == BarcodeView.BarcodeTypeProperty.PropertyName) {
				UpdateBarcode ();
			}
		}

		private void UpdateBarcode(){
			Control.Image = null;

			if (!string.IsNullOrEmpty (Element.Barcode)) {
				try{
					ZXing.Mobile.BarcodeWriter writer = new ZXing.Mobile.BarcodeWriter ();
					writer.Format = (ZXing.BarcodeFormat)Element.BarcodeType;
					writer.Options.Width = 800;
					writer.Options.Height = 600;
					Control.Image = writer.Write(Element.Barcode);
				}catch(Exception ex){
					Log.LogException (ex);
				}
			}				
		}
	}
}

