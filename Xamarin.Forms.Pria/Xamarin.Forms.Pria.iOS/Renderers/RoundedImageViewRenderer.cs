using System;
using Xamarin.Forms;
using NutritionAdvisor.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms.Pria;
using CoreGraphics;

[assembly: ExportRenderer (typeof (RoundedImageView), typeof (RoundedImageViewRenderer))]
namespace NutritionAdvisor.iOS
{
	

	public class RoundedImageViewRenderer : ImageRenderer
	{
		
		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			

			base.OnElementChanged (e);

			if (e.OldElement == null)
			{
				Control.Layer.CornerRadius = ((RoundedImageView)Element).Radius;
			}
		}
		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
		}
	}
}

