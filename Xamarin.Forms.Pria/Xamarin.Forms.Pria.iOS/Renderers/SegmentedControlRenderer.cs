﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly:ExportRenderer(typeof(SegmentedControl), typeof(SegmentedControlRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class SegmentedControlRenderer : ViewRenderer<SegmentedControl, UISegmentedControl>
	{
		protected override void OnElementChanged (ElementChangedEventArgs<SegmentedControl> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null)
			{
				UISegmentedControl segmentedControl = new UISegmentedControl ();
				segmentedControl.TintColor = Element.TintColor.ToUIColor ();
				segmentedControl.BackgroundColor = Element.BackgroundColor.ToUIColor ();

				for (var i = 0; i < Element.Children.Count; i++) {
					segmentedControl.InsertSegment (Element.Children [i].Text, i, false);
				}

				segmentedControl.ValueChanged += (sender, eventArgs) => {
					Element.SelectedIndex = (int)segmentedControl.SelectedSegment;
				};

				segmentedControl.SelectedSegment = Element.SelectedIndex;

				SetNativeControl (segmentedControl);
			}
		}
	}
}

