﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.iOS;

[assembly: ExportRenderer (typeof (RoundedContentView), typeof (RoundedContentRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class RoundedContentRenderer : ViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null)
			{
				SetStyle ();
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == VisualElement.BackgroundColorProperty.PropertyName)
			{
				SetStyle ();
			}
		}

		void SetStyle()
		{
			NativeView.Layer.CornerRadius = ((RoundedContentView)Element).Radius;
			NativeView.BackgroundColor = Element.BackgroundColor.ToUIColor ();
		}
	}
}
	