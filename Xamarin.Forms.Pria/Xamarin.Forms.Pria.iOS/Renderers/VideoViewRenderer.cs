using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using UIKit;
using CoreGraphics;
using System.ComponentModel;
using System.IO;
using Foundation;
using MediaPlayer;
using Xamarin.Forms.Pria.Views;
using Xamarin.Forms.Pria.iOS;

[assembly: ExportRenderer (typeof (VideoView), typeof (VideoViewRenderer))]

namespace Xamarin.Forms.Pria.iOS
{
	public class VideoViewRenderer : ViewRenderer<VideoView, UIView>
	{
		MPMoviePlayerController moviePlayer;
		protected override void OnElementChanged (ElementChangedEventArgs<VideoView> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) {

				moviePlayer = new MPMoviePlayerController ();
				moviePlayer.ShouldAutoplay = Element.AutoPlay;
				moviePlayer.RepeatMode = Element.Repeat ? MPMovieRepeatMode.One : MPMovieRepeatMode.None;
				moviePlayer.AllowsAirPlay = true;

				NSUrl u = NSUrl.FromString (Uri.EscapeUriString( Element.Source.ToString ()));

				moviePlayer.ContentUrl = u;
				moviePlayer.PrepareToPlay ();

				SetNativeControl (moviePlayer.View);

			}

		}

		public override void RemoveFromSuperview ()
		{
			base.RemoveFromSuperview ();

			if(moviePlayer!=null){
				moviePlayer.Stop (); // :-( not happy but necessary
			}
		}
	}

}

