﻿using System;
using System.Linq;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using MapKit;
using Xamarin.Forms;
using CoreLocation;
using CoreGraphics;
using Xamarin.Forms.Pria;
using Xamarin.Forms.Pria.iOS;

[assembly: ExportRenderer(typeof(ExtendedMapView), typeof(ExtendedMapViewRenderer))]
namespace Xamarin.Forms.Pria.iOS
{
	public class ExtendedMapViewRenderer:ViewRenderer<ExtendedMapView,MKMapView>,IMKMapViewDelegate
	{
		public class CustomRouteOverlay:MKOverlay{
			ExtendedMapPolyline polyline;
			public ExtendedMapPolyline Polyline{
				get{ return polyline; }
				set{
					polyline = value;
				}
			}
			public MKPolyline MKPolyline{
				get{
					return MKPolyline.FromCoordinates (Polyline.Points.ConvertAll (point => new CLLocationCoordinate2D (point.Latitude, point.Longitude)).ToArray ());
				}
			}

			#region implemented abstract members of MKAnnotation

			public override CLLocationCoordinate2D Coordinate {
				get {
					return MKPolyline.Coordinate;
				}
			}

			#endregion

			#region implemented abstract members of MKOverlay

			public override MKMapRect BoundingMapRect {
				get {
					return MKPolyline.BoundingMapRect;
				}
			}

			#endregion
		}

		public class CustomAnnotation:MKPointAnnotation{
			ExtendedMapPin pin;
			public ExtendedMapPin Pin{
				get{
					return pin;
				}
				set{
					if (pin != null) {
						pin.PropertyChanged-=PinPropertyChanged;
					}
					pin = value;
					pin.PropertyChanged+=PinPropertyChanged;

					UpdateAnnotation ();
				}
			}

			void PinPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
			{
				UpdateAnnotation ();
			}

			private void UpdateAnnotation(){
				Title = pin.Title;
				Subtitle = pin.Subtitle;

				Coordinate = new CoreLocation.CLLocationCoordinate2D (pin.Position.Latitude, pin.Position.Longitude);
			}
		}

		public class CustomMKMapView:MKMapView{
			public ExtendedMapViewRenderer Renderer {get;private set;}

			public CustomMKMapView (ExtendedMapViewRenderer renderer)
			{
				this.Renderer = renderer;
			}
		}

		public class CustomMKMapViewDelegate:MKMapViewDelegate
		{
			public ExtendedMapViewRenderer Renderer{ get; private set;}

			const int imageSize = 42;
			bool firstUpdate = true;

			public CustomMKMapViewDelegate (ExtendedMapViewRenderer renderer)
			{
				this.Renderer = renderer;
			}

			public override void DidUpdateUserLocation (MKMapView mapView, MKUserLocation userLocation)
			{
				if (firstUpdate)
				{
					firstUpdate = false;
					MKCoordinateRegion region = new MKCoordinateRegion (userLocation.Coordinate, new MKCoordinateSpan (0.2, 0.2));
					mapView.SetRegion (region, true);
				}
			}

			public override MKAnnotationView GetViewForAnnotation (MKMapView mapView, IMKAnnotation annotation)
			{
				MKAnnotationView v = new MKAnnotationView (annotation,null);

				CustomAnnotation ca = annotation as CustomAnnotation;
				if (ca == null)
				{
					// this is user location pin
					return null;
				}

				v.CanShowCallout = !string.IsNullOrEmpty(ca.Pin.Title);

				if (ca.Pin.CalloutDetailCommand != null) {
					v.RightCalloutAccessoryView = UIButton.FromType (UIButtonType.InfoLight);
				}

				UIImage image;
				if (ca.Pin.PinImage is FileImageSource)
				{
					image = new FileImageSourceHandler ().LoadImageAsync (ca.Pin.PinImage).Result;
				}
				else
				{
					image = System.Threading.Tasks.Task.Run (() => new ImageLoaderSourceHandler ().LoadImageAsync (ca.Pin.PinImage)).Result;
				}

				if (image == null)
				{
					return v;
				}

				v.LeftCalloutAccessoryView = new UIImageView (image) { ContentMode = UIViewContentMode.ScaleAspectFill };
				v.LeftCalloutAccessoryView.Frame = new CoreGraphics.CGRect (0, 0, imageSize, imageSize);

				UIGraphics.BeginImageContextWithOptions (new CoreGraphics.CGSize (imageSize, imageSize), false, 0.0f);
			
				image.Draw (new CGRect (0, 0, imageSize, imageSize));
				v.Image = GetRoundedImage (UIGraphics.GetImageFromCurrentImageContext (), new CGSize (imageSize, imageSize), imageSize / 2);

				UIGraphics.EndImageContext ();

				if (ca.Pin.BorderColor.HasValue)
				{
					v.Image = AddBorderToImage (v.Image, 6.0f, ca.Pin.BorderColor.Value.ToCGColor());
				}

				v.CenterOffset = new CoreGraphics.CGPoint (0, -v.Image.Size.Height/2);

				return v;
			}

			UIImage GetRoundedImage(UIImage image, CGSize imageSize, float cornerRadius)
			{
				UIGraphics.BeginImageContextWithOptions(imageSize, false, 0.0f);   //  <= notice 0.0 as third scale parameter. It is important cause default draw scale ≠ 1.0. Try 1.0 - it will draw an ugly image..

				CGRect bounds = new CGRect (0, 0, imageSize.Width, imageSize.Height);
				UIBezierPath path = UIBezierPath.FromRoundedRect (bounds, cornerRadius);
				path.AddClip ();

				image.Draw (bounds);

				UIImage finalImage = UIGraphics.GetImageFromCurrentImageContext();

				UIGraphics.EndImageContext();

				return finalImage;
			}

			UIImage AddBorderToImage (UIImage image, float borderWidth, CGColor color)
			{
				CGSize inflatedSize = new CGSize (imageSize + borderWidth, imageSize + borderWidth);

				UIGraphics.BeginImageContextWithOptions (inflatedSize, false, 0.0f);

				CGContext context = UIGraphics.GetCurrentContext ();
				context.SetFillColor (color);
				context.FillEllipseInRect (new CGRect (new CGPoint (0, 0), inflatedSize));

				image.Draw (new CGRect (borderWidth / 2, borderWidth / 2, imageSize, imageSize));
				UIImage result = UIGraphics.GetImageFromCurrentImageContext ();

				UIGraphics.EndImageContext();

				return result;
			}

			public override MKOverlayView GetViewForOverlay (MKMapView mapView, IMKOverlay overlay)
			{
				CustomRouteOverlay o = overlay as CustomRouteOverlay;
				if (o != null) {
					return new MKPolylineView (o.MKPolyline){StrokeColor = o.Polyline.Color.ToUIColor()};
				}
				return null;
			}

			public override void DidSelectAnnotationView(MKMapView mapView, MKAnnotationView view)
			{
				CustomAnnotation ca = (CustomAnnotation)view.Annotation;
				ca.Pin.IsSelected = true;
			}

			public override void DidDeselectAnnotationView(MKMapView mapView, MKAnnotationView view)
			{
				CustomAnnotation ca = (CustomAnnotation)view.Annotation;
				ca.Pin.IsSelected = false;
			}
		
			public override void CalloutAccessoryControlTapped (MKMapView mapView, MKAnnotationView view, UIControl control)
			{
				CustomAnnotation ca = (CustomAnnotation)view.Annotation;
				if (control == view.RightCalloutAccessoryView) {
					if(ca.Pin.CalloutDetailCommand!=null && ca.Pin.CalloutDetailCommand.CanExecute(ca.Pin)){
						ca.Pin.CalloutDetailCommand.Execute(ca.Pin);
					}
				}
			}

			public override void RegionChanged (MKMapView mapView, bool animated)
			{
				if (Renderer.Element != null && !animated) {
					Renderer.Element.VisibleRegion = new ExtendedMapSpan (new ExtendedPosition (mapView.Region.Center.Latitude, mapView.Region.Center.Longitude), mapView.Region.Span.LatitudeDelta, mapView.Region.Span.LongitudeDelta);		
				}
			}

			/*public override void DidSelectAnnotationView (MKMapView mapView, MKAnnotationView view)
			{
				view.DetailCalloutAccessoryView.Hidden = false;
			}

			public override void DidDeselectAnnotationView (MKMapView mapView, MKAnnotationView view)
			{
				view.DetailCalloutAccessoryView.Hidden = true;
			}*/
			/*public override MKOverlayView GetViewForOverlay (MKMapView mapView, IMKOverlay overlay)
			{
				MKPolylineRenderer line = new MKPolylineRenderer ();
				//MKMapPoint.FromCoordinate
				//line.Path
				return line;
			}*/
		}
		public ExtendedMapViewRenderer ()
		{

		}
		CustomMKMapView m;
		protected override void OnElementChanged (ElementChangedEventArgs<ExtendedMapView> e)
		{
			if (Control == null) {
				m = new CustomMKMapView (this);
				m.Delegate = new CustomMKMapViewDelegate (this);
				m.ShowsUserLocation = true;
				m.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
				SetNativeControl (m);
			}
			base.OnElementChanged (e);
			if (e.OldElement == null) {
				UpdatePins ();
				UpdateVisibleRegion ();
				UpdatePolylines ();
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (ExtendedMapView.PinsProperty.PropertyName == e.PropertyName) {
				UpdatePins ();
			}
			if (ExtendedMapView.VisibleRegionProperty.PropertyName == e.PropertyName) {
				UpdateVisibleRegion ();
			}
			if (ExtendedMapView.PolylinesProperty.PropertyName == e.PropertyName) {
				UpdatePolylines ();
			}
		}
		private void UpdatePolylines (){
			if (Control.Overlays != null) {
				Control.RemoveOverlays (Control.Overlays);
			}
			if (Element.Polylines != null) {
				Element.Polylines.ToList ().ForEach (p => {
					Control.AddOverlay(new CustomRouteOverlay(){Polyline = p});
				});
			}
		}

		private void UpdatePins(){
			Control.RemoveAnnotations (Control.Annotations);

			if (Element.Pins != null) {
				foreach (ExtendedMapPin p in Element.Pins) {
					Control.AddAnnotation (new CustomAnnotation (){ Pin = p });

				}
			}
		}


		private void UpdateVisibleRegion(){
			if (Element.VisibleRegion!=null) {
				ExtendedPosition center = Element.VisibleRegion.Center;
				MKCoordinateRegion region = new MKCoordinateRegion (new CLLocationCoordinate2D (center.Latitude, center.Longitude), new MKCoordinateSpan (Element.VisibleRegion.LatitudeDegrees, Element.VisibleRegion.LongitudeDegrees));
				Control.SetRegion (region, true);
			}
		}
	}
}

