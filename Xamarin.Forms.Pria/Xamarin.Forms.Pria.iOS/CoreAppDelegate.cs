using System;
using UIKit;
using Foundation;
using System.Reflection;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Ninject;

namespace Xamarin.Forms.Pria.iOS
{
	public abstract class CoreAppDelegate : FormsApplicationDelegate
	{
		public class OpenUrlEventArgs:EventArgs{
			public UIApplication Application { get; private set;}
			public NSUrl Url { get; private set;}
			public string SourceApplication { get; private set;}
			public NSObject Annotation { get; private set;}
			public bool Handled { get; set; }

			public OpenUrlEventArgs (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
			{
				this.Application = application;
				this.Url = url;
				this.SourceApplication = sourceApplication;
				this.Annotation = annotation;
			}
			
		}

		public BaseApp App{ get; protected set;}

		public event EventHandler<EventArgs> OnAppActivated;
		public event EventHandler<OpenUrlEventArgs> OnHandleOpenUrl;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			List<Assembly> localizations = GetAssembliesForLocalization () ?? new List<Assembly>();
			localizations.Insert(0,Assembly.Load("Xamarin.Forms.Pria"));
			localizations.Insert(1,Assembly.Load("Xamarin.Forms.Pria.iOS"));
			LocalizationManager.Instance.LoadLocalizations (localizations);
		
			App = CreateApp ();
			App.Kernel.Bind (typeof(ILocalization)).To (typeof(LocalizationManagerIoC)).InSingletonScope ().WithConstructorArgument(typeof(List<Assembly>), localizations);


			if (UseLocalNotifications) {
				DependencyService.Get<ILocalNotificationService> ().RequestLocalNotifications ();
			}

			if (GoogleAnalyticsTrackingID != null) {
				DependencyService.Get<IGoogleAnalyticsService> ().Init (GoogleAnalyticsTrackingID, App.Kernel.Get<ILocalization>().GetText("AppName"));
			}

			App.MainPage = App.GetMainPage ();

			LoadApplication (App);

			return base.FinishedLaunching (app, options);
		}

		protected abstract bool UseLocalNotifications{ get; }
		protected abstract string GoogleAnalyticsTrackingID{ get; }

		protected virtual void DeviceTokenRegistered(string token)
		{
		}

		protected abstract BaseApp CreateApp ();

		public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			if (OnHandleOpenUrl != null) {
				OpenUrlEventArgs args = new OpenUrlEventArgs (application, url, sourceApplication, annotation);

				OnHandleOpenUrl (this,args);

				return args.Handled;
			}
			return base.OpenUrl (application, url, sourceApplication, annotation);
		}
			
		public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
		{
			if (application.ApplicationState == UIApplicationState.Active)
			{
				new UIAlertView (notification.AlertTitle, notification.AlertBody, null, App.Kernel.Get<ILocalization>().GetText ("OK"), null).Show ();
				if (!string.IsNullOrEmpty (notification.SoundName) && notification.SoundName != UILocalNotification.DefaultSoundName) {
					DependencyService.Get<IAudioService> ().PlayResourceFile (notification.SoundName);
				}
			} 

			if (notification.UserInfo != null && notification.UserInfo.ContainsKey (new NSString ("UserData")))
			{
				App.ReceivedLocalNotification (notification.UserInfo.ValueForKey (new NSString ("UserData")).ToString ());
			} 
			else
			{
				App.ReceivedLocalNotification (null);
			}

			application.CancelLocalNotification (notification);
		}

		public override void OnActivated (UIApplication application)
		{
			base.OnActivated (application);
			if (OnAppActivated != null) {
				OnAppActivated (this, EventArgs.Empty);
			}

		}

		protected abstract List<Assembly> GetAssembliesForLocalization ();
	}
}

