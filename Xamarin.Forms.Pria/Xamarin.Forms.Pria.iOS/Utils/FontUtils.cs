using System;
using System.Text;
using UIKit;

namespace Xamarin.Forms.Pria.iOS
{
	public static class FontUtils
	{
		public static void PrintFontList()
		{
			var fontList = new StringBuilder();
			var familyNames = UIFont.FamilyNames;
			foreach (var familyName in familyNames ){
				fontList.Append(String.Format("Family: {0}\n", familyName));
				Console.WriteLine("Family: {0}\n", familyName);
				var fontNames = UIFont.FontNamesForFamilyName(familyName);
				foreach (var fontName in fontNames ){
					Console.WriteLine("\tFont: {0}\n", fontName);
					fontList.Append(String.Format("\tFont: {0}\n", fontName));
				}
			};
		}
	}
}

