using System;
using System.Linq;
using System.IO;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Pria.iOS.Utils;
using Xamarin.Forms.Pria.Utils;
using Xamarin.Forms.Pria.Services;
using Foundation;

[assembly: Dependency(typeof(IsolatedStorage))]
namespace Xamarin.Forms.Pria.iOS.Utils
{
	public class IsolatedStorage : IIsolatedStorage
	{
		string libraryFolder = Path.Combine (NSFileManager.DefaultManager.GetUrls (NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User).ToList ().Last ().Path, "..", "Library");

		public IsolatedStorage ()
		{
		}

		public string GetFullPath(string name)
		{
			string strFinalPath = string.Empty;
			string normalizedFirstPath = libraryFolder.TrimEnd(Path.DirectorySeparatorChar);
			string normalizedSecondPath = (name ?? string.Empty).TrimStart(Path.DirectorySeparatorChar);
			strFinalPath =  Path.Combine(normalizedFirstPath, normalizedSecondPath);
			return strFinalPath;
		}

		#region IIsolatedStorage implementation

		public string[] GetNames (string folder)
		{
			return Directory.GetFiles (GetFullPath (folder));
		}

		public Stream Create (string file)
		{
			string fullPath = Path.GetFullPath(GetFullPath (file));
			string dir = Path.GetDirectoryName (fullPath);
			if (!Directory.Exists (dir)) {
				Directory.CreateDirectory (dir);
			}
			Stream s = File.Create (GetFullPath(file));

			Foundation.NSFileManager.SetSkipBackupAttribute (GetFullPath (file), true);

			return s;
		}

		public Stream OpenWrite (string file)
		{
			Stream s = File.OpenWrite (GetFullPath (file));

			Foundation.NSFileManager.SetSkipBackupAttribute (GetFullPath (file), true);

			return s;
		}
		
		public Stream OpenRead (string file)
		{
			return File.OpenRead (GetFullPath (file));
		}

		public Stream OpenReadFullPath (string path)
		{
			throw new NotImplementedException ();
		}

		public bool Exists (string file)
		{
			return File.Exists (GetFullPath (file));
		}

		public bool Delete (string file)
		{
			try{
				File.Delete (GetFullPath (file));
				return true;
			}
			catch(IOException ex){
				Debug.WriteLine (ex.ToString ());
				return false;
			}
		}
	
		#endregion
	}
}

