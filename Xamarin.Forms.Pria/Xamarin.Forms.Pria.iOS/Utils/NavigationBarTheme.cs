﻿using System;
using UIKit;

namespace Xamarin.Forms.Pria.iOS
{
	public static class NavigationBarTheme
	{
		public static UIColor BarTintColor = UIColor.LightGray;
		public static UIColor TintColor = UIColor.White;
		public static UIColor TitleColor = UIColor.White;

		public static UIStatusBarStyle StatusBarStyle = UIStatusBarStyle.LightContent;

		public static void StyleNavigationBar(UINavigationBar navigationBar)
		{
			navigationBar.BarTintColor = BarTintColor;
			navigationBar.TitleTextAttributes = new UIStringAttributes (){ ForegroundColor = TitleColor };
			navigationBar.TintColor = TintColor;
			navigationBar.Translucent = false;

			UIApplication.SharedApplication.SetStatusBarStyle (StatusBarStyle, false);
		}
	}
}

