using System;
using UIKit;
using CoreGraphics;

namespace Xamarin.Forms.Pria.iOS
{
	public static class iOSImageUtils
	{
		public static UIImage CreateUIImageFromColor(UIColor color)
		{
			CGRect rect = CGRect.FromLTRB(0.0f, 0.0f, 1.0f, 1.0f);
			UIGraphics.BeginImageContext(rect.Size);
			CGContext context = UIGraphics.GetCurrentContext();

			context.SetFillColor(color.CGColor);
			context.FillRect (rect);

			UIImage image = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return image;
		}
	}
}

