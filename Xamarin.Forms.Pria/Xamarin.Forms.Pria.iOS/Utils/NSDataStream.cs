using System;
using System.IO;
using Foundation;

namespace Xamarin.Forms.Pria.iOS
{
	internal unsafe class NSDataStream
		: UnmanagedMemoryStream
	{
		public NSDataStream (NSData data)
			: base ((byte*)data.Bytes, (long)data.Length)
		{
			this.data = data;
		}

		private readonly NSData data;

		protected override void Dispose (bool disposing)
		{
			if (disposing)
				this.data.Dispose();

			base.Dispose (disposing);
		}
	}
}

