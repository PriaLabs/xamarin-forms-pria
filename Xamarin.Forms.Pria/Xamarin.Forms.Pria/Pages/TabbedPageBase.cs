﻿using System;
using System.Windows.Input;

namespace Xamarin.Forms.Pria
{
	public class TabbedPageBase : TabbedPage
	{
		public static readonly BindableProperty OnPageChangedCommandProperty = BindableProperty.Create ("OnPageChangedCommand", typeof(ICommand), typeof(TabbedPageBase), null, BindingMode.OneWay);

		public TabbedPageBase ()
		{
			CurrentPageChanged += (object sender, EventArgs e) => {
				if (sender == null) {
					return;
				}
				OnPageChangedCommand?.Execute (((TabbedPage)sender).CurrentPage);
			};
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();

			if(this.CurrentPage == null){
				return;
			}

			OnPageChangedCommand?.Execute (this.CurrentPage);
		}

		public ICommand OnPageChangedCommand {
			get {
				return (ICommand)base.GetValue (TabbedPageBase.OnPageChangedCommandProperty);
			}
			set {
				base.SetValue (TabbedPageBase.OnPageChangedCommandProperty, value);
			}
		}
	}
}

