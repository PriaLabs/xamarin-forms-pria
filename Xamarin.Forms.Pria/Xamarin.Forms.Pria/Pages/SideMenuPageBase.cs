﻿using System;
using System.Collections.ObjectModel;

namespace Xamarin.Forms.Pria
{
	public abstract class SideMenuPageBase : ContentPage
	{
		public ListView CreateMenuListView()
		{
			BindableListView sideMenuListView = new BindableListView ();
			sideMenuListView.ItemTemplate = new DataTemplate (() => {
				ImageCell imageCell = new ImageCell();

				imageCell.SetBinding (ImageCell.ImageSourceProperty, new Binding ("IconImage"));
				imageCell.SetBinding (TextCell.TextProperty, new Binding("Caption"));

				return imageCell;
			});
			sideMenuListView.SetBinding (ItemsView<Cell>.ItemsSourceProperty, new Binding ("SideMenuItems", 0, new GenericConverter<ObservableCollection<SideMenuItem>,FilteredObservableCollection<SideMenuItem>> ((s) => s == null ? null : new FilteredObservableCollection<SideMenuItem> (s, (mi) => mi.IsVisible))));
			//sideMenuListView.SetBinding (ItemsView<Cell>.ItemsSourceProperty, new Binding ("SideMenuItems"));
			sideMenuListView.SetBinding (BindableListView.ItemTappedCommandProperty, new Binding("SideMenuItemTapped"));

			return sideMenuListView;
		}
	}
}
