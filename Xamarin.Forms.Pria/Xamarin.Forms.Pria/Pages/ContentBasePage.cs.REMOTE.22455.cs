using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.DataContexts;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria.Pages
{
	public class ContentBasePage<T>:ContentPage where T:DataContextBase, new()
	{
		public T DataContext{ get { return(T)BindingContext; } }

		public bool IsDataLoaded{ get; private set; }

		public EventHandler<EventArgs> OnMessageClosed;

		public ContentBasePage (T dataContext)
		{
			BindingContext = dataContext;
			dataContext.SetPage (this);

			ToolbarItem reloadToolbarItem = CreateReloadToolBarItem ();
			if (reloadToolbarItem != null) {
				reloadToolbarItem.Activated += async (object sender, EventArgs e) => {await ReloadData();};
				ToolbarItems.Add (reloadToolbarItem);
			}
		}

		public ContentBasePage () : this (new T ())
		{
		}

		protected virtual ToolbarItem CreateReloadToolBarItem(){
			return null;
		}

		protected virtual async Task ReloadData(){
			IsDataLoaded = false;

			await LoadData ();
		}

		async Task LoadData ()
		{
			this.IsBusy = true;

			await DataContext.LoadData();

			IsDataLoaded = true;

			OnDataLoaded();

			this.IsBusy = false;
		}

		protected override async void OnAppearing ()
		{
			base.OnAppearing ();

			if (!IsDataLoaded) {
				await LoadData ();
			}

			DataContext.PageAppearing ();
		}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();

			DataContext.PageDisappearing ();
		}

		public event EventHandler<EventArgs> DataLoaded;

		protected virtual void OnDataLoaded ()
		{
			if (DataLoaded != null) {
				DataLoaded (this, EventArgs.Empty);
			}

		}

	}
}

