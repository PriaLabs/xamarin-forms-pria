﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class RootPage : MasterDetailPage, INavigation
	{
		public SideMenuViewModelBase SideMenuViewModel{get; private set;}

		public RootPage (SideMenuViewModelBase sideMenuViewModel)
		{
			this.SideMenuViewModel = sideMenuViewModel;
			SideMenuViewModel.LoadData ();
			SideMenuViewModel.RootNavigation = this;

			Master = SideMenuViewModel.Page;
			SideMenuViewModel.ShowMenuItem (SideMenuViewModel.InitialSideMenuItem);
		}

		#region INavigation implementation

		public void RemovePage (Page page)
		{
			Navigation.RemovePage (page);
		}

		public void InsertPageBefore (Page page, Page before)
		{
			Navigation.InsertPageBefore (page, before);
		}

		public async Task PushAsync (Page page, bool animated)
		{
			await Navigation.PushAsync (page, animated);
		}

		public async Task<Page> PopAsync (bool animated)
		{
			return await Navigation.PopAsync (animated);
		}

		public async Task PopToRootAsync (bool animated)
		{
			await Navigation.PopToRootAsync (animated);
		}

		public async Task PushModalAsync (Page page, bool animated)
		{
			Detail = WrapToPage (page);
			IsPresented = false;
		}

		public async Task<Page> PopModalAsync (bool animated)
		{
			return await Navigation.PopModalAsync(animated);
		}

		public async Task PushAsync (Page page)
		{
			await Navigation.PushAsync(page);
		}

		public async Task<Page> PopAsync ()
		{
			return await Navigation.PopAsync ();
		}

		public async Task PopToRootAsync ()
		{
			await Navigation.PopToRootAsync ();
		}

		public async Task PushModalAsync (Page page)
		{
			Detail = WrapToPage (page);
			IsPresented = false;
		}

		public async Task<Page> PopModalAsync ()
		{
			return await Navigation.PopModalAsync ();
		}

		public IReadOnlyList<Page> NavigationStack {
			get {
				return NavigationStack;
			}
		}

		public IReadOnlyList<Page> ModalStack {
			get {
				return ModalStack;
			}
		}

		#endregion

		protected virtual Page WrapToPage(Page page){
			return new NavigationPage(page);
		}
	}
}

