﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.DataContexts;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria.Pages
{
	public class ContentBasePage<T>:ContentPage where T:DataContextBase, new()
	{
		public T DataContext{ get { return(T)BindingContext; } }

		public bool IsDataLoaded{ get; private set; }

		public EventHandler<EventArgs> OnMessageClosed;

		public ContentBasePage (T dataContext)
		{
			BindingContext = dataContext;
			dataContext.SetPage (this);
		}

		public ContentBasePage () : this (new T ())
		{
		}

		protected virtual async Task ReloadData ()
		{
			IsDataLoaded = false;

			await LoadData ();
		}

		async Task LoadData ()
		{
			await DataContext.LoadData ();

			IsDataLoaded = true;

			OnDataLoaded ();
		}

		protected override async void OnAppearing ()
		{
			base.OnAppearing ();

			if (!IsDataLoaded) {
				await LoadData ();
			}

			DataContext.PageAppearing ();
		}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();

			DataContext.PageDisappearing ();
		}

		public event EventHandler<EventArgs> DataLoaded;

		protected virtual void OnDataLoaded ()
		{
			if (DataLoaded != null) {
				DataLoaded (this, EventArgs.Empty);
			}

		}

	}
}

