using System;
using Xamarin.Forms.Pria.ViewModels;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria.Pages
{
	public class ListItemCollection<T> : ObservableCollection<T>
	{
		public string CollectionName { get; private set; }
		public virtual string CollectionCaption{get;protected set;}
		public int CollectionOrder{ get; private set; }

		public ListItemCollection (string collectionName, IEnumerable<T> e, int order = 0):base(e)
		{	
			CollectionName = collectionName;
			CollectionOrder = order;
		}

		public ListItemCollection (string name, int order = 0)
		{
			CollectionName = name;
			CollectionOrder = order;
		}
	}

	public abstract class ListViewBasePage:ContentBasePage
	{
		public BindableListView ListView { get; private set; }
		protected SearchBar SearchBar{get;private set;}

		private View loadingView;
		private View contentView;


		public ListViewBasePage ()
		{
			ListView = CreateListView ();

			ListView.ItemTemplate = CreateDataTemplate ();

			loadingView = CreateLoadingView();

			if (AllowSearchBar()) {
				contentView = new StackLayout (){ Orientation = StackOrientation.Vertical, BackgroundColor = Color.Transparent, Children = { CreateSearchBar (), CreateLayout() } };
			} else {
				contentView = CreateLayout();
			}

			//Content = loadingView;
			Content = contentView;
		}

		protected override async Task ReloadData ()
		{
			Content = loadingView;

			await base.ReloadData ();

			Content = contentView;
		}

		protected virtual BindableListView CreateListView(){
			return new BindableListView (){ BackgroundColor = Color.Transparent };
		}

		protected virtual View CreateLoadingView(){
			return new ActivityIndicator (){ IsRunning = true };
		}

		protected virtual View CreateLayout(){
			return ListView;
		}
			
		protected virtual bool AllowSearchBar(){
			return false;
		}

		protected virtual void Search(string term){
		}

		protected abstract DataTemplate CreateDataTemplate();

		private SearchBar CreateSearchBar(){
			SearchBar = new SearchBar ();

			SearchBar.SearchButtonPressed += (sender, e) => {
				Search(SearchBar.Text);
			};
			SearchBar.TextChanged += (object sender, TextChangedEventArgs e) => {
				if(string.IsNullOrEmpty(e.NewTextValue)){
					Search(null);
				}
			};
			return SearchBar;
		}

	}
}

