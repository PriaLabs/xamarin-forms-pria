using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria.ViewModels;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria.Pages
{
	[Obsolete("Do not use. TODO: Move to Stratocaching Project and remove.")]
	public class ContentBasePage:ContentPage
	{
		public ContentBasePage ()
		{
			ToolbarItem reloadToolbarItem = CreateReloadToolBarItem ();
			if (reloadToolbarItem != null) {
				reloadToolbarItem.Activated += async (object sender, EventArgs e) => {await ReloadData();};
				ToolbarItems.Add (reloadToolbarItem);
			}
		}

		protected virtual ToolbarItem CreateReloadToolBarItem(){
			return null;
		}
		protected virtual async Task ReloadData(){
			await ((ViewModelBase)BindingContext).RefreshData ();
		}

	}
}

