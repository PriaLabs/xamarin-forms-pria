﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IContactService
	{
		void CallNumber(string phoneNumber);
		void ComposeSms(string phoneNumber);
		void ComposeEmail(string email, string subject, string body);

		bool CanMakePhoneCalls();
		bool CanSendSMS();
	}
}

