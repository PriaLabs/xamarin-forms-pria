﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System;


namespace Xamarin.Forms.Pria
{
    public interface IFacebookService
    {
		void Setup(string appId,string appName);

		bool IsLogged{get;}

		event EventHandler OnSessionStatusChanged;

		string AccessToken{get;}

		void Share(string title, string description, string url, string imageUrl = null);

		void Logout();
    }
}
