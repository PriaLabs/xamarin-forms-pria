﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IGooglePlusService
	{
		void Setup(string clientId);
		event EventHandler OnLoginStatusChanged;
		bool IsLogged{get;}
		string AccessToken{get;}
		void SignIn();
	}
}

