﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IAudioService
	{
		// from IsolatedStorage
		void PlayLocalFile (string filePath);

		// from Bundle or Resource
		void PlayResourceFile (string resourceName);
	}
}

