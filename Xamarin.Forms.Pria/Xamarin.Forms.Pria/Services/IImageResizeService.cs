﻿using System;
using System.IO;

namespace Xamarin.Forms.Pria
{
	public interface IImageResizeService
	{
		Stream ResizeImage(MediaFile mediaFile, int maxPixelDimension);
	}
}

