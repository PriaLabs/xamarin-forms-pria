﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IGoogleAnalyticsService
	{
		void Init(string trackingID, string appName);
		void TrackPage(string pageNameToTrack);
		void TrackEvent(string category, string action, string label);
		void TrackException(string exceptionMessageToTrack, bool isFatalException);
	}
}

