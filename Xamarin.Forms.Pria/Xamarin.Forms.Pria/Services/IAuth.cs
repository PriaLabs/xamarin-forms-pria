﻿using System;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria
{
	public class Account
	{
		public Site Site{get;set;}
		public string AccessToken{get;set;}
	}

	public enum Site
	{
		Facebook,
		GPlus
	}

	public interface IAuth
	{
		Task<Account> LoginFacebook (string appId, string secret, string redirectUrl, string scope);
		Task<Account> LoginGooglePlus (string appId, string secret, string redirectUrl, string scope);
	}
}

