﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IMapService
	{
		void ShowMap(string title, double latitude,double longitude, bool requestNavigation = false);
        void ShowMap(string address);
	}
}

