﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IUrlService
	{
		void OpenUrl (string url);
		void OpenStore (string appId);
	}
}

