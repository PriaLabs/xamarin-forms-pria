﻿using System;
using System.IO;

namespace Xamarin.Forms.Pria.Services
{
	public interface IIsolatedStorage
	{
		Stream Create (string file);
		Stream OpenWrite(string file);
		Stream OpenRead(string file);
		Stream OpenReadFullPath(string path);
		string[] GetNames (string folder);
		bool Exists(string file);
		bool Delete(string file);
		string GetFullPath(string file);
	}
}

