﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IGlobalPersistor
	{
		void Save(string key, string value);
		string Load(string key);
	}
}

