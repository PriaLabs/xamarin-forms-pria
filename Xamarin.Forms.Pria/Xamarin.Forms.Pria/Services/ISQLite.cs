﻿using System;
using SQLite.Net;

namespace Xamarin.Forms.Pria
{
	public interface ISQLite
	{
		SQLiteConnectionWithLock GetConnection(string databaseName);

		SQLiteConnectionWithLock GetConnectionLocal(string databaseName);
	}
}

