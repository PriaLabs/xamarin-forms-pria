﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface IShareService
	{
		void Share(string subject, string description,string url);
	}
}

