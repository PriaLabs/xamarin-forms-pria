﻿using System;

namespace Xamarin.Forms.Pria
{
	public static class MediaUtils
	{
		public static void ResizeBasedOnPixelDimension(int? maxPixelDimension, int currentWidth, int currentHeight,
			out int targetWidth, out int targetHeight)
		{
			if (!maxPixelDimension.HasValue)
			{
				targetWidth = currentWidth;
				targetHeight = currentHeight;
				return;
			}

			double ratio;
			if (currentWidth > currentHeight)
				ratio = (maxPixelDimension.Value) / ((double)currentWidth);
			else
				ratio = (maxPixelDimension.Value) / ((double)currentHeight);

			targetWidth = (int)Math.Round(ratio * currentWidth);
			targetHeight = (int)Math.Round(ratio * currentHeight);
		}
	}
}

