﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class Purchase
	{
		public string ID{get;set;}
		public string Token{get;set;} // for ConsumeProduct(token)
		public string RecieptData{get;set;}
		public string Signature{get;set;}
	}

	public class Product
	{
		public string ID{get;set;}
		public string Title{get;set;}
		public string Description{get;set;}
		public string Price{get;set;}
	}

	public interface IInAppBilling
	{
		void Init();
		bool IsAvailable();
		bool SupportsManualRestore();

		Task<Purchase> PurchaseProduct(Product product);
		Task<Purchase> PurchaseSubscription(Product product);

		Task<List<Product>> QueryInventoryForSubscriptions(List<string> productIds);
		Task<List<Product>> QueryInventoryForProducts(List<string> productIds);

		Task<List<Purchase>> GetPurchasedProducts();
		Task<List<Purchase>> GetPurchasedSubscriptions();

		//Task<bool> ConsumeProduct (Purchase purchase);

		Task<List<Purchase>> RestorePurchases();
	}
}

