﻿using System;

namespace Xamarin.Forms.Pria
{
	public interface ILocalNotificationService
	{
		void RequestLocalNotifications();
		void RequestRemoteNotifications();
		void Notify(string title,string text,string action,string userData = null,string soundName = null);
	}
}

