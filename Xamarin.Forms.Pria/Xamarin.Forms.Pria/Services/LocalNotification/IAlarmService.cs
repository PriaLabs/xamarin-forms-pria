﻿using System;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria.Services
{
	public interface IAlarmService
	{
		void RegisterNotifications (List<LocalNotification> notifications);
		void ClearAll ();
		void ClearNotification (LocalNotification notification);
	}
}

