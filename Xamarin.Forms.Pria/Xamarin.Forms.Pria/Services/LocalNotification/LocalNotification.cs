﻿using System;
using System.Xml.Serialization;

namespace Xamarin.Forms.Pria
{
	public class LocalNotification
	{
		public string Id{ get; set;}
		public string Title{get;set;}
		public string Body{get;set;}

		/// <summary>
		/// iOS: put sound file (.wav, .caf, .aiff) into Resources folder as BundleResource
		/// Android: put sound file (.mp3, ???) int Resources/raw folder as AndroidResource
		/// </summary>
		/// <value>The name of the sound.</value>
		public string SoundName{get;set;}

		public string UserData{get;set;}

		[XmlIgnore]
		public DateTime Date{
			get{
				return new DateTime (DateTicks);
			}
			set{
				DateTicks = value.Ticks;
			}
		}

		public long DateTicks{ get; set; }

		/// <summary>
		/// Used only on iOS ("Swipe for 'alertAction')"
		/// </summary>
		/// <value>The alert action.</value>
		public string AlertAction{get;set;}
	}
}

