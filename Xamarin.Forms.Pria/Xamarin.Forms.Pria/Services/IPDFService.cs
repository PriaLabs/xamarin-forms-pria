﻿using System;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria
{
	public interface IPDFService
	{
		void OpenLocalPDF(string path, string title = null);
		Task OpenWebPDF(string url, string title = null);
	}
}

