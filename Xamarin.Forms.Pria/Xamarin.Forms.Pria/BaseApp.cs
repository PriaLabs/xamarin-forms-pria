﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ninject;

namespace Xamarin.Forms.Pria
{
	public abstract class BaseApp : Application
	{
		protected BaseApp ()
		{
			Kernel = new StandardKernel();
            
			// called from CoreAppDelegate and from CoreActivity
			//MainPage = GetMainPage ();
		}

		public static BaseApp CurrentApp{
			get{
				return (BaseApp)Application.Current;
			}
		}

		public IKernel Kernel{ get; private set; }

		public abstract Page GetMainPage();

		public virtual void ReceivedLocalNotification(string parameter){ }

		public virtual void ReceivedRemoteNotification(Dictionary<string, string> parameters){ }

		bool mIsForeground = true;
		public bool IsForeground
		{
			get { return mIsForeground; }
			set {
				if (value != mIsForeground)
				{
					mIsForeground = value;
					OnPropertyChanged ();
				}
			}
		}

		protected override void OnSleep ()
		{
			base.OnSleep ();

			IsForeground = false;
		}

		protected override async void OnResume ()
		{
			base.OnResume ();

			IsForeground = true;

			await ExecuteForegroundActions();
		}

		protected override async void OnStart ()
		{
			base.OnStart ();

			IsForeground = true;

			await ExecuteForegroundActions();
		}
		interface IPostRunnable
		{
			void Execute();
		}

		class PostActionRunnable : IPostRunnable
		{
			Func<Task> action;
			TaskCompletionSource<Task> taskCompletionSource;
			public PostActionRunnable(TaskCompletionSource<Task> taskCompletionSource, Func<Task> action)
			{
				this.action = action;
				this.taskCompletionSource = taskCompletionSource;
			}
			void IPostRunnable.Execute()
			{
				taskCompletionSource.SetResult(action.Invoke());
			}
		}

		class PostFuncRunnable<T> : IPostRunnable
		{
			TaskCompletionSource<T> taskCompletionSource;
			Func<Task<T>> action;
			public PostFuncRunnable(TaskCompletionSource<T> taskCompletionSource, Func<Task<T>> action)
			{
				this.taskCompletionSource = taskCompletionSource;
				this.action = action;
			}
			async void IPostRunnable.Execute()
			{
				taskCompletionSource.SetResult(await action.Invoke());
			}

		}

		List<IPostRunnable> foregroundActions = new List<IPostRunnable>();

		public async Task<T> PostForegroundAction<T>(Func<Task<T>> action)
		{
			if (IsForeground)
			{
				return await action.Invoke();
			}
			else {
				TaskCompletionSource<T> t = new TaskCompletionSource<T>();
				foregroundActions.Add(new PostFuncRunnable<T>(t, action));
				return await t.Task;
			}
		}

		public Task PostForegroundAction(Func<Task> action)
		{
			if (IsForeground)
			{
				return action.Invoke();
			}
			else {
				TaskCompletionSource<Task> t = new TaskCompletionSource<Task>();
				foregroundActions.Add(new PostActionRunnable(t, action));
				return t.Task;
			}
		}

		async Task ExecuteForegroundActions()
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				while (foregroundActions.Count > 0)
				{
					foregroundActions[0].Execute();

					foregroundActions.RemoveAt(0);
				}
			});

		}
	}
}

