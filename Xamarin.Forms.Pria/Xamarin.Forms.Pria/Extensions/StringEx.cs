﻿using System;
using System.Linq;

namespace Xamarin.Forms.Pria
{
	public static class StringEx
	{
		public static string RemoveWhitespaces(this string input)
		{
			return new string (input.ToCharArray ().Where (c => !Char.IsWhiteSpace (c)).ToArray ());
		}
	}
}

