﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Collections.Generic
{
	public static class ListExt
	{
		public static List<U> ConvertAll<T,U> (this List<T> tCollection, Func<T, U> convertor)
		{
			List<U> result = new List<U> ();
			tCollection.ForEach (i => result.Add(convertor (i)));
			return result;
		}

		public static List<T> FindAll<T> (this List<T> tCollection, Func<T, bool> predicate)
		{
			List<T> result = new List<T> ();
			tCollection.ForEach (i => {
				if (predicate (i))
					result.Add (i);
			});
			return result;
		}

		public static void ForEach<T> (this List<T> tCollection, Action<T> action)
		{
			foreach (T i in tCollection) {
				action.Invoke (i);
			}
		}

		public static T Find<T> (this List<T> tCollection, Func<T, bool> predicate)
		{
			foreach (T i in tCollection) {
				if (predicate (i))
					return i; 
			}
			return default(T);
		}
	}
}
