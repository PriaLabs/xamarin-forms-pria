﻿using System;
using TwinTechs.Gestures;

namespace Xamarin.Forms.Pria
{
	public class GestureEventArgs : EventArgs
	{
		public BaseGestureRecognizer GestureRecognizer{ get; private set; }
		public GestureRecognizerState GestureState{ get; private set; }
		public object CommandParameter{ get; private set; }

		public GestureEventArgs (BaseGestureRecognizer gestureRecognizer, GestureRecognizerState gestureState, object commandParameter)
		{
			this.GestureRecognizer = gestureRecognizer;
			this.GestureState = gestureState;
			this.CommandParameter = commandParameter;
		}
	}
}

