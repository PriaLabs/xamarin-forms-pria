namespace Xamarin.Forms.Pria
{
	public interface ILocalization
	{
		string GetText (string key);
		string GetText (string key, params object[] values);
	}
	
}
