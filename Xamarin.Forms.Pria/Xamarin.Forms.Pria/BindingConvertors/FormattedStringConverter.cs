﻿using System;

namespace Xamarin.Forms.Pria
{
	public class FormattedStringConverter:IValueConverter
	{
		string formatString;

		public FormattedStringConverter (string formatString)
		{
			this.formatString = formatString;
		}

		#region IValueConverter implementation
		public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return string.Format (formatString, value);
		}
		public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException ();
		}
		#endregion
	}
}

