﻿using System;

namespace Xamarin.Forms.Pria
{
	public class GenericConverter<S,D>:IValueConverter
	{
		Func<S,D> convert;
		Func<D,S> convertBack;

		public GenericConverter (Func<S, D> convert, Func<D, S> convertBack = null)
		{
			this.convert = convert;
			this.convertBack = convertBack;
		}
		

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return convert ((S)value);
		}

		public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (convertBack == null) {
				return value;
			}

			return convertBack ((D)value);
		}

		#endregion
	}
}

