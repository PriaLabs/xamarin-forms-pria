﻿using System;

namespace Xamarin.Forms.Pria
{
	public class BoolInverter:IValueConverter
	{
		public BoolInverter ()
		{
		}

		#region IValueConverter implementation

		object IValueConverter.Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return !((bool)value);
		}

		object IValueConverter.ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return !((bool)value);
		}

		#endregion
	}
}

