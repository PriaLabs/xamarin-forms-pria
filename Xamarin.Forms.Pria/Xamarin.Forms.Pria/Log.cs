﻿using System;
using System.Diagnostics;
using System.Text;


namespace Xamarin.Forms.Pria
{
	public static class Log
	{
		public static void LogException (Exception ex)
		{
			Debug.WriteLine (ex.ToString ());
		}

		public static void LogWarning (string warning)
		{
			Debug.WriteLine (warning);
		}

		public static void LogInfo (string info)
		{
			Debug.WriteLine (info);
		}

		public static void LogDebug (string debug)
		{
			Debug.WriteLine (debug);
		}

		public static void Console (params Object[] args)
		{
			if(args == null){
				Debug.WriteLine ("NULL");
			}
				

			StringBuilder builder = new StringBuilder (args.Length);
			foreach (Object o in args) {
				if (o == null) {
					builder.Append ("NULL ");
				} else {
					builder.Append (o.ToString ());
					builder.Append (" ");
				}
			}

			String log = builder.ToString ();
			Debug.WriteLine (log);
		}
	}
}

