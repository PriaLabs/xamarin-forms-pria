﻿using System;
using Xamarin.Forms;

namespace Xamarin.Forms.Pria.Views
{
	public class VideoView : View
	{
		public Uri Source{get;set;}
		public bool AutoPlay{get;set;}
		public bool Repeat{get;set;}
	}
}

