﻿using System;
using Xamarin.Forms;

namespace Xamarin.Forms.Pria
{
	public class RoundedContentView : ContentView
	{
		public float Radius{get; private set;}

		public RoundedContentView (float radius)
		{
			this.Radius = radius;
		}
	}
}
