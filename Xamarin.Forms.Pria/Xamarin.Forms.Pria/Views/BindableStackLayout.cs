﻿using System;
using Xamarin.Forms;
using System.Collections;
using System.Windows.Input;

namespace Xamarin.Forms
{
	public class BindableStackLayout:StackLayout
	{
		public BindableStackLayout ()
		{
		}

		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create("ItemsSource",typeof(IList),typeof(BindableStackLayout),null);

		public static readonly BindableProperty ItemTemplateProperty  = BindableProperty.Create("ItemTemplate",typeof(Func<View>),typeof(BindableStackLayout),null);

		public static readonly BindableProperty ItemTappedCommandProperty = BindableProperty.Create ("ItemTappedCommand", typeof(ICommand), typeof(BindableStackLayout), null, BindingMode.OneWay, null, null, null, null);

		public IList ItemsSource {
			get{
				return (IList)base.GetValue (ItemsSourceProperty);
			}
			set{
				base.SetValue (ItemsSourceProperty, value);
			}
		}

		public Func<View> ItemTemplate {
			get{ 
				return (Func<View>)base.GetValue (ItemTemplateProperty);
			}
			set{
				base.SetValue (ItemTemplateProperty,value);
			}
		}

		public ICommand ItemTappedCommand {
			get {
				return (ICommand)base.GetValue (BindableStackLayout.ItemTappedCommandProperty);
			}
			set {
				base.SetValue (BindableStackLayout.ItemTappedCommandProperty, value);
			}
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == ItemsSourceProperty.PropertyName || propertyName == ItemTemplateProperty.PropertyName) {
				CreateLayout ();
			}
		}

		private void CreateLayout()
		{
			Children.Clear ();

			if (ItemTemplate != null)
			{ 				
				if (ItemsSource == null) {
					return;
				}

				for (int i = 0; i < ItemsSource.Count; i++)
				{
					View v = new ContentView (){ Content = ItemTemplate.Invoke () };
					v.BindingContext = ItemsSource [i];

					TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ();
					tapGestureRecognizer.Tapped += (sender, e) => {
						if (ItemTappedCommand != null && IsEnabled) {
							ItemTappedCommand.Execute (v.BindingContext);
						}
					};
					v.GestureRecognizers.Add (tapGestureRecognizer);

					Children.Add (v);
				}
			}
		}
	}
}

