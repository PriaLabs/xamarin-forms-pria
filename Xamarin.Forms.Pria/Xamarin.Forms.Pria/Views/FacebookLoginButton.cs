﻿using System;

namespace Xamarin.Forms.Pria
{
	public class FacebookLoginButton : View
	{
		public static readonly BindableProperty ReadPermissionsProperty = BindableProperty.Create ("ReadPermissions", typeof(string[]), typeof(FacebookLoginButton), null, BindingMode.OneWay, null, null, null, null);

		public string[] ReadPermissions {
			get {
				return (string[])base.GetValue (FacebookLoginButton.ReadPermissionsProperty);
			}
			set {
				base.SetValue (FacebookLoginButton.ReadPermissionsProperty, value);
			}
		}

		public FacebookLoginButton ()
		{
		}
	}
}

