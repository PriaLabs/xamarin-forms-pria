using System;
using System.Windows.Input;

namespace Xamarin.Forms.Pria
{
	public class ImageButton : TintedImage
	{
		public double DisabledOpacity{ get; private set; }

		public ImageButton (double disabledOpacity = 0.5f)
		{
			TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ();
			tapGestureRecognizer.Tapped += (sender, e) => {
				if(IsEnabled && Clicked!=null){
					Clicked(this,EventArgs.Empty);
				}
				if (Command != null && Command.CanExecute (CommandParameter) && IsEnabled) {

					Command.Execute (CommandParameter);
				}
			};
			GestureRecognizers.Add (tapGestureRecognizer);

			DisabledOpacity = disabledOpacity;
		}

		public event EventHandler Clicked;

		public static readonly BindableProperty CommandProperty = BindableProperty.Create ("Command", typeof(ICommand), typeof(ImageButton), null, BindingMode.OneWay, null, delegate (BindableObject bo, object o, object n) {
			((ImageButton)bo).OnCommandChanged ();
		}, null, null);

		public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create ("CommandParameter", typeof(object), typeof(ImageButton), null, BindingMode.OneWay, null, delegate (BindableObject bindable, object oldvalue, object newvalue) {
			((ImageButton)bindable).CommandCanExecuteChanged (bindable, EventArgs.Empty);
		}, null, null);


		public ICommand Command {
			get {
				return (ICommand)base.GetValue (ImageButton.CommandProperty);
			}
			set {
				base.SetValue (ImageButton.CommandProperty, value);
			}
		}

		public object CommandParameter {
			get {
				return base.GetValue (ImageButton.CommandParameterProperty);
			}
			set {
				base.SetValue (ImageButton.CommandParameterProperty, value);
			}
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (ImageButton.IsEnabledProperty.PropertyName == propertyName) {
				Opacity = IsEnabled ? 1.0f : DisabledOpacity;
			}
		}
		private void CommandCanExecuteChanged (object sender, EventArgs eventArgs)
		{

		}

		private void OnCommandChanged ()
		{

		}
	}
}

