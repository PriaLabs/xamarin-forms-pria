using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;

namespace Xamarin.Forms.Pria
{
	public struct ExtendedDistance
	{
		//
		// Properties
		//
		public double Kilometers {
			get {
				return Meters / 1000.0;
			}
		}
		double meters;
		public Double Meters { get { return meters; } private set { meters = value; } }

		public double Miles {
			get {
				return Meters / 1609.34;
			}
		}

		//
		// Constructors
		//
		public ExtendedDistance (double meters)
		{
			this.meters = meters;
		}

		//
		// Static Methods
		//
		public static ExtendedDistance FromKilometers (double kilometers)
		{
			if (kilometers < 0) {
				kilometers = 0;
			}
			return new ExtendedDistance (kilometers * 1000);
		}

		public static ExtendedDistance FromPositionDistance(ExtendedPosition p1,ExtendedPosition p2){
			double num = Math.PI * p1.Latitude / 180;
			double num2 = Math.PI * p1.Longitude / 180;
			double num3 = Math.PI * p2.Latitude / 180;
			double num4 = Math.PI * p2.Longitude / 180;
			double d = Math.Cos (num) * Math.Cos (num3) * Math.Cos (num2) * Math.Cos (num4) + Math.Cos (num) * Math.Sin (num2) * Math.Cos (num3) * Math.Sin (num4) + Math.Sin (num) * Math.Sin (num3);
			double num5 = Math.Acos (d);
			return ExtendedDistance.FromKilometers(6371 * num5);
		}
		public static ExtendedDistance FromMeters (double meters)
		{
			if (meters < 0) {
				meters = 0;
			}
			return new ExtendedDistance (meters);
		}

		public static ExtendedDistance FromMiles (double miles)
		{
			if (miles < 0) {
				miles = 0;
			}
			return new ExtendedDistance (miles * 1609.34);
		}

		//
		// Methods
		//
		public bool Equals (ExtendedDistance other)
		{
			return Meters.Equals (other.Meters);
		}

		public override bool Equals (object obj)
		{
			return obj != null && obj is ExtendedDistance && this.Equals ((ExtendedDistance)obj);
		}

		public override int GetHashCode ()
		{
			return this.Meters.GetHashCode ();
		}

		//
		// Operators
		//
		public static bool operator == (ExtendedDistance left, ExtendedDistance right) {
			return left.Equals (right);
		}

		public static bool operator != (ExtendedDistance left, ExtendedDistance right) {
			return !left.Equals (right);
		}
	}
	
}
