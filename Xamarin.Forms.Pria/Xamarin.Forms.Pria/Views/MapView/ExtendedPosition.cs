using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;

namespace Xamarin.Forms.Pria
{
	public class ExtendedPosition : ExtendedBindableObject{
		private double mLatitude;

		public double Latitude {
			get { return mLatitude; }
			set { 
				if (value != mLatitude) {
					mLatitude = value;
					OnPropertyChanged ();
				}
			}
		}
		private double mLongitude;

		public double Longitude {
			get { return mLongitude; }
			set { 
				if (value != mLongitude) {
					mLongitude = value;
					OnPropertyChanged ();
				}
			}
		}

		public ExtendedPosition (double latitude, double longitude)
		{
			this.Latitude = latitude;
			this.Longitude = longitude;
		}
		// Methods
		//
		public override bool Equals (object obj)
		{
			if (obj == null) {
				return false;
			}
			if (obj.GetType () != base.GetType ()) {
				return false;
			}
			ExtendedPosition position = (ExtendedPosition)obj;
			return this.Latitude == position.Latitude && this.Longitude == position.Longitude;
		}

		public override int GetHashCode ()
		{
			return this.Latitude.GetHashCode () * 397 ^ this.Longitude.GetHashCode ();
		}

		//
		// Operators
		//
		public static bool operator == (ExtendedPosition left, ExtendedPosition right) {
			return object.Equals (left, right);
		}

		public static bool operator != (ExtendedPosition left, ExtendedPosition right) {
			return !object.Equals (left, right);
		}
	}
	
}
