using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class ExtendedMapPolyline : ExtendedBindableObject{
		private Color mColor;
		public Color Color{
			get { return mColor;}
			set { 
				if (value != mColor)
				{
					mColor = value;
					OnPropertyChanged();
				}
			}
		}

		private List<ExtendedPosition> mPoints;

		public List<ExtendedPosition> Points {
			get { return mPoints; }
			set { 
				if (value != mPoints) {
					mPoints = value;
					OnPropertyChanged ();
				}
			}
		}
	}
	
}
