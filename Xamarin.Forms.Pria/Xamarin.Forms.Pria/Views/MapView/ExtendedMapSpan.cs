using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class ExtendedMapSpan:ExtendedBindableObject{

		public ExtendedPosition Center{ get; private set;}
		public double LatitudeDegrees{get;private set;}
		public double LongitudeDegrees{get;private set;}

		public ExtendedDistance Radius {
			get {
				double val = ExtendedMapSpan.LatitudeDegreesToKm (this.LatitudeDegrees);
				double val2 = ExtendedMapSpan.LongitudeDegreesToKm (this.Center, this.LongitudeDegrees);
				return new ExtendedDistance (1000 * Math.Min (val, val2) / 2);
			}
		}

		//
		// Constructors
		//
		public ExtendedMapSpan (ExtendedPosition center, double latitudeDegrees, double longitudeDegrees)
		{
			Center = center;
			LatitudeDegrees = Math.Min (Math.Max (latitudeDegrees, 8.99321605918731E-06), 90);
			LongitudeDegrees = Math.Min (Math.Max (longitudeDegrees, 8.99321605918731E-06), 180);
		}

		//
		// Static Methods
		//
		private static double DistanceToLatitudeDegrees (ExtendedDistance distance)
		{
			return distance.Kilometers / 40030.1735920411 * 360;
		}

		private static double DistanceToLongitudeDegrees (ExtendedPosition position, ExtendedDistance distance)
		{
			double num = ExtendedMapSpan.LatitudeCircumferenceKm (position);
			return distance.Kilometers / num * 360;
		}

		public static ExtendedMapSpan FromCenterAndRadius (ExtendedPosition center, ExtendedDistance radius)
		{
			return new ExtendedMapSpan (center, 2 * ExtendedMapSpan.DistanceToLatitudeDegrees (radius), 2 * ExtendedMapSpan.DistanceToLongitudeDegrees (center, radius));
		}

		public static ExtendedMapSpan FromPoints(List<ExtendedPosition> points, double radiusMultiplier = 1.0f)
		{
			double minLat = 90, minLng = 180, maxLat = -90, maxLng = -180;
			points.ForEach (p => {
				minLat = Math.Min(minLat,p.Latitude);
				minLng = Math.Min(minLng,p.Longitude);
				maxLat = Math.Max(maxLat,p.Latitude);
				maxLng = Math.Max(maxLng,p.Longitude);
			});
			ExtendedPosition center = new ExtendedPosition ((maxLat + minLat)/2.0f,(maxLng + minLng)/2.0f);
			return new ExtendedMapSpan (center, Math.Abs ((maxLat - minLat)) * radiusMultiplier, Math.Abs ((maxLng - minLng)) * radiusMultiplier);
		}

		private static double LatitudeCircumferenceKm (ExtendedPosition position)
		{
			return 40030.1735920411 * Math.Cos (position.Latitude * 3.14159265358979 / 180);
		}

		private static double LatitudeDegreesToKm (double latitudeDegrees)
		{
			return 40030.1735920411 * latitudeDegrees / 360;
		}

		private static double LongitudeDegreesToKm (ExtendedPosition position, double longitudeDegrees)
		{
			return ExtendedMapSpan.LatitudeCircumferenceKm (position) * longitudeDegrees / 360;
		}

		//
		// Methods
		//
		public ExtendedMapSpan ClampLatitude (double north, double south)
		{
			north = Math.Min (Math.Max (north, 0), 90);
			south = Math.Max (Math.Min (south, 0), -90);
			double num = Math.Max (Math.Min (this.Center.Latitude, north), south);
			double val = Math.Min (north - num, -south + num) * 2;
			return new ExtendedMapSpan (new ExtendedPosition (num, this.Center.Longitude), Math.Min (this.LatitudeDegrees, val), this.LongitudeDegrees);
		}

		private bool Equals (ExtendedMapSpan other)
		{
			return this.Center.Equals (other.Center) && this.LongitudeDegrees.Equals (other.LongitudeDegrees) && this.LatitudeDegrees.Equals (other.LatitudeDegrees);
		}

		public override bool Equals (object obj)
		{
			return obj != null && (this == obj || (obj is ExtendedMapSpan && this.Equals ((ExtendedMapSpan)obj)));
		}

		public override int GetHashCode ()
		{
			return (this.Center.GetHashCode () * 397 ^ this.LongitudeDegrees.GetHashCode ()) * 397 ^ this.LatitudeDegrees.GetHashCode ();
		}

		public ExtendedMapSpan WithZoom (double zoomFactor)
		{
			double val = Math.Min (90 - this.Center.Latitude, 90 + this.Center.Latitude) * 2;
			return new ExtendedMapSpan (this.Center, Math.Min (this.LatitudeDegrees / zoomFactor, val), this.LongitudeDegrees / zoomFactor);
		}

		//
		// Operators
		//
		public static bool operator == (ExtendedMapSpan left, ExtendedMapSpan right) {
			return object.Equals (left, right);
		}

		public static bool operator != (ExtendedMapSpan left, ExtendedMapSpan right) {
			return !object.Equals (left, right);
		}
	}
	
}
