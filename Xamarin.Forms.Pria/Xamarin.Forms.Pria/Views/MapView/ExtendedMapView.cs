﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class ExtendedMapView : View
	{
		public enum ExtendedMapType
		{
			Street,
			Satellite,
			Hybrid
		}

		public ExtendedMapView ()
		{
			HorizontalOptions = LayoutOptions.FillAndExpand;
			VerticalOptions = LayoutOptions.FillAndExpand;
		}

		public static readonly BindableProperty HasZoomEnabledProperty = BindableProperty.Create ("HasZoomEnabled", typeof(bool), typeof(ExtendedMapView), true, BindingMode.OneWay, null, null, null, null, null);

		public static readonly BindableProperty MapTypeProperty = BindableProperty.Create ("MapType", typeof(ExtendedMapType), typeof(ExtendedMapView), ExtendedMapType.Street, BindingMode.OneWay, null, null, null, null, null);

		public static readonly BindableProperty IsShowingUserProperty = BindableProperty.Create ("IsShowingUser", typeof(bool), typeof(ExtendedMapView), false, BindingMode.OneWay, null, null, null, null, null);

		public static readonly BindableProperty HasScrollEnabledProperty = BindableProperty.Create ("HasScrollEnabled", typeof(bool), typeof(ExtendedMapView), true, BindingMode.OneWay, null, null, null, null, null);

		public static readonly BindableProperty PinsProperty = BindableProperty.Create ("Pins", typeof(IList<ExtendedMapPin>), typeof(ExtendedMapView), null, BindingMode.OneWay, null, null, null, null, null);

		public static readonly BindableProperty PolylinesProperty = BindableProperty.Create ("Polylines", typeof(IList<ExtendedMapPolyline>), typeof(ExtendedMapView), null, BindingMode.OneWay, null, null, null, null, null);

		public static readonly BindableProperty VisibleRegionProperty = BindableProperty.Create ("VisibleRegion", typeof(ExtendedMapSpan), typeof(ExtendedMapView), null, BindingMode.TwoWay, null, null, null, null, null);

		//
		// Properties
		//
		public bool HasScrollEnabled {
			get {
				return (bool)base.GetValue (ExtendedMapView.HasScrollEnabledProperty);
			}
			set {
				base.SetValue (ExtendedMapView.HasScrollEnabledProperty, value);
			}
		}

		public bool HasZoomEnabled {
			get {
				return (bool)base.GetValue (ExtendedMapView.HasZoomEnabledProperty);
			}
			set {
				base.SetValue (ExtendedMapView.HasZoomEnabledProperty, value);
			}
		}

		public bool IsShowingUser {
			get {
				return (bool)base.GetValue (ExtendedMapView.IsShowingUserProperty);
			}
			set {
				base.SetValue (ExtendedMapView.IsShowingUserProperty, value);
			}
		}

		public IList<ExtendedMapPin> Pins {
			get {
				return (IList<ExtendedMapPin>)base.GetValue (ExtendedMapView.PinsProperty);
			}
			set {
				base.SetValue (ExtendedMapView.PinsProperty, value);
			}
		}

		public IList<ExtendedMapPolyline> Polylines {
			get {
				return (IList<ExtendedMapPolyline>)base.GetValue (ExtendedMapView.PolylinesProperty);
			}
			set {
				base.SetValue (ExtendedMapView.PolylinesProperty, value);
			}
		}

		public ExtendedMapSpan VisibleRegion {
			get {
				return (ExtendedMapSpan)base.GetValue (ExtendedMapView.VisibleRegionProperty);
			}
			set {
				base.SetValue (ExtendedMapView.VisibleRegionProperty, value);
			}
		}
	}
}

