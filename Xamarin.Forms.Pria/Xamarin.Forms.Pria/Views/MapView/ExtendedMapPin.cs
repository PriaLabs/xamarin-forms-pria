using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;

namespace Xamarin.Forms.Pria
{
	public class ExtendedMapPin : ExtendedBindableObject
	{
		internal object Id{ get; set; }

		public Color? BorderColor{ get; set; }

		private ExtendedPosition mPosition;

		public ExtendedPosition Position {
			get { return mPosition; }
			set { 
				if (value != mPosition) {
					mPosition = value;
					OnPropertyChanged ();
				}
			}
		}
		private string mTitle;

		public string Title {
			get { return mTitle; }
			set { 
				if (value != mTitle) {
					mTitle = value;
					OnPropertyChanged ();
				}
			}
		}
		private string mSubtitle;

		public string Subtitle {
			get { return mSubtitle; }
			set { 
				if (value != mSubtitle) {
					mSubtitle = value;
					OnPropertyChanged ();
				}
			}
		}

		private ImageSource mPinImage;

		public ImageSource PinImage {
			get { return mPinImage; }
			set { 
				if (value != mPinImage) {
					mPinImage = value;
					OnPropertyChanged ();
				}
			}
		}
		private Command mCalloutDetailCommand;

		public Command CalloutDetailCommand {
			get { return mCalloutDetailCommand; }
			set { 
				if (value != mCalloutDetailCommand) {
					mCalloutDetailCommand = value;
					OnPropertyChanged ();
				}
			}
		}

		private Command mPinCommand;
		public Command PinCommand
		{
			get { return mPinCommand; }
			set
			{
				if (value != mPinCommand)
				{
					mPinCommand = value;
					OnPropertyChanged();
				}
			}
		}

		private bool mIsSelected;
		public bool IsSelected
		{
			get { return mIsSelected; }
			set
			{
				if (value != mIsSelected)
				{
					mIsSelected = value;
					OnPropertyChanged();
				}
			}
		}



		private object mTag;

		public object Tag
		{
			get { return mTag; }
			set { 
				if (value != mTag) {
					mTag = value;
					OnPropertyChanged ();
				}
			}
		}
	}
}
