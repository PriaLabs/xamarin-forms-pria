﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class RateView : ContentView
	{
		public static readonly BindableProperty RatingProperty = BindableProperty.Create ("Rating", typeof(int), typeof(RateView), 0, BindingMode.TwoWay);

		ImageSource selectedImage, unselectedImage;
		Grid grid;
		List<Image> stars;

		private const int MAX_RATING = 5;

		/// <summary>
		/// Use ImageSource.FromFile() => .FromResource() internally creates StreamImageSource and the stars aren't show sometimes
		/// </summary>
		/// <param name="selectedImage">Selected ImageSource.</param>
		/// <param name="unselectedImage">Unselected ImageSource.</param>
		public RateView (ImageSource selectedImage, ImageSource unselectedImage)
		{
			this.selectedImage = selectedImage;
			this.unselectedImage = unselectedImage;

			Rating = 0;
			stars = new List<Image> ();

			CreateView ();
		}

		public int Rating {
			get{ 
				return (int)base.GetValue (RatingProperty);
			}
			set{
				base.SetValue (RatingProperty, value);
			}
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == RatingProperty.PropertyName)
			{
				CreateView ();
			}
		}

		private void CreateView()
		{
			if (grid == null)
			{
				grid = LayoutHelper.CreateGrid (Enumerable.Repeat (1, MAX_RATING).ToArray (), new int[]{ -1 });
				grid.HorizontalOptions = LayoutOptions.FillAndExpand;
				grid.VerticalOptions = LayoutOptions.FillAndExpand;
				grid.ColumnSpacing = 0;

				for (int i = 1; i <= MAX_RATING; i++) {
					stars.Add (CreateStarView (Rating >= i ? selectedImage : unselectedImage, i));
					grid.Children.Add (stars.Last (), i - 1, 0);
				}

				/*foreach (Image s in stars) {
					s.IsVisible = false;
					s.IsVisible = true;
				}*/
			} 
			else
			{
				for (int i = 1; i <= MAX_RATING; i++) {
					stars [i - 1].Source = Rating >= i ? selectedImage : unselectedImage;
				}
			}

			Content = grid;
		}

		private Image CreateStarView(ImageSource source, int index)
		{
			Image star = new Image (){ Source = source };

			TapGestureRecognizer tgr = new TapGestureRecognizer (){ CommandParameter = index };
			tgr.Tapped += (object sender, EventArgs e) => { 
				if(IsEnabled){
					Rating = (int)((TappedEventArgs)e).Parameter;
				}
			};
			star.GestureRecognizers.Add (tgr);

			return star;
		}
	}
}

