﻿using System;

namespace Xamarin.Forms.Pria
{
	public class ExtendedButton : Button
	{
		public ExtendedButton ()
		{
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (View.IsEnabledProperty.PropertyName == propertyName)
			{
				Opacity = IsEnabled ? 1.0f : 0.5f;
			}
		}
	}
}

