﻿using System;

namespace Xamarin.Forms.Pria
{
	public class TintedImage:Image
	{
		public TintedImage ()
		{
		}

		public static readonly BindableProperty TintColorProperty = BindableProperty.Create ("TintColor", typeof(Color), typeof(TintedImage), Color.Black, BindingMode.OneWay, null, null, null, null);

		//
		// Properties
		//
		public Color TintColor {
			get {
				return (Color)base.GetValue (TintedImage.TintColorProperty);
			}
			set {
				base.SetValue (TintedImage.TintColorProperty, value);
			}
		}
	}
}

