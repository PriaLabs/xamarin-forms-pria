﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Pria;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Xamarin.Forms.Pria
{
	public enum SearchType
	{
		Immediate,
		OnSearchKey
	}

	public class SearchableListView : StackLayout
	{
		public SearchableListView (SearchType searchType)
		{
			SearchBar = new SearchBar ();
			SearchBar.TextChanged += (object sender, TextChangedEventArgs e) => {
				if(string.IsNullOrEmpty(e.NewTextValue)){
					RefreshItemsSource();
					SearchCommand.Execute(null);
				}
				if(searchType == SearchType.Immediate){
					RefreshItemsSource();
					SearchCommand.Execute(e.NewTextValue);
				}

			};
			SearchBar.SearchCommand = new Command (() => {
				RefreshItemsSource();
				SearchCommand.Execute(SearchBar.Text);
			});

			ListView = new BindableListView ();

			LoadingIndicator = new ActivityIndicator (){ IsRunning = true };
			LoadingLabel = new Label () {
				Text = LocalizationManager.Instance.GetText("LoadingLabelText"),
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			LoadingLayout = new StackLayout () {
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Spacing = 10,
				Children = {
					LoadingIndicator,
					LoadingLabel
				}
			};

			LimitLabel = new Label () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				HorizontalTextAlignment = TextAlignment.Center,
				Text = LocalizationManager.Instance.GetText ("LimitLabelText", ItemsLimit)
			};
			LimitLayout = new StackLayout (){ Children = { LimitLabel } };

			EmptyLabel = new Label () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				HorizontalTextAlignment = TextAlignment.Center,
				Text = LocalizationManager.Instance.GetText ("EmptyLabelText")
			};
			EmptyResultLayout = new StackLayout (){ Children = { EmptyLabel } };

			EmptySearchTextLabel = new Label () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				HorizontalTextAlignment = TextAlignment.Center,
				Text = LocalizationManager.Instance.GetText ("EmptySearchTextLabelText")
			};
			EmptySearchTextLayout = new StackLayout (){ Children = { EmptySearchTextLabel } };

			Children.Add (SearchBar);
			Children.Add (ListView);
			Children.Add (LoadingLayout);
			Children.Add (LimitLayout);
			Children.Add (EmptyResultLayout);
			Children.Add (EmptySearchTextLayout);
			Spacing = 0;

			RefreshItemsSource ();
		}

		public BindableListView ListView{get;private set;}

		public SearchBar SearchBar{get;private set;}

		public StackLayout LoadingLayout{ get; set; }
		public StackLayout LimitLayout{ get; private set; }
		public StackLayout EmptyResultLayout{ get; private set; }
		public StackLayout EmptySearchTextLayout{ get; private set; }

		public Label LoadingLabel{ get; set; }
		public ActivityIndicator LoadingIndicator{get;set;}
		public Label LimitLabel{ get; set; }
		public Label EmptyLabel{ get; set; }
		public Label EmptySearchTextLabel{ get; set; }

		private bool allowLoading = true;
		public bool AllowLoading {
			get {
				return allowLoading;
			}
			set {
				allowLoading = value;
				RefreshItemsSource ();
			}
		}

		private bool allowEmpty = true;
		public bool AllowEmptyResult {
			get {
				return allowEmpty;
			}
			set {
				allowEmpty = value;
				RefreshItemsSource ();
			}
		}

		private bool allowEmptySearchText = false;
		public bool AllowEmptySearchText {
			get {
				return allowEmptySearchText;
			}
			set {
				allowEmptySearchText = value;
				RefreshItemsSource ();
			}
		}

		private int itemsLimit = int.MaxValue;
		public int ItemsLimit {
			get {
				return itemsLimit;
			}
			set {
				itemsLimit = value;
				RefreshItemsSource ();
			}
		}

		public static readonly BindableProperty SearchCommandProperty = BindableProperty.Create ("SearchCommand", typeof(ICommand), typeof(SearchableListView), null, BindingMode.OneWay, null, null, null, null);

		public ICommand SearchCommand {
			get {
				return (ICommand)base.GetValue (SearchableListView.SearchCommandProperty);
			}
			set {
				base.SetValue (SearchableListView.SearchCommandProperty, value);
			}
		}

		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create ("ItemsSource", typeof(IList), typeof(SearchableListView), null, BindingMode.OneWay, null, null, null, null);

		public IList ItemsSource {
			get {
				return (IList)base.GetValue (SearchableListView.ItemsSourceProperty);
			}
			set {
				base.SetValue (SearchableListView.ItemsSourceProperty, value);
			}
		}


		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == ItemsSourceProperty.PropertyName)
			{	
				RefreshItemsSource ();
			}
		}

		private void RefreshItemsSource ()
		{
			ListView.IsVisible = ItemsSource != null && ItemsSource.Count > 0 && ItemsSource.Count < ItemsLimit;

			LoadingLayout.IsVisible = AllowLoading && ItemsSource == null && (!AllowEmptySearchText || AllowEmptySearchText && !string.IsNullOrEmpty (SearchBar.Text));

			EmptyResultLayout.IsVisible = AllowEmptyResult && ItemsSource != null && ItemsSource.Count == 0;
			EmptySearchTextLayout.IsVisible = AllowEmptySearchText && ItemsSource == null && string.IsNullOrEmpty (SearchBar.Text);
			LimitLayout.IsVisible = ItemsSource != null && ItemsSource.Count >= ItemsLimit;

			ListView.ItemsSource = ItemsSource;
		}
	}
}

