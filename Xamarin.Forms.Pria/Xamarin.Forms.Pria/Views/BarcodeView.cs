﻿using System;
using Xamarin.Forms.Pria.Services;

namespace Xamarin.Forms.Pria
{
	public class BarcodeView:View
	{
		public BarcodeView ()
		{
		}

		public static readonly BindableProperty BarcodeProperty = BindableProperty.Create ("Barcode", typeof(string), typeof(BarcodeView), null, BindingMode.OneWay);

		public string Barcode {
			get{ 
				return (string)base.GetValue (BarcodeProperty);
			}
			set{
				base.SetValue (BarcodeProperty, value);
			}
		}

		public static readonly BindableProperty BarcodeTypeProperty = BindableProperty.Create ("BarcodeType", typeof(BarcodeType), typeof(BarcodeView), BarcodeType.EAN_13, BindingMode.OneWay);

		public BarcodeType BarcodeType {
			get{ 
				return (BarcodeType)base.GetValue (BarcodeTypeProperty);
			}
			set{
				base.SetValue (BarcodeTypeProperty, value);
			}
		}
	}
}

