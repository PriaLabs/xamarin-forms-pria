﻿using System;

namespace Xamarin.Forms.Pria
{
	public class SquareImage:Image
	{
		public SquareImage ()
		{
		}
		protected override SizeRequest OnSizeRequest (double widthConstraint, double heightConstraint)
		{
			SizeRequest r = OnSizeRequest (widthConstraint, heightConstraint);

			return new SizeRequest (new Size (r.Request.Width, r.Request.Width), new Size (r.Minimum.Width, r.Minimum.Width));
		}
	}
}

