﻿using System;
using System.Collections;
using System.Windows.Input;

namespace Xamarin.Forms.Pria
{
	public class BindableListView : ListView
	{
		public BindableListView (ListViewCachingStrategy cachingStrategy = ListViewCachingStrategy.RecycleElement) : base(cachingStrategy)
		{
			BackgroundColor = Color.Transparent;

			this.ItemTapped += (sender, e) => {
				if(ItemTappedCommand!=null){
					this.SelectedItem = null;
					ItemTappedCommand.Execute(e.Item);
				}
			};

			ItemAppearing += (object sender, ItemVisibilityEventArgs e) =>
			{
				if (LoadMoreCommand == null)
				{
					return;
				}

				IList source = ItemsSource as IList;
				if (source != null && source.Count > 0)
				{
					if (e.Item == lastItemAppeared)
					{
						return;
					}

					if (source.IndexOf(e.Item) == source.Count - 1)
					{
						lastItemAppeared = e.Item;
						LoadMoreCommand.Execute(null);
					}

					if (source.IndexOf(e.Item) == 0)
					{
						lastItemAppeared = null;
					}
				}
			};
		}

		public static readonly BindableProperty ItemTappedCommandProperty = BindableProperty.Create ("ItemTappedCommand", typeof(ICommand), typeof(BindableListView), null, BindingMode.OneWay, null, delegate (BindableObject bo, object o, object n) {
			((BindableListView)bo).OnItemTappedCommandChanged ();
		}, null, null);

		public ICommand ItemTappedCommand {
			get {
				return (ICommand)base.GetValue (BindableListView.ItemTappedCommandProperty);
			}
			set {
				base.SetValue (BindableListView.ItemTappedCommandProperty, value);
			}
		}

		private void OnItemTappedCommandChanged ()
		{

		}

		object lastItemAppeared;


		public static readonly BindableProperty LoadMoreCommandProperty = BindableProperty.Create("LoadMoreCommand", typeof(ICommand), typeof(BindableListView), null);

		public ICommand LoadMoreCommand
		{
			get
			{
				return (ICommand)GetValue(LoadMoreCommandProperty);
			}
			set
			{
				SetValue(LoadMoreCommandProperty, value);
			}
		}

	}
}

