﻿using System;
using Xamarin.Forms;

namespace Xamarin.Forms.Pria
{
	public class RoundedImageView : Image
	{
		public RoundedImageView (float radius)
		{
			this.Radius = radius;
		}

		public float Radius{get;set;}
	}
}

