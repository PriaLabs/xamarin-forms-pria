﻿using System;
using System.Collections;
using System.Diagnostics;

namespace Xamarin.Forms
{
	public class BindablePicker : Picker
	{
		public string Placeholder{ get; set; }
		public string CloseButtonText{ get; set; }
		public string OKButtonText{ get; set; }

		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create ("ItemsSource", typeof(IEnumerable), typeof(BindablePicker), null, BindingMode.OneWay, null, null, null, null);

		public IEnumerable ItemsSource {
			get {
				return (IEnumerable)base.GetValue (BindablePicker.ItemsSourceProperty);
			}
			set {
				base.SetValue (BindablePicker.ItemsSourceProperty, value);
			}
		}
			
		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == ItemsSourceProperty.PropertyName && ItemsSource != null) {
				IEnumerable items = ItemsSource;

				Items.Clear ();

				foreach (object item in items) {
					Items.Add (item.ToString ());
				}
			}
		}

		protected override void OnBindingContextChanged ()
		{
			// fix for invoking old binding (xamarin.forms bug?)
			//RemoveBinding (BindablePicker.ItemsSourceProperty);
			//RemoveBinding (BindablePicker.SelectedIndexProperty);

			base.OnBindingContextChanged ();
		}
	}
}

