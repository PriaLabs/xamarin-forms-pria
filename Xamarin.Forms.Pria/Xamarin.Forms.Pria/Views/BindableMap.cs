﻿using System;
using Xamarin.Forms.Maps;
using System.Collections;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;

namespace Xamarin.Forms.Pria
{
	public class BindableMap : Map
	{
		public BindableMap ()
		{
		}

		public static readonly BindableProperty PinsSourceProperty = BindableProperty.Create ("ItemsSource", typeof(IList<Pin>), typeof(BindableMap), null);

		public static readonly BindableProperty PinTappedCommandProperty = BindableProperty.Create ("ItemTappedCommand", typeof(ICommand), typeof(BindableMap), null, BindingMode.OneWay, null, delegate (BindableObject bo, object o, object n) {
			((BindableMap)bo).OnPinTappedCommandChanged ();
		}, null, null);


		public static readonly BindableProperty RegionProperty = BindableProperty.Create ("Region", typeof(MapSpan), typeof(BindableMap), null);


		protected override void OnPropertyChanging (string propertyName)
		{
			base.OnPropertyChanging (propertyName);

			if (propertyName == PinsSourceProperty.PropertyName && ItemsSource != null)
			{
				if (ItemsSource is INotifyCollectionChanged) {
					((INotifyCollectionChanged)ItemsSource).CollectionChanged -= HandleCollectionChanged;
				}
			}
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == PinsSourceProperty.PropertyName && ItemsSource != null)
			{
				if (ItemsSource is INotifyCollectionChanged) {
					((INotifyCollectionChanged)ItemsSource).CollectionChanged += HandleCollectionChanged;
				}

				RebindPins ();
			}

			if (propertyName == "VisibleRegion") {
				if (!MapSpan.Equals (Region, VisibleRegion)) {
					Region = VisibleRegion;
					//Debug.WriteLine (VisibleRegion.Center.Latitude + " : " + VisibleRegion.Center.Longitude);
				}
			}
			if (propertyName == RegionProperty.PropertyName) {
				if (!MapSpan.Equals (Region, VisibleRegion)) {
					if (Region != null) {
						MoveToRegion (Region);
						//Debug.WriteLine ("MoveToRegion: " + Region.Center.Latitude + " : " + Region.Center.Longitude);
					}
				}
			}
		}
			
		void HandleCollectionChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Add) {
				foreach (var item in e.NewItems) {
					AddPin ((Pin)item);
				}
			} 
			else if (e.Action == NotifyCollectionChangedAction.Remove) {
				foreach (var item in e.NewItems) {
					RemovePin ((Pin)item);
				}
			} 
			else if (e.Action == NotifyCollectionChangedAction.Reset) {
				RebindPins ();
			}
		}

		private void RebindPins()
		{
			while (Pins.Count > 0) {
				RemovePin (Pins[0]);
			}

			foreach (var item in ItemsSource) {
				AddPin (item);
			}
		}

		private void AddPin(Pin pin)
		{
			pin.Clicked += PinClickedHandler;
			Pins.Add (pin);
		}

		private void RemovePin(Pin pin)
		{
			pin.Clicked -= PinClickedHandler;
			Pins.Remove (pin);
		}

		void PinClickedHandler (object sender, EventArgs e)
		{
			if (ItemTappedCommand != null) {
				ItemTappedCommand.Execute (sender);
			}
		}

		public MapSpan Region {
			get{
				return (MapSpan)base.GetValue (RegionProperty);
			}
			set{
				base.SetValue (RegionProperty, value);
			}
		}

		public IList<Pin> ItemsSource {
			get{
				return (IList<Pin>)base.GetValue (PinsSourceProperty);
			}
			set{
				base.SetValue (PinsSourceProperty, value);
			}
		}
			
		public ICommand ItemTappedCommand {
			get {
				return (ICommand)base.GetValue (BindableMap.PinTappedCommandProperty);
			}
			set {
				base.SetValue (BindableMap.PinTappedCommandProperty, value);
			}
		}

		private void OnPinTappedCommandChanged ()
		{
		}
	}
}

