﻿using System;
using System.Collections;
using System.Linq;
using System.Windows.Input;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class CardView : ContentView
	{
		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create("ItemsSource",typeof(IList),typeof(CardView),null);

		public static readonly BindableProperty ItemTemplateProperty  = BindableProperty.Create("ItemTemplate",typeof(Func<View>),typeof(CardView),null);

		public static readonly BindableProperty ColumnWeightsProperty  = BindableProperty.Create("ColumnWeights",typeof(int[]),typeof(CardView),null);

		public static readonly BindableProperty RowWeightProperty = BindableProperty.Create ("RowWeight", typeof(int), typeof(CardView), -1);

		public static readonly BindableProperty ItemTappedCommandProperty = BindableProperty.Create ("ItemTappedCommand", typeof(ICommand), typeof(BindableListView), null, BindingMode.OneWay, null, null, null, null);

		Grid grid;

		public CardView ()
		{
			Content = grid;
		}

		private int columnSpacing;
		public int ColumnSpacing{
			get{
				return columnSpacing; 
			}
			set{
				columnSpacing = value;
				if (grid != null) {
					grid.ColumnSpacing = value;
					grid.ForceLayout ();
				}
			}
		}

		private int rowSpacing;
		public int RowSpacing{
			get{
				return rowSpacing; 
			}
			set{
				rowSpacing = value;
				if (grid != null) {
					grid.RowSpacing = value;
					grid.ForceLayout ();
				}
			}
		}

		public IList ItemsSource {
			get{
				return (IList)base.GetValue (ItemsSourceProperty);
			}
			set{
				base.SetValue (ItemsSourceProperty, value);
			}
		}

		public Func<View> ItemTemplate {
			get{ 
				return (Func<View>)base.GetValue (ItemTemplateProperty);
			}
			set{
				base.SetValue (ItemTemplateProperty,value);
			}
		}

		public int[] ColumnWeights {
			get{ 
				return (int[])base.GetValue (ColumnWeightsProperty);
			}
			set{
				base.SetValue (ColumnWeightsProperty,value);
			}
		}

		public int RowWeight {
			get{ 
				return (int)base.GetValue (RowWeightProperty);
			}
			set{
				base.SetValue (RowWeightProperty,value);
			}
		}

		public ICommand ItemTappedCommand {
			get {
				return (ICommand)base.GetValue (CardView.ItemTappedCommandProperty);
			}
			set {
				base.SetValue (CardView.ItemTappedCommandProperty, value);
			}
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == ItemsSourceProperty.PropertyName || propertyName == ItemTemplateProperty.PropertyName || propertyName == ColumnWeightsProperty.PropertyName || propertyName == RowWeightProperty.PropertyName) {
				CreateLayout ();
			}
		}

		private void CreateLayout(){
			if (ColumnWeights != null && ItemTemplate != null) { 
				if (grid != null) {
					Content = null;
					grid = null;
				}
				if (ItemsSource == null) {
					return;
				}

				int rows = (int) Math.Ceiling(ItemsSource.Count / (double)ColumnWeights.Length);

				grid = LayoutHelper.CreateGrid (ColumnWeights, Enumerable.Repeat (RowWeight, rows).ToArray());
				grid.HorizontalOptions = LayoutOptions.FillAndExpand;
				grid.VerticalOptions = LayoutOptions.FillAndExpand;
				grid.ColumnSpacing = ColumnSpacing;
				grid.RowSpacing = RowSpacing;

				for(int i = 0;i<ItemsSource.Count;i++){
					View v = new ContentView (){ Content = ItemTemplate.Invoke () };
					v.BindingContext = ItemsSource [i];
					grid.Children.Add(v,i % ColumnWeights.Length,i / ColumnWeights.Length);

					TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ();
					tapGestureRecognizer.Tapped += (sender, e) => {
						if (ItemTappedCommand != null && IsEnabled) {
							ItemTappedCommand.Execute (v.BindingContext);
						}
					};
					v.GestureRecognizers.Add (tapGestureRecognizer);
				}

				Content = grid;
			}
		}
	}
}

