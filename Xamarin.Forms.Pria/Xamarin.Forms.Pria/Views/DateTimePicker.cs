﻿using System;

namespace Xamarin.Forms.Pria
{
	public class DateTimePicker : DatePicker
	{
		public string Placeholder{ get; set; }
		public string CloseButtonText{ get; set; }
		public string OKButtonText{ get; set; }
		public Color TintColor{ get; set; }

		public static readonly BindableProperty DateTimeProperty = BindableProperty.Create ("DateTime", typeof(DateTime?), typeof(DateTimePicker), null, BindingMode.TwoWay, null, null, null, null, null);

		public DateTime? DateTime {
			get {
				return (DateTime?)base.GetValue (DateTimePicker.DateTimeProperty);
			}
			set {
				base.SetValue (DateTimePicker.DateTimeProperty, value);
			}
		}
	}
}

