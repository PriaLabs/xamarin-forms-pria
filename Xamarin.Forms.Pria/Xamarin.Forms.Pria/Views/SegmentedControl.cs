﻿using System;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class SegmentedControl : View, IViewContainer<SegmentedControlOption>
	{
		public IList<SegmentedControlOption> Children { get; set; }

		// used in Android only
		public Color TextColor{ get; set; }

		public SegmentedControl (Color tintColor)
		{
			Children = new List<SegmentedControlOption> ();
			TintColor = tintColor;
		}

		public Color TintColor{ get; private set; }

		public static readonly BindableProperty SelectedIndexProperty = BindableProperty.Create ("SelectedIndex", typeof(int), typeof(SegmentedControl), 0, BindingMode.TwoWay);

		public int SelectedIndex {
			get {
				return (int)base.GetValue (SegmentedControl.SelectedIndexProperty);
			}
			set {
				base.SetValue (SegmentedControl.SelectedIndexProperty, value);
			}
		}
	}

	public class SegmentedControlOption : View
	{
		public static readonly BindableProperty TextProperty = BindableProperty.Create<SegmentedControlOption, string> (p => p.Text, string.Empty);

		public string Text {
			get{ return (string)GetValue (TextProperty); }
			set{ SetValue (TextProperty, value); }
		}
	}
}

