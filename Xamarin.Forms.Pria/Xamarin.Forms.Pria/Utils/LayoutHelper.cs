﻿using System;

namespace Xamarin.Forms.Pria
{
	public static class LayoutHelper
	{
		/// <summary>
		/// Creates grid.
		/// </summary>
		/// <param name="columns">Number of columns.</param>
		/// <param name="rows">Number of rows.</param>
		/// <param name="columnWeights">Positive ones are Stars, negative ones are Auto.</param>
		/// <param name="rowWeights">Positive ones are Stars, negative ones are Auto. </param>
		/// 
		public static Grid CreateGrid(int[] columnWeights, int[] rowWeights)
		{
			Grid g = new Grid (){ ColumnSpacing = 0, RowSpacing = 0 };
			for (int i = 0; i < columnWeights.Length; i++) {
				if (columnWeights [i] < 0) {
					g.ColumnDefinitions.Add (new ColumnDefinition (){ Width = new GridLength (Math.Abs(columnWeights [i]), GridUnitType.Auto) });
				} else {
					g.ColumnDefinitions.Add (new ColumnDefinition (){ Width = new GridLength (columnWeights [i], GridUnitType.Star) });
				}
			}
			for (int i = 0; i < rowWeights.Length; i++) {
				if (rowWeights [i] < 0) {
					g.RowDefinitions.Add (new RowDefinition (){ Height = new GridLength (Math.Abs(rowWeights [i]), GridUnitType.Auto) });
				} else {
					g.RowDefinitions.Add (new RowDefinition (){ Height = new GridLength (rowWeights [i], GridUnitType.Star) });
				}
			}
			return g;
		}
		public static RelativeLayout WrapToRelativeLayout(View backgroundImage, View image, View bottomTitle,float bottomTitleRatio)
		{
			RelativeLayout wrapper = new RelativeLayout () {
			};
			wrapper.Children.Add (backgroundImage, Constraint.Constant (0), Constraint.Constant (0),Constraint.RelativeToParent((rl)=>rl.Width),Constraint.RelativeToParent((rl)=>rl.Height));
			wrapper.Children.Add (image, Constraint.Constant (0), Constraint.Constant (0),Constraint.RelativeToParent((rl)=>rl.Width),Constraint.RelativeToParent((rl)=>rl.Height));
			wrapper.Children.Add (bottomTitle,Constraint.Constant (0), Constraint.RelativeToParent (rl => (1.0f - bottomTitleRatio)*rl.Height),Constraint.RelativeToParent((rl)=>rl.Width),Constraint.RelativeToParent (rl => bottomTitleRatio*rl.Height));

			return wrapper;
		}
		public static RelativeLayout WrapToRelativeLayout(View image, View bottomTitle,float bottomTitleRatio)
		{
			RelativeLayout wrapper = new RelativeLayout () {
			};

			wrapper.Children.Add (image, Constraint.Constant (0), Constraint.Constant (0),Constraint.RelativeToParent((rl)=>rl.Width),Constraint.RelativeToParent((rl)=>rl.Height));
			wrapper.Children.Add (bottomTitle,Constraint.Constant (0), Constraint.RelativeToParent (rl => (1.0f - bottomTitleRatio)*rl.Height),Constraint.RelativeToParent((rl)=>rl.Width),Constraint.RelativeToParent (rl => bottomTitleRatio*rl.Height));
		
			return wrapper;
		}
	}
}

