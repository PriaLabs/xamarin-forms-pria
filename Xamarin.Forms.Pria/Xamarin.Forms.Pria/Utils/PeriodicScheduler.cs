﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Xamarin.Forms.Pria.Utils
{
	public class PeriodicSchedulerItem{
		public string ID{ get; set;}

		public long LastValueTicks{get;set;}

		[XmlIgnore]
		public TimeSpan LastValue { 
			get { 
				return TimeSpan.FromTicks (LastValueTicks); 
			}
			set {
				LastValueTicks = value.Ticks;
			}
		}
	}
	public class PeriodicScheduler
	{
		public List<PeriodicSchedulerItem> Items{ get; set;}

		void LoadItems ()
		{
			Items = new IsolatedStoragePersistor<List<PeriodicSchedulerItem>> ().Load (typeof(PeriodicScheduler).FullName) ?? new List<PeriodicSchedulerItem> ();
		}
		void SaveItems ()
		{
			new IsolatedStoragePersistor<List<PeriodicSchedulerItem>> ().Save (typeof(PeriodicScheduler).FullName,Items);
		}

		public PeriodicScheduler ()
		{
			LoadItems ();
		}


		public TimeSpan GetNextPeriod(string ID,TimeSpan minPeriod,TimeSpan maxPeriod,TimeSpan initialPeriod, bool lastHit){
			PeriodicSchedulerItem item = Items.Find (i => i.ID == ID);
			if (item == null) {
				item = new PeriodicSchedulerItem (){ ID = ID,LastValue = initialPeriod };
				Items.Add (item);
			}

			if (lastHit) {
				item.LastValue = minPeriod;
			} else {
				TimeSpan newValue = item.LastValue + item.LastValue;
				if (newValue < maxPeriod) {
					item.LastValue = newValue;
				}
			}

			SaveItems ();
			return item.LastValue;
		}
	}
}

