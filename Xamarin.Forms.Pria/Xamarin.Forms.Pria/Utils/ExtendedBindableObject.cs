﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class ExtendedBindableObject:BindableObject
	{
		class ObservedProperty{
			public string CallerProperty{ get; set; }
			public object Source{get;set;}
			public string SourceProperty{ get; set; }
		}

		public ExtendedBindableObject ()
		{
		}

		private List<ObservedProperty> observedProperties = new List<ObservedProperty>();

		public T ObserveProperty<T,U>(U source,Expression<Func<U,T>> projection,[CallerMemberName] string propertyName = null) where U : INotifyPropertyChanged
		{
			string sourceProperty = ((MemberExpression)projection.Body).Member.Name;
			if (!observedProperties.Exists (op => op.CallerProperty == propertyName && object.Equals(op.Source,source) && op.SourceProperty == sourceProperty))
			{
				source.PropertyChanged += (s, e) => {
					if (e.PropertyName == sourceProperty)
						OnPropertyChanged (propertyName);
				};
				observedProperties.Add(new ObservedProperty(){Source = source,SourceProperty = sourceProperty,CallerProperty=propertyName});
			}

			return projection.Compile ().Invoke (source);
		}
	}
}

