﻿using System;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Xamarin.Forms.Pria
{
	public class DistanceHelper
	{
		public Position? p1{ get; private set; }
		public Position? p2{ get; private set; }

		public DistanceHelper (Position? p1, Position? p2)
		{
			this.p1 = p1;
			this.p2 = p2;
		}

		public double Distance{
			get{
				return LocationUtils.GetDistance (p1, p2);
			}
		}
	}

	public static class LocationUtils
	{
		/// <summary>
		/// Gets the distance in KM
		/// </summary>
		/// <returns>The distance.</returns>
		/// <param name="p1">P1.</param>
		/// <param name="p2">P2.</param>
		public static double GetDistance(Position? p1, Position? p2)
		{
			double e=(Math.PI*p1.Value.Latitude/180);
			double f=(Math.PI*p1.Value.Longitude/180);
			double g=(Math.PI*p2.Value.Latitude/180);
			double h=(Math.PI*p2.Value.Longitude/180);
			double i=(Math.Cos(e)*Math.Cos(g)*Math.Cos(f)*Math.Cos(h)+Math.Cos(e)*Math.Sin(f)*Math.Cos(g)*Math.Sin(h)+Math.Sin(e)*Math.Sin(g));
			double j=(Math.Acos(i));
			double k=(6371*j);

			return k;
		}

		public static MapSpan DefaultMapSpanPrague(int radiusKM){
			return MapSpan.FromCenterAndRadius (new Position (50.0800572f, 14.4277953f), Distance.FromKilometers (radiusKM));
		}

		public static MapSpan DefaultMapSpanCzechRepublic(){
			return MapSpan.FromCenterAndRadius (new Position (49.814735f, 15.47995f), Distance.FromKilometers (250));
		}

		public static async Task<Position?> GetUserPosition()
		{
			Position? position = null;

			await DependencyService.Get<Xamarin.Forms.Pria.Services.IGeolocator> ().GetPositionAsync (2000).ContinueWith (t => {
				if (!t.IsFaulted && !t.IsCanceled) {
					position = new Position (t.Result.Latitude, t.Result.Longitude);
				}
			} );

			return position;
		}

		public static MapSpan FromPoints(List<Position> points, double radiusMultiplier = 1.0f)
		{
			Position? smallestLat = null, smallestLng = null, biggestLat = null, biggestLng = null;

			smallestLat = points.OrderByDescending (p => p.Latitude).First();
			smallestLng = points.OrderByDescending (p => p.Longitude).First();
			biggestLat = points.OrderBy (p => p.Latitude).First();
			biggestLng = points.OrderBy (p => p.Longitude).First();

			List<DistanceHelper> distances = new List<DistanceHelper> () {
				new DistanceHelper (smallestLat, smallestLng),
				new DistanceHelper (smallestLat, biggestLat),
				new DistanceHelper (smallestLat, biggestLng),
				new DistanceHelper (smallestLng, biggestLng),
				new DistanceHelper (smallestLng, biggestLng),
				new DistanceHelper (biggestLat, biggestLng)
			};

			DistanceHelper max = distances.OrderByDescending (o => o.Distance).First ();
			Position center = new Position ((max.p1.Value.Latitude + max.p2.Value.Latitude) / 2.0, (max.p1.Value.Longitude + max.p2.Value.Longitude) / 2.0);
			double distance = (LocationUtils.GetDistance (center, max.p1) * radiusMultiplier);

			return MapSpan.FromCenterAndRadius (center, Distance.FromKilometers(distance));
		}

		public class GeoAngle
		{
			public bool IsNegative { get; set; }
			public int Degrees { get; set; }
			public int Minutes { get; set; }
			public int Seconds { get; set; }
			public int Milliseconds { get; set; }

			public static GeoAngle FromDouble(double angleInDegrees)
			{
				//ensure the value will fall within the primary range [-180.0..+180.0]
				while (angleInDegrees < -180.0)
					angleInDegrees += 360.0;

				while (angleInDegrees > 180.0)
					angleInDegrees -= 360.0;

				var result = new GeoAngle();

				//switch the value to positive
				result.IsNegative = angleInDegrees < 0;
				angleInDegrees = Math.Abs(angleInDegrees);

				//gets the degree
				result.Degrees = (int)Math.Floor(angleInDegrees);
				var delta = angleInDegrees - result.Degrees;

				//gets minutes and seconds
				var seconds = (int)Math.Floor(3600.0 * delta);
				result.Seconds = seconds % 60;
				result.Minutes = (int)Math.Floor(seconds / 60.0);
				delta = delta * 3600.0 - seconds;

				//gets fractions
				result.Milliseconds = (int)(1000.0 * delta);

				return result;
			}



			public override string ToString()
			{
				var degrees = this.IsNegative
					? -this.Degrees
					: this.Degrees;

				return string.Format(
					"{0}° {1:00}' {2:00}\"",
					degrees,
					this.Minutes,
					this.Seconds);
			}



			public string ToString(string format)
			{
				switch (format)
				{
				case "NS":
					return string.Format(
						"{0}°{1:00}'{2:00}\" {4}",
						this.Degrees,
						this.Minutes,
						this.Seconds,
						this.Milliseconds,
						this.IsNegative ? 'S' : 'N');

				case "WE":
					return string.Format(
						"{0}°{1:00}'{2:00}\" {4}",
						this.Degrees,
						this.Minutes,
						this.Seconds,
						this.Milliseconds,
						this.IsNegative ? 'W' : 'E');

				default:
					throw new NotImplementedException();
				}
			}
		}
	}
}

