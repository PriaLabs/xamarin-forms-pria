﻿using System;
using System.Reflection;
using System.Linq;
using System.Collections;

namespace Xamarin.Forms.Pria
{
	public static class ExtensionMethods
	{
		public static string GetQueryString(this object request)
		{
			// Get all properties on the object
			var properties = request.GetType().GetRuntimeProperties()
				.Where(x => x.CanRead)
				.Where(x => x.GetValue(request, null) != null)
				.ToDictionary(x => x.Name, x => x.GetValue(request, null));

			// Get names for all IEnumerable properties (excl. string)
			var propertyNames = properties
				.Where(x => !(x.Value is string) && x.Value is IEnumerable)
				.Select(x => x.Key)
				.ToList();

			// Concat all IEnumerable properties into a comma separated string
			foreach (var key in propertyNames)
			{
				var valueType = properties[key].GetType();
				var valueElemType = valueType.IsConstructedGenericType
					? valueType.GenericTypeArguments[0]
					: valueType.GetElementType();
				if (valueElemType == typeof (string))
				{
					var enumerable = properties[key] as IEnumerable;
					properties[key] = string.Join(",", enumerable.Cast<object>());
				}
			}

			// Concat all key/value pairs into a string separated by ampersand
			return string.Join("&", properties
				.Select(x => string.Concat(
					Uri.EscapeDataString(x.Key), "=",
					Uri.EscapeDataString(x.Value.ToString()))));
		}
	}
}

