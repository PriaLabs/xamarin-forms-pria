﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Diagnostics;
using Xamarin.Forms.Pria.Services;
using System.Linq;
using System.Xml;

namespace Xamarin.Forms.Pria.Utils
{
	public class IsolatedStoragePersistor<T>
	{
		static object syncObj = new object();

        private IIsolatedStorage storage;
		public IsolatedStoragePersistor ()
		{
            storage = DependencyService.Get<IIsolatedStorage>();
		}

        public IsolatedStoragePersistor(IIsolatedStorage storage)
        {
            this.storage = storage;
        }

		private string KeyToFileName(string key){
			string result = System.Uri.EscapeDataString (key);
			return result;
		}

		public void Save(string key, T data){
			lock (syncObj)
			{
				using (MemoryStream ms = new MemoryStream())
				{
					XmlSerializer serializer = new XmlSerializer(typeof(T));

					StreamWriter sw = new StreamWriter(ms, System.Text.Encoding.UTF8);
					XmlWriter xmlWriter = XmlWriter.Create(sw, new XmlWriterSettings() { Encoding = System.Text.Encoding.UTF8 });

					serializer.Serialize(xmlWriter, data);

					sw.Flush();

					ms.Seek(0, SeekOrigin.Begin);

					//atomic operation - delete and then create new one
					if (storage.Exists(KeyToFileName(key)))
					{
						storage.Delete(KeyToFileName(key));
					}
					using (Stream s = storage.Create(KeyToFileName(key)))
					{
						ms.WriteTo(s);
					}
				}
			}
		}

		public T Load(string key){
            if (Exists(key))
            {
				lock(syncObj)
				{
					using (Stream s = storage.OpenRead(KeyToFileName(key)))
					{
						XmlSerializer serializer = new XmlSerializer(typeof(T));

						return (T)serializer.Deserialize(s);
					}
				}
			}
			return default(T);
		}

		public string GetFullPath(string key){
			return storage.GetFullPath(KeyToFileName(key));
		}

		public bool Exists(string key){
			return storage.Exists (KeyToFileName(key));
		}

		public void PurgeCache(string prefix){
            lock (syncObj)
			{
				List<string> files = storage.GetNames(null).ToList();
				foreach (string file in files)
				{
					if (file.Contains(prefix))
					{
						Debug.WriteLine("deleting file: " + file);
						storage.Delete(file);
					}
				}
			}

		}
	}
}

