﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

namespace Xamarin.Forms.Pria.Utils
{
	public class HttpApi
	{
		public HttpApi ()
		{
		}

		public const string CONTENT_TYPE_JSON = "application/json";
		public const string CONTENT_TYPE_XML = "application/xml";

		public enum ResultFormat
		{
			XML,
			Json,
			Custom
		}

		public enum HttpMethod
		{
			GET,
			POST,
			PUT,
			DELETE
		}

		public async Task<Stream> Call(string baseUrl,ResultFormat format= ResultFormat.Custom, HttpMethod method = HttpMethod.GET, string contentType = null, Dictionary<string, string> httpParameters = null, string requestBody = null, string userAgent = null, Stream destinationStream = null,Action<int> downloadProgress = null)
        {
			StringBuilder sb = new StringBuilder();
			if (httpParameters!=null && httpParameters.Keys.Count > 0)
			{
				sb.AppendFormat("{0}?", baseUrl);
				foreach (string key in httpParameters.Keys) {
					sb.AppendFormat ("{0}={1}&", Uri.EscapeDataString (key), Uri.EscapeDataString (httpParameters [key]));
				}
			}
			else
			{
				sb.Append(baseUrl);
			}

			string url = sb.ToString();

			HttpWebRequest request = HttpWebRequest.CreateHttp(url);

            request.Headers = new WebHeaderCollection();
           	
			request.Method = GetMethodString (method);

            if (!string.IsNullOrEmpty(userAgent))
            {
				SetUserAgent (request, userAgent);
			}

			if (!string.IsNullOrEmpty (contentType)) {
				request.ContentType = contentType;
			}

			switch (format)
			{
			case ResultFormat.Json:
				request.Accept = CONTENT_TYPE_JSON;
				break;
			case ResultFormat.XML:
				request.Accept = CONTENT_TYPE_XML;
				break;
			default:
				request.Accept = "*/*";
				break;
			}

			if (!string.IsNullOrEmpty (requestBody)) {
				using (var requestStream = (Stream)(await Task<Stream>.Factory.FromAsync (request.BeginGetRequestStream, request.EndGetRequestStream, null).ConfigureAwait (false))) {
					using (StreamWriter sw = new StreamWriter (requestStream)) {
						sw.Write (requestBody);
					}
				}
			}

			using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null).ConfigureAwait(false)))
			{
				// HANDLE REDIRECT
				// 3xx status codes
				bool hasRedirection = response.StatusCode == HttpStatusCode.Found
				                      || response.StatusCode == HttpStatusCode.Moved
				                      || response.StatusCode == HttpStatusCode.MovedPermanently
				                      || response.StatusCode == HttpStatusCode.MultipleChoices
				                      || response.StatusCode == HttpStatusCode.SeeOther
				                      || response.StatusCode == HttpStatusCode.UseProxy
				                      || response.StatusCode == HttpStatusCode.TemporaryRedirect
				                      || response.StatusCode == HttpStatusCode.Redirect
				                      || response.StatusCode == HttpStatusCode.Ambiguous;

				if (hasRedirection && !string.IsNullOrEmpty (response.Headers ["Location"])) {
					return await Call (response.Headers ["Location"], format, method, contentType, httpParameters, requestBody, userAgent, destinationStream, downloadProgress);
				}
					
				using (var responseStream = response.GetResponseStream())
				{
					int progressPercent = -1;

					Stream ms = destinationStream ?? new MemoryStream();
					var buffer = new byte[1024 * 128]; //128kB
					int bytesReceived;
					long totalBytesReceived = 0;
					long totalBytes = response.ContentLength;

					while ((bytesReceived = responseStream.Read(buffer, 0, buffer.Length)) != 0)
					{
						totalBytesReceived += bytesReceived;
						ms.Write(buffer, 0, bytesReceived);

						if ((totalBytesReceived * 100) / totalBytes != progressPercent) {
							progressPercent = (int) ((totalBytesReceived * 100) / totalBytes);
							if (downloadProgress != null) {
								downloadProgress.Invoke (progressPercent);
							}
						}
					}
					if (totalBytes >= 0 && totalBytes != totalBytesReceived) {
						throw new WebException ("Downloading interrupted.");
					}
					ms.Seek(0, SeekOrigin.Begin);

					return ms;
				}
			}
		}

		public async Task<T> Call<T>(string baseUrl,ResultFormat format,HttpMethod method = HttpMethod.GET, string contentType = null, Dictionary<string,string> httpParameters = null,string requestBody = null,Func<Stream,T> customDeserializer=null, Stream destinationStream = null,Action<int> downloadProgress = null)
		{
			Stream ms = await Call (baseUrl,format, method, contentType, httpParameters, requestBody, null, destinationStream,downloadProgress);

			switch (format) {
			case ResultFormat.XML:
				ms.Seek (0, SeekOrigin.Begin);
				return (T)new XmlSerializer (typeof(T)).Deserialize (ms); 
			case ResultFormat.Json:
				string datajson = new StreamReader (ms).ReadToEnd ();
				return Newtonsoft.Json.JsonConvert.DeserializeObject<T> (datajson);
			case ResultFormat.Custom:
				return customDeserializer.Invoke (ms);
			}
				
			return default(T);
		}

		private void SetUserAgent(HttpWebRequest request, string userAgent)
		{
			Type type = request.GetType();

			System.Reflection.PropertyInfo prop = type.GetRuntimeProperty("UserAgent");

			if (prop != null)
			{
				prop.SetValue(request, userAgent, null);
			}
		}

		private string GetMethodString(HttpMethod method)
		{
			switch(method){
			case HttpMethod.POST: return "POST";
			case HttpMethod.DELETE: return "DELETE";
			case HttpMethod.PUT: return "PUT";
			default:
			case HttpMethod.GET: return "GET";
			}
		}

	}
}

