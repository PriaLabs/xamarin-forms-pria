﻿using System;
using HashLib.Crypto;
using HashLib;

namespace Xamarin.Forms.Pria
{
	public static class Crypto
	{
		public static byte[] SHA1(byte[] data){
			SHA1 sha1 = new SHA1();
			HashResult hr = sha1.ComputeBytes (data);
			return hr.GetBytes ();
		}

	}
}

