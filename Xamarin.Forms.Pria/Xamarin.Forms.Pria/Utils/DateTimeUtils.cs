﻿using System;

namespace Xamarin.Forms.Pria
{
	public static class DateTimeUtils
	{
		public static DateTime Now{
			get{
				DateTime now = DateTime.Now;

				// Android LocalTime convert bug - doesn't respect daylight saving time offset (https://bugzilla.xamarin.com/show_bug.cgi?id=10901)
				// Should be fixed

				/*if (Device.OS == TargetPlatform.Android && now.IsDaylightSavingTime ()) {
					now = now.AddHours (1);
				}*/

				return now;
			}
		}

		public static DateTime UtcNow{
			get{
				return DateTime.UtcNow;
			}
		}
	}
}

