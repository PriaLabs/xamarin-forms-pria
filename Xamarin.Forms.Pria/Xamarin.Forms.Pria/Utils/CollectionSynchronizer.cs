﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Xamarin.Forms.Pria
{
	public class CollectionSynchronizer<S,D> : IDisposable
	{
		IList<S> source;
		IList<D> dest;
		Func<S, D> converter;
		public CollectionSynchronizer(IList<S> source, IList<D> dest,Func<S, D> converter)
		{
			this.source = source;
			this.dest = dest;
			this.converter = converter;

			INotifyCollectionChanged cc = source as INotifyCollectionChanged;
			if (cc != null)
			{
				cc.CollectionChanged += HandleCollectionChanged;
			}

			DoFullSynchronization();
		}

		void HandleCollectionChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
			case NotifyCollectionChangedAction.Add:
				{
					int index = e.NewStartingIndex;
					foreach (var item in e.NewItems.Cast<S>())
						dest.Insert(index++, converter.Invoke(item));
				}
				break;
			case NotifyCollectionChangedAction.Move:
				{
					D item = dest[e.OldStartingIndex];
					dest.RemoveAt(e.OldStartingIndex);
					dest.Insert(e.NewStartingIndex, item);
				}
				break;
			case NotifyCollectionChangedAction.Remove:
				{
					dest.RemoveAt(e.OldStartingIndex);
				}
				break;
			case NotifyCollectionChangedAction.Replace:
				{
					dest.RemoveAt(e.OldStartingIndex);
					dest.Insert(e.NewStartingIndex, converter.Invoke(source[e.NewStartingIndex]));
				}
				break;
			case NotifyCollectionChangedAction.Reset:
				DoFullSynchronization();
				break;			
			}
		}
		public void RefreshDestinationItems(){
			for (int i = 0; i < dest.Count; i++) {
				dest [i] = converter (source [i]);
			}
		}
		void DoFullSynchronization()
		{
			dest.Clear();
			foreach (S item in source) {
				dest.Add (converter (item));
			}
		}

		public void Dispose()
		{
			if (source != null)
			{
				INotifyCollectionChanged cc = source as INotifyCollectionChanged;
				if (cc != null)
				{
					cc.CollectionChanged -= HandleCollectionChanged;
				}
				source = null;
			}
			dest = null;
		}
	}
}

