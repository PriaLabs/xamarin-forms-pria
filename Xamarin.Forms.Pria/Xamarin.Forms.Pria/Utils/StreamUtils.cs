﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria
{
	public static class StreamUtils
	{
		public static byte[] GetDataFromStream(Stream stream)
		{
			byte[] data = new byte[stream.Length];
			stream.Read(data, 0, data.Length);
			return data;
		}

		public static string GetBase64DataFromStream(Stream stream)
		{
			return Convert.ToBase64String (GetDataFromStream (stream));
		}

		public static void CopyStream(Stream input, Stream output)
		{
			byte[] buffer = new byte[32768];
			int read;
			while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write (buffer, 0, read);
			}
			input.Seek (0, SeekOrigin.Begin);
			output.Seek (0, SeekOrigin.Begin);
		}
	}
}

