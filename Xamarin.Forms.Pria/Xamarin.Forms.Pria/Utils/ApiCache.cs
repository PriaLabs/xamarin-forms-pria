﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms.Pria.Utils;
using System.Xml.Serialization;

namespace Xamarin.Forms.Pria
{
	public class ApiCache
	{
		public class CacheItem<T> {
			public T CachedData{get;set;}
			public DateTime CacheTimeStamp{get;set;}

			[XmlIgnore]
			public Exception Exception{ get; set; }
		}

		public ApiCache ()
		{
		}

		// TODO: extend by CancellationToken
		public async Task<CacheItem<T>> GetCachedItem<T>(string cacheKey)
		{
			return await Task.Run (() => {
				string isoKey = typeof(ApiCache).FullName + cacheKey;
				IsolatedStoragePersistor<CacheItem<T>> isp = new IsolatedStoragePersistor<CacheItem<T>> ();
				return isp.Load (isoKey);
			});
		}

		public async Task<CacheItem<T>> CallWithCache<T>(Func<Task<T>> action,string cacheKey)
		{
			string isoKey = typeof(ApiCache).FullName + cacheKey;
			IsolatedStoragePersistor<CacheItem<T>> isp = new IsolatedStoragePersistor<CacheItem<T>> ();
			CacheItem<T> item = null;

			item = isp.Load (isoKey) ?? new CacheItem<T>();

			try{
				T data = await action.Invoke();
				item.CachedData = data;
				item.CacheTimeStamp = DateTimeUtils.Now;
			}
			catch(Exception ex){
				item.Exception = ex;
			}

			if (item.Exception == null) {
				isp.Save (isoKey, item);
			}

			return item;
		}
	}
}

