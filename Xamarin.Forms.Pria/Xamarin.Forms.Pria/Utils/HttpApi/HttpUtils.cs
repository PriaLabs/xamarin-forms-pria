﻿using System;
using System.Net;

namespace Xamarin.Forms.Pria
{
	public static class HttpUtils
	{
		// 2xx
		public static bool IsOK(HttpStatusCode statusCode)
		{
			return statusCode == HttpStatusCode.OK
				|| statusCode == HttpStatusCode.Created
				|| statusCode == HttpStatusCode.NoContent
				|| statusCode == HttpStatusCode.NonAuthoritativeInformation
				|| statusCode == HttpStatusCode.Accepted
				|| statusCode == HttpStatusCode.PartialContent
				|| statusCode == HttpStatusCode.ResetContent;
		}

		// 3xx
		public static bool IsRedirect(HttpStatusCode statusCode)
		{
			return statusCode == HttpStatusCode.Found
				|| statusCode == HttpStatusCode.Moved
				|| statusCode == HttpStatusCode.MovedPermanently
				|| statusCode == HttpStatusCode.MultipleChoices
				|| statusCode == HttpStatusCode.SeeOther
				|| statusCode == HttpStatusCode.UseProxy
				|| statusCode == HttpStatusCode.TemporaryRedirect
				|| statusCode == HttpStatusCode.Redirect
				|| statusCode == HttpStatusCode.Ambiguous;
		}
	}
}
