﻿using System;

namespace Xamarin.Forms.Pria
{
	public enum HttpMethod
	{
		GET,
		POST,
		PUT,
		DELETE
	}
}

