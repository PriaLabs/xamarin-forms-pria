using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

namespace Xamarin.Forms.Pria.Utils
{
	public class StreamWrapper
	{
		public HttpStatusCode StatusCode{ get; set; }
		public Stream Stream{ get; set; }
	}

	/// <summary>
	/// This version returns status code from http response
	/// </summary>
	public class HttpApiNew
	{
		public const string CONTENT_TYPE_JSON = "application/json";
		public const string CONTENT_TYPE_XML = "application/xml";

		public async Task<StreamWrapper> Call(string baseUrl,HttpResultFormat format= HttpResultFormat.Custom, HttpMethod method = HttpMethod.GET, string contentType = null, Dictionary<string, string> httpParameters = null, string requestBody = null, string userAgent = null, Dictionary<string,string> headerParameters = null, Stream destinationStream = null, Action<int> downloadProgress = null)
		{
			StringBuilder sb = new StringBuilder();
			if (httpParameters!=null && httpParameters.Keys.Count > 0)
			{
				sb.AppendFormat("{0}?", baseUrl);
				foreach (string key in httpParameters.Keys) {
					sb.AppendFormat ("{0}={1}&", WebUtility.UrlEncode (key), WebUtility.UrlEncode (httpParameters [key]));
					//sb.AppendFormat ("{0}={1}&", key, httpParameters [key]);
				}
			}
			else
			{
				sb.Append(baseUrl);
			}

			string url = sb.ToString();

			HttpWebRequest request = HttpWebRequest.CreateHttp(url);

			request.Headers = new WebHeaderCollection();
			if (headerParameters != null && headerParameters.Keys.Count > 0) {
				foreach (string key in headerParameters.Keys) {
					request.Headers [key] = headerParameters [key];
				}
			}

			request.Method = GetMethodString (method);

			if (!string.IsNullOrEmpty(userAgent))
			{
				SetUserAgent (request, userAgent);
			}

			if (!string.IsNullOrEmpty (contentType)) {
				request.ContentType = contentType;
			}

			switch (format)
			{
			case HttpResultFormat.Json:
				request.Accept = CONTENT_TYPE_JSON;
				break;
			case HttpResultFormat.XML:
				request.Accept = CONTENT_TYPE_XML;
				break;
			default:
				request.Accept = "*/*";
				break;
			}

			if (!string.IsNullOrEmpty (requestBody)) {
				using (var requestStream = (Stream)(await Task<Stream>.Factory.FromAsync (request.BeginGetRequestStream, request.EndGetRequestStream, null).ConfigureAwait (false))) {
					using (StreamWriter sw = new StreamWriter (requestStream)) {
						sw.Write (requestBody);
					}
				}
			}

			using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null).ConfigureAwait(false)))
			{
				// HANDLE REDIRECT
				if (HttpUtils.IsRedirect(response.StatusCode) && !string.IsNullOrEmpty (response.Headers ["Location"])) {
					return await Call (response.Headers ["Location"], format, method, contentType, httpParameters, requestBody, userAgent, headerParameters, destinationStream, downloadProgress);
				}

				using (var responseStream = response.GetResponseStream())
				{
					int progressPercent = -1;

					Stream ms = destinationStream ?? new MemoryStream();
					var buffer = new byte[1024 * 128]; //128kB
					int bytesReceived;
					long totalBytesReceived = 0;
					long totalBytes = response.ContentLength;

					while ((bytesReceived = responseStream.Read(buffer, 0, buffer.Length)) != 0)
					{
						totalBytesReceived += bytesReceived;
						ms.Write(buffer, 0, bytesReceived);

						if ((totalBytesReceived * 100) / totalBytes != progressPercent) {
							progressPercent = (int) ((totalBytesReceived * 100) / totalBytes);
							if (downloadProgress != null) {
								downloadProgress.Invoke (progressPercent);
							}
						}
					}
					if (totalBytes >= 0 && totalBytes != totalBytesReceived) {
						throw new WebException ("Downloading interrupted.");
					}
					ms.Seek(0, SeekOrigin.Begin);

					return new StreamWrapper (){ Stream = ms, StatusCode = response.StatusCode };
				}
			}
		}

		public async Task<HttpApiResponse<T>> Call<T>(string baseUrl,HttpResultFormat format,HttpMethod method = HttpMethod.GET, string contentType = null, Dictionary<string,string> httpParameters = null,string requestBody = null, Dictionary<string,string> headerParameters = null, Func<Stream,T> customDeserializer=null, Stream destinationStream = null,Action<int> downloadProgress = null)
		{
			StreamWrapper sw = await Call (baseUrl, format, method, contentType, httpParameters, requestBody, null, headerParameters, destinationStream, downloadProgress);

			switch (format) {
			case HttpResultFormat.XML:
				sw.Stream.Seek (0, SeekOrigin.Begin);
				return new HttpApiResponse<T> (sw.StatusCode, (T)new XmlSerializer (typeof(T)).Deserialize (sw.Stream));
			case HttpResultFormat.Json:
				string datajson = new StreamReader (sw.Stream).ReadToEnd ();
				return new HttpApiResponse<T> (sw.StatusCode, Newtonsoft.Json.JsonConvert.DeserializeObject<T> (datajson));
			case HttpResultFormat.Custom:
				return new HttpApiResponse<T> (sw.StatusCode, customDeserializer.Invoke (sw.Stream));
			}

			return default(HttpApiResponse<T>);
		}

		void SetUserAgent(HttpWebRequest request, string userAgent)
		{
			Type type = request.GetType();

			PropertyInfo prop = type.GetRuntimeProperty("UserAgent");

			if (prop != null)
			{
				prop.SetValue(request, userAgent, null);
			}
		}

		string GetMethodString(HttpMethod method)
		{
			switch(method){
			case HttpMethod.POST: return "POST";
			case HttpMethod.DELETE: return "DELETE";
			case HttpMethod.PUT: return "PUT";
			default:
			case HttpMethod.GET: return "GET";
			}
		}
	}
}

