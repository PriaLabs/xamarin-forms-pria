﻿using System;
using System.Net;

namespace Xamarin.Forms.Pria
{
	public class HttpApiResponse<T>
	{
		public HttpApiResponse (HttpStatusCode statusCode, T data)
		{
			StatusCode = statusCode;
			Data = data;
		}

		public HttpStatusCode StatusCode{ get; private set; }
		public T Data{ get; private set; }

		public bool IsOK{
			get{
				return HttpUtils.IsOK (StatusCode);
			}
		}
	}
}

