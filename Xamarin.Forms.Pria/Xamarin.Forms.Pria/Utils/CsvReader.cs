
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Xamarin.Forms.Pria.Utils
{
	public class CsvReader<T>
		where T : new ()
	{
		IFormatProvider formatProvider;
		TextReader reader;

		string[] headerNames;

		public CsvReader (TextReader reader)
		{
			this.formatProvider = CultureInfo.InvariantCulture;
			this.reader = reader;
		}

		public IEnumerable<T> ReadAll ()
		{
			//
			// Associate header names with properties
			//
			headerNames = reader.ReadLine ().Split (',');

			var props = new PropertyInfo[headerNames.Length];
			for (var hi = 0; hi < props.Length; hi++) {
				Type t = typeof(T);

				while (t != null) {
					TypeInfo ti = t.GetTypeInfo (); 
					var p = ti.GetDeclaredProperty (headerNames [hi]);
					if (p == null && ti.BaseType == null)
						throw new Exception (
							"Property '" + headerNames [hi] + "' not found in " + typeof(T).Name);

					if (p != null) {
						props [hi] = p;
						break;
					}

					t = ti.BaseType;
				}
			}


			//
			// Read all the records
			//
			var r = new T ();
			var i = 0;

			var ch = reader.Read ();
			while (ch > 0) {
				if (ch == '\n') {
					yield return r;
					r = new T ();
					i = 0;
					ch = reader.Read ();
				}
				else if (ch == '\r') {
					ch = reader.Read ();
				}
				else if (ch == '"') {
					ch = ReadQuoted (r, props[i]);
				}
				else if (ch == ',') {
					i++;
					ch = reader.Read ();
				}
				else {
					ch = ReadNonQuoted (r, props[i], (char)ch);
				}
			}
		}

		int ReadNonQuoted (T r, PropertyInfo prop, char first)
		{
			var sb = new StringBuilder ();

			sb.Append (first);

			var ch = reader.Read ();

			while (ch >= 0 && ch != ',' && ch != '\r' && ch != '\n') {
				sb.Append ((char)ch);
				ch = reader.Read ();
			}

			var valueString = sb.ToString ().Trim ();

			if (typeof(System.Collections.IList).GetTypeInfo().IsAssignableFrom (prop.PropertyType.GetTypeInfo())) {
				var coll = (System.Collections.IList)prop.GetValue (r, null);
				coll.Add (valueString);
			} else {
				prop.SetValue (
					r,
					Convert.ChangeType (
						valueString, 
						prop.PropertyType, 
						formatProvider),
					null);
			}

			return ch;
		}

		int ReadQuoted (T r, PropertyInfo prop)
		{
			var sb = new StringBuilder ();

			var ch = reader.Read ();

			var hasQuote = false;

			while (ch >= 0) {

				if (ch == '"') {
					if (hasQuote) {
						sb.Append ('"');
						hasQuote = false;
					}
					else {
						hasQuote = true;
					}
				}
				else {
					if (hasQuote) {
						prop.SetValue (r, Convert.ChangeType (sb.ToString ().Trim (), prop.PropertyType, formatProvider), null);
						return ch;
					}
					else {
						sb.Append ((char)ch);
					}
				}

				ch = reader.Read ();
			}

			prop.SetValue (r, Convert.ChangeType (sb.ToString ().Trim (), prop.PropertyType, formatProvider), null);
			return ch;
		}
	}
}

