﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Xamarin.Forms.Pria
{
	public static class TextUtils
	{
		private const String diacritic =   "áäčďéěíĺľňóôőöŕšťúůűüýřžàâçéèêëîïôùûü";
		private const String diacRemoved = "aacdeeillnoooorstuuuuyrzaaceeeeiiouuu";

		public static string RemoveDiacritics (string keyword)
		{
			StringBuilder sb = new StringBuilder (keyword);
			for (int i = 0; i < diacritic.Length; i++) {
				sb.Replace (diacritic [i], diacRemoved [i]);
				sb.Replace (diacritic [i].ToString ().ToUpper (), diacRemoved [i].ToString ().ToUpper ());
			}
			sb.Replace ("ß", "ss");
			return sb.ToString ();
		}

		public static bool IsValidPhoneNumber(string number)
		{
			return Regex.IsMatch(number, @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$", RegexOptions.IgnoreCase);
		}

		public static bool IsValidEmailAddress(string address)
		{
			return Regex.IsMatch(address, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
		}

		//TODO rozsirit o parametr pocet znaku nebo odstavcu
		public static string LoremIpsumShort{
			get{
				return "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam egestas wisi a erat. Fusce suscipit libero eget elit. Integer pellentesque quam vel velit. Donec iaculis gravida nulla. Fusce wisi. Suspendisse sagittis ultrices augue";
			}
		}

		public static string LoremIpsumLong{
			get{
				return "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam egestas wisi a erat. Fusce suscipit libero eget elit. Integer pellentesque quam vel velit. Donec iaculis gravida nulla. Fusce wisi. Suspendisse sagittis ultrices augue. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam posuere lacus quis dolor. Aliquam erat volutpat. Etiam bibendum elit eget erat. In convallis. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Quisque porta. Integer malesuada. Aliquam erat volutpat. Fusce wisi. Aliquam erat volutpat. Nulla pulvinar eleifend sem. Maecenas sollicitudin."; 
			}
		}
	}
}

