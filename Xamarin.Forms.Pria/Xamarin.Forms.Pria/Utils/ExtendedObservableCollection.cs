﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Xamarin.Forms.Pria
{
	public class ExtendedObservableCollection<T>:ObservableCollection<T>
	{
		public ExtendedObservableCollection ()
		{
		}

		public ExtendedObservableCollection (IEnumerable<T> collection) : base (collection)
		{
		}

		public void AddRange(IEnumerable<T> collection)
		{
			if (collection == null) throw new ArgumentNullException("collection");

			foreach (var i in collection) Items.Add(i);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, collection.ToList()));
		}

		public void ReplaceRange(IEnumerable<T> collection)
		{
			if (collection == null) throw new ArgumentNullException("collection");

			Clear ();

			foreach (var i in collection) Items.Add(i);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, collection.ToList()));
		}

		public void RemoveRange(IEnumerable<T> collection)
		{
			if (collection == null) throw new ArgumentNullException("collection");

			foreach (var i in collection) Items.Remove(i);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, collection.ToList()));
		}

		public void RemoveAll(Func<T,bool> condition)
		{
			var itemsToRemove = Items.Where(condition).ToList();

			RemoveRange (itemsToRemove);
		}
	}
}

