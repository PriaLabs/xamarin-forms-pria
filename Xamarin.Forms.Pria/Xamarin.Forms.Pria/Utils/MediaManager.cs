﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Xamarin.Forms.Pria
{
	public class MediaManager
	{
		IMediaPicker mediaPicker;

		private readonly TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext ();

		public string Status { get; private set; }
		public string VideoInfo{ get; set; }

		public MediaManager ()
		{
			mediaPicker = DependencyService.Get<IMediaPicker> ();
		}

		public async Task<ImageSource> SelectPicture ()
		{
			ImageSource imageSource = null;
			try {
				var mediaFile = await mediaPicker.SelectPhotoAsync (new CameraMediaStorageOptions {
					DefaultCamera = CameraDevice.Rear,
					MaxPixelDimension = 400
				});
				imageSource = ImageSource.FromStream (() => mediaFile.Source);
			} catch (System.Exception ex) {
				Debug.WriteLine (ex.ToString ());
			}
			return imageSource;
		}

		public async Task<ImageSource> TakePicture ()
		{
			ImageSource imageSource = null;

			await this.mediaPicker.TakePhotoAsync (new CameraMediaStorageOptions {
				DefaultCamera = CameraDevice.Rear,
				MaxPixelDimension = 400
			}).ContinueWith (t => {
				if (t.IsFaulted) {
					Status = t.Exception.InnerException.ToString ();
				} else if (t.IsCanceled) {
					Status = "Canceled";
				} else {
					var mediaFile = t.Result;

					imageSource = ImageSource.FromStream (() => mediaFile.Source);

					return imageSource;
				}

				return null;

			}, scheduler);

			return imageSource;
		}

		public async Task<MediaFile> SelectVideo()
		{
			VideoInfo = "Selecting video";

			MediaFile mediaFile = null;

			try
			{
				mediaFile = await this.mediaPicker.SelectVideoAsync(new VideoMediaStorageOptions());

				if (mediaFile != null)
				{
					VideoInfo = string.Format("Your video size {0} MB", ConvertBytesToMegabytes(mediaFile.Source.Length));
				}
				else 
				{
					VideoInfo = "No video was selected";
				}
			} 
			catch (System.Exception ex) 
			{
				if (ex is TaskCanceledException)
				{
					VideoInfo = "Selecting video canceled";
				}
				else
				{
					VideoInfo = ex.Message;
				}
			}

			return mediaFile;
		}

		private double ConvertBytesToMegabytes(long bytes)
		{
			return (bytes / 1024f) / 1024f;
		}
	}
}

