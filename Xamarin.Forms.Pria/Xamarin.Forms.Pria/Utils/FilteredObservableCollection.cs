﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	public class FilteredObservableCollection<T>:ExtendedObservableCollection<T> where T:BindableObject
	{
		public ObservableCollection<T> Source{get;private set;}

		Predicate<T> filter;
		public Predicate<T> Filter {
			get{ return filter; }
			set {
				filter = value;
				Reset ();
			}
		}

		public FilteredObservableCollection (ObservableCollection<T> source, Predicate<T> filter)
		{
			this.Source = source;
			this.Filter = filter;

			this.Source.CollectionChanged += OnSourceCollectionChanged;
		}
		private void OnSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			// Check the NewItems
			List<T> newlist = new List<T> ();
			if (e.NewItems != null)
				foreach (T item in e.NewItems)
					if (Filter == null || Filter (item))
						newlist.Add (item);

			// Check the OldItems
			List<T> oldlist = new List<T> ();
			if (e.OldItems != null)
				foreach (T item in e.OldItems)
					if (Filter == null || Filter (item))
						oldlist.Add (item);

			// Create the Add/Remove/Replace lists
			List<T> addlist = new List<T> ();
			List<T> removelist = new List<T> ();
			List<T> replacelist = new List<T> ();

			// Fill the Add/Remove/Replace lists
			foreach (T item in newlist)
				if (oldlist.Contains (item))
					replacelist.Add (item);
				else
					addlist.Add (item);
			foreach (T item in oldlist)
				if (newlist.Contains (item))
					continue;
				else
					removelist.Add (item);

			// Send the corrected event
			switch (e.Action) {
			case NotifyCollectionChangedAction.Add:
			case NotifyCollectionChangedAction.Move:
			case NotifyCollectionChangedAction.Remove:
			case NotifyCollectionChangedAction.Replace:
				if (addlist.Count > 0) {
					addlist.ForEach (item => item.PropertyChanged += ItemPropertyChanged);
					AddRange (addlist);
				}

				if (replacelist.Count > 0) {
					OnCollectionChanged (new NotifyCollectionChangedEventArgs (NotifyCollectionChangedAction.Replace, replacelist));
				}
				if (removelist.Count > 0) {
					removelist.ForEach (item => item.PropertyChanged -= ItemPropertyChanged);
					RemoveRange (removelist);
				}
				break;
			case NotifyCollectionChangedAction.Reset:
				Reset ();
				break;
			}
		}

		public void Reset()
		{
			foreach (T item in Source) {
				item.PropertyChanged -= ItemPropertyChanged;
			}

			Items.Clear ();

			foreach (T item in Source)
			{
				if(Filter == null || Filter(item)){
					Items.Add (item);
				}

				item.PropertyChanged += ItemPropertyChanged;
			}

			OnCollectionChanged (new NotifyCollectionChangedEventArgs (NotifyCollectionChangedAction.Reset));
		}

		void ItemPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			T item = (T)sender;

			if (Filter == null || Filter (item)) {
				if (!Contains (item)) {
					Insert (SourceIndexToFiltered (Source.IndexOf (item)), item);
				}
			} else {
				if (Contains (item)) {
					Remove (item);
				}
			}
		}

		int SourceIndexToFiltered(int sourceIndex){
			if (Count==0) {
				return 0;
			}
			int filteredIndex = 0;
			for (int i = 0; i < sourceIndex && filteredIndex < Count; i++) {
				if (this [filteredIndex] == Source [i]) {
					filteredIndex++;
				}

			}
			return filteredIndex;
		}
	}
}

