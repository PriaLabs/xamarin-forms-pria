﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria.Utils
{
    public static class HtmlUtils
    {
        public static string RemoveHtmlTags(string html)
        {
            string result = html;
            result = Regex.Replace(result, "</p>", "\n");
            result = Regex.Replace(result, "</h[0-9]>", "\n");
            result = Regex.Replace(result, "<br>*", "\n");
            result = Regex.Replace(result, "<br/>*", "\n");

            result = Regex.Replace(result, "<(.|\\n)*?>", "\n");
            return result;
        }
    }
}
