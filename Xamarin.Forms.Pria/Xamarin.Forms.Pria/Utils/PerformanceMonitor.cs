﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace Xamarin.Forms.Pria
{
	public class PerformanceMonitor:IDisposable
	{
		class PerformanceCounterItem{
			public PerformanceCounterItem Parent { get; set;}
			public List<PerformanceCounterItem> Items { get; set;}
			public string Tag {get;set;}

			DateTime start = DateTime.Now,end;

			public PerformanceCounterItem (string tag)
			{
				this.Tag = tag;
				Items = new List<PerformanceCounterItem>();
			}
			
			public void Stop(){
				end = DateTime.Now;
			}

			public void Print(int level, StringBuilder sb){
				sb.AppendFormat("{0}{2} :\t{1}",string.Empty.PadLeft(level*2,' '),(end-start).TotalMilliseconds.ToString("F1"),Tag);
				sb.AppendLine ();
                foreach (PerformanceCounterItem i in Items)
                {
                    i.Print(level + 1, sb);
                }
			}
		}

		#if DEBUG
		static PerformanceCounterItem rootItem;

		PerformanceCounterItem item;
		#endif

		public PerformanceMonitor (string tag)
		{
			#if DEBUG
			item = new PerformanceCounterItem (tag);
			item.Parent = rootItem;
			if (rootItem != null) {
				rootItem.Items.Add (item);
			}
			rootItem = item;
			#endif
		}

		#region IDisposable implementation

		public void Dispose ()
		{
			#if DEBUG
			item.Stop ();
			rootItem = item.Parent;
			if (rootItem == null) {
				StringBuilder sb = new StringBuilder ();
				sb.AppendLine ("Results:");
				item.Print (0, sb);
				Debug.WriteLine (sb.ToString ());
			}
			#endif
		}

		#endregion
	}
}

