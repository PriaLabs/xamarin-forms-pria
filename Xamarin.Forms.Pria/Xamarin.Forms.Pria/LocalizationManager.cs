﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.IO;
using Xamarin.Forms.Pria.Utils;
using System.Linq;

namespace Xamarin.Forms.Pria
{
	public class LocalizationManager
	{
		Dictionary<string,LocalizedText> localization = new Dictionary<string, LocalizedText>();
		const string defaultLanguageFileSuffix = "localization.csv";
		string currentLanguage;

		static LocalizationManager instance;
		public static LocalizationManager Instance{
			get {

				return instance ?? (instance = new LocalizationManager()); 
			}
		}

		public void LoadLocalizations(List<Assembly> assemblies, string language = null){
			if (language == null) {
				language = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
			}

			localization.Clear ();
			currentLanguage = language;

			foreach (Assembly a in assemblies) {
				LoadAssemblyLocalization (a, language);
			}
		}
		void LoadAssemblyLocalization (Assembly a,string twoLetterISOLanguageName)
		{
			String localizationFileSuffix = String.Format ("localization_{0}.csv", twoLetterISOLanguageName.ToLower ());

			LoadLanguageStreams(FindResourceStreams(a,defaultLanguageFileSuffix));
			LoadLanguageStreams(FindResourceStreams(a,localizationFileSuffix));
		}

		void LoadLanguageStreams (List<Stream> list)
		{
			foreach (Stream s in list) {
				using (TextReader tr = new StreamReader (s, System.Text.Encoding.UTF8)) {
					UpdateLocalizations (new CsvReader<LocalizedText> (tr).ReadAll ().ToList ());
				}
			}
		}

		public void UpdateLocalizations(List<LocalizedText> texts){
			foreach (LocalizedText t in texts) {
				if(localization.ContainsKey(t.Key)){
					localization[t.Key] = t;
				}
				else{
					localization.Add(t.Key,t);
				}
			}
		}

		private List<Stream> FindResourceStreams(Assembly assembly,string dataResourceName){
			List<Stream> result = new List<Stream> ();
			foreach (string s in assembly.GetManifestResourceNames()) {
				if (s.EndsWith (dataResourceName)) {
					result.Add (assembly.GetManifestResourceStream (s));
				}
			}
			return result;
		}


		public string GetText(string key)
		{
			if (localization.ContainsKey(key)) {
				return localization[key].Value;
			}
			return string.Format ("?{0}?{1}",key,currentLanguage); 
		}

		public string GetText(string key, params object[] values)
		{
			return string.Format (GetText (key), values);
		}
	}
}

