﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms.Pria.Utils;
using System.Globalization;

namespace Xamarin.Forms.Pria
{
	public class GooglePlacesApi
	{
		private const string BASE_URL_AUTOCOMPLETE = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
		private const string BASE_URL_DETAIL = "https://maps.googleapis.com/maps/api/place/details/json";
		private const string BASE_URL_GEOCODE_SEARCH = "https://maps.googleapis.com/maps/api/geocode/json";

		private string apiKey;

		public GooglePlacesApi (string apiKey)
		{
			this.apiKey = apiKey;
		}

		public async Task<GPApiAutocompleteResponse> Autocomplete(string searchText, string twoLetterLanguage, string country = null)
		{
			string url = string.Format ("{0}?input={1}&language={2}&key={3}", BASE_URL_AUTOCOMPLETE, searchText, twoLetterLanguage, apiKey);
			if (!string.IsNullOrEmpty (country)) {
				url = string.Format ("{0}&components=country:{1}", url, country);
			}

			return await new HttpApi ().Call<GPApiAutocompleteResponse> (url, HttpApi.ResultFormat.Json);
		}

		public async Task<GPApiPlaceDetailApiResponse> GetPlaceDetail(string placeId)
		{
			string url = string.Format ("{0}?placeid={1}&key={2}", BASE_URL_DETAIL, placeId, apiKey);

			return await new HttpApi ().Call<GPApiPlaceDetailApiResponse> (url, HttpApi.ResultFormat.Json);
		}

		public async Task<GPApiGeocodeApiResponse> SearchForPlace(double lat, double lng, string twoLetterLanguage)
		{
			//https://developers.google.com/maps/documentation/geocoding

			string url = string.Format ("{0}?latlng={1},{2}&result_type=locality&language={3}&key={4}", 
				BASE_URL_GEOCODE_SEARCH, 
				lat.ToString(CultureInfo.InvariantCulture), 
				lng.ToString(CultureInfo.InvariantCulture), 
				twoLetterLanguage, 
				apiKey);

			return await new HttpApi ().Call<GPApiGeocodeApiResponse> (url, HttpApi.ResultFormat.Json);
		}
	}
}

