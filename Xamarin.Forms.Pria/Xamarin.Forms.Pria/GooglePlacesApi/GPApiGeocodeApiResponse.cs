﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Xamarin.Forms.Pria
{
	public class GPApiGeocodeApiResponse
	{
		public string Status{ get; set;}

		public List<GPGeocodePlace> Results{ get; set;}
	}

	public class  GPAddressComponent
	{
		[JsonProperty("long_name")]
		public string LongName{get;set;}

		[JsonProperty("short_name")]
		public string ShortName{get;set;}

		public List<string> Types{get;set;}
	}

	public class GPGeocodePlace
	{
		[JsonProperty("formatted_address")]
		public string AddressFormatted{ get; set; }

		[JsonProperty("address_components")]
		public List<GPAddressComponent> AddressComponents {get; set;}

		//TODO: deserialize more properties when needed
		//example request: https://maps.googleapis.com/maps/api/geocode/json?latlng=49.2075653076172,17.6413440704346&result_type=locality&language=cs&key=AIzaSyAZTOuwZ1IePl43O2pQkF-uQjRcM5zOZVM
	}
}


