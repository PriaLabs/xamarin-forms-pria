﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xamarin.Forms.Pria
{
	/// <summary>
	/// Doc: https://developers.google.com/places/webservice/details
	/// </summary>

	public class GPApiPlaceDetailApiResponse
	{
		public string Status{ get; set;}

		public GPResult Result{ get; set;}
	}

	public class GPResult
	{
		public string Id{ get; set; }

		public string Vicinity{ get; set; } // okolí

		[JsonProperty("formatted_address")]
		public string AddressFormatted{ get; set; }

		public string Url{ get; set; }

		public List<string> Types{ get; set; }

		public string Reference{ get; set; }

		[JsonProperty("place_id")]
		public string PlaceId{ get; set; }

		public string Scope{ get; set; }

		[JsonProperty("icon")]
		public string IconUrl{ get; set; }

		public string Name{ get; set; }

		public GPGeometry Geometry{get;set;}
	}

	public class GPGeometry
	{
		public GPLocation Location{get;set;}

		public GPViewPort ViewPort{get;set;}
	}

	public class GPLocation
	{
		[JsonProperty("lat")]
		public double Latitude{get;set;}

		[JsonProperty("lng")]
		public double Longitude{get;set;}
	}

	public class GPViewPort
	{
		public GPLocation SouthWest{get;set;}
		public GPLocation NorthEast{get;set;}
	}
}

