﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Xamarin.Forms.Pria
{
	/// <summary>
	/// Doc: https://developers.google.com/places/webservice/autocomplete
	/// </summary>

	public class GPApiAutocompleteResponse
	{
		public string Status{ get; set;}

		public List<GPPrediction> Predictions{ get; set;}
	}

	public class GPPrediction
	{
		public string Reference{ get; set; }

		public string Id{ get; set; }

		public List<string> Types{ get; set; }

		[JsonProperty("matched_substrings")]
		public List<GPMatchedSubstring> MatchedSubstrings{ get; set; }

		[JsonProperty("place_id")]
		public string PlaceId{ get; set; }

		public string Description{ get; set; }

		public List<GPTerm> Terms{ get; set; }
	}

	public class GPMatchedSubstring
	{
		public int Length{ get; set; }
		public int Offset{ get; set; }
	}

	public class GPTerm
	{
		public int Offset{ get; set; }
		public string Value{ get; set; }
	}
}

