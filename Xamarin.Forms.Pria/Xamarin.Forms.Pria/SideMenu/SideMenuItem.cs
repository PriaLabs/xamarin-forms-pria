﻿using System;
using Xamarin.Forms.Pria.ViewModels;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria
{
	public class SideMenuItem : ExtendedBindableObject
	{
		public virtual bool IsVisible{
			get{
				return true;
			}
		}

		private string mCaption;

		public string Caption {
			get { return mCaption; }
			set { 
				if (value != mCaption) {
					mCaption = value;
					OnPropertyChanged ();
				}
			}
		}

		private bool mIsSelected;

		public bool IsSelected {
			get { return mIsSelected; }
			set { 
				if (value != mIsSelected) {
					mIsSelected = value;
					OnPropertyChanged ();
				}
			}
		}

		private string mIcon;

		public string Icon {
			get { return mIcon; }
			set { 
				if (value != mIcon) {
					mIcon = value;
					OnPropertyChanged ();
				}
			}
		}

		bool mIsBusy;

		public bool IsBusy {
			get { return mIsBusy; }
			set { 
				if (value != mIsBusy) {
					mIsBusy = value;
					OnPropertyChanged ();
				}
			}
		}
			
		private Func<ViewModelBase> mViewModel;

		public Func<ViewModelBase> ViewModel {
			get { return mViewModel; }
			set { 
				if (value != mViewModel) {
					mViewModel = value;
					OnPropertyChanged ();
				}
			}
		}

		Action mAction;

		public Action Action {
			get { return mAction; }
			set { 
				if (value != mAction) {
					mAction = value;
					OnPropertyChanged ();
				}
			}
		}
	}
}
