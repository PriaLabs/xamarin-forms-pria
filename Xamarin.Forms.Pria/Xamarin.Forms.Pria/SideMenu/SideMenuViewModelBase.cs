﻿using System;
using System.Linq;
using Xamarin.Forms.Pria.ViewModels;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Xamarin.Forms.Pria
{
	public abstract class SideMenuViewModelBase : ViewModelBase
	{
		protected SideMenuViewModelBase ()
		{
			SideMenuItems = new ObservableCollection<SideMenuItem> ();
		}

		public abstract void CreateSideMenuItems ();

		public INavigation RootNavigation { get; set; }

		public virtual SideMenuItem InitialSideMenuItem{
			get{
				return SideMenuItems[0];
			}
		}

		private ObservableCollection<SideMenuItem> mSideMenuItems;

		public ObservableCollection<SideMenuItem> SideMenuItems {
			get { return mSideMenuItems; }
			set { 
				if (value != mSideMenuItems) {
					mSideMenuItems = value;
					OnPropertyChanged ();
				}
			}
		}

		public Command SideMenuItemTapped {
			get {
				return new Command (async (item) => await ShowMenuItem ((SideMenuItem)item));
			}
		}

		public async Task ShowMenuItem(SideMenuItem item)
		{
			foreach (SideMenuItem i in SideMenuItems) {
				i.IsSelected = false;
			}
			item.IsSelected = true;

			if (item.Action != null) {
				item.Action.Invoke ();
			} else {
				await item.ViewModel ().Show (RootNavigation, true);
			}
		}

	}
}

