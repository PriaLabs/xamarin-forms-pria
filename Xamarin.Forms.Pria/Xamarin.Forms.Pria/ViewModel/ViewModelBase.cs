using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Pria.Pages;
using System.Diagnostics;
using System.Threading;
using System.Net;
using Ninject;
using System.IO;
using Newtonsoft.Json;

namespace Xamarin.Forms.Pria.ViewModels
{
	public abstract class ViewModelBase : ExtendedBindableObject
	{
		protected class LoadDataResult
		{
			public Exception Exception{ get; set; }

			public DateTime TimeStamp{ get; set; }

			public LoadDataResult ()
			{
				TimeStamp = DateTimeUtils.Now;
			}
		}

		// WORKAROUND for Java.Lang.IllegalStateException: Can not perform this action after onSaveInstanceState
		public bool IsForeground
		{
			get {
				return BaseApp.CurrentApp.IsForeground;
			}
		}

		private bool mIsShown;

		/// <summary>
		/// True between OnAppearing and OnDisappearing call
		/// </summary>
		/// <value><c>true</c> if is shown; otherwise, <c>false</c>.</value>
		public bool IsShown
		{
			get { return mIsShown; }
			set
			{
				if (value != mIsShown)
				{
					mIsShown = value;
					OnPropertyChanged();
				}
			}
		}


		[Inject]
		public ILocalization Localization { get; set; }

		protected INavigation Navigation{ get; private set; }

		private bool mIsDataLoaded;

		public bool IsDataLoaded {
			get { return mIsDataLoaded; }
			protected set { 
				if (value != mIsDataLoaded) {
					mIsDataLoaded = value;
					OnPropertyChanged ();
				}
			}
		}

		Page page;
		public Page Page{ 
			get { 
				if (page == null) {
					page = CreatePage ();
					page.BindingContext = this;
					page.Appearing += (object sender, EventArgs e) => OnAppearing();
					page.Disappearing += (object sender, EventArgs e) => OnDisappearing();
				}
				return page;
			} 
		}

		private DateTime mLastUpdate;

		public DateTime LastUpdate {
			get { return mLastUpdate; }
			set { 
				if (value != mLastUpdate) {
					mLastUpdate = value;
					OnPropertyChanged ();
				}
			}
		}

		protected abstract Page CreatePage ();

		public async Task Show(INavigation navigation, bool modal, bool shouldSetBusy = true, bool animated = true){
			Navigation = navigation;
			PageIsBusy = shouldSetBusy;

			if (page != null) {//if page was previously shown, invalidate it and let it create new one.
				page = null;
			}

			await BaseApp.CurrentApp.PostForegroundAction(async () =>
			{
				if (modal)
				{
					await navigation.PushModalAsync(Page, animated);
				}
				else {
					await navigation.PushAsync(Page, animated);
				}

				if (!IsDataLoaded)
				{
					await LoadData();
				}
			});

			PageIsBusy = false;
		}

		public async Task Close (INavigation navigation, bool modal, bool animated = true){
			await BaseApp.CurrentApp.PostForegroundAction(async () =>
			{
				if (modal)
				{
					await navigation.PopModalAsync(animated);
				}
				else {
					await navigation.PopAsync(animated);
				}
			});
		}

		public ViewModelBase ()
		{
		}
			
		public virtual async Task RefreshData()
		{
			IsDataLoaded = false;
			PageIsBusy = true;

			await LoadData ();

			PageIsBusy = false;
		}

		protected virtual void OnAppearing()
		{
			IsShown = true;
		}

		protected virtual void OnDisappearing()
		{
			IsShown = false;
		}

		public async Task LoadData ()
		{     
			bool retry;
            
			do {
				Exception exception = null;
				retry = true;

				try {
					LoadDataResult status = await DoLoadData ();

					LastUpdate = status.TimeStamp;

					IsDataLoaded = true;
                    retry = false;
				} catch (WebException webException) {
					//var resp = new StreamReader(webException.Response.GetResponseStream()).ReadToEnd();
					Log.LogException(webException);
					exception = webException;
				}
				catch(Exception e){
					exception = e;
				}


				if(exception != null){
					retry = await LoadDataError(exception);
					if(retry){
						await new TaskFactory().StartNew(()=>{
							new ManualResetEvent(false).WaitOne(2000);
						});
					}
                }

			} while(retry);
		}

		protected virtual Task<bool> LoadDataError(Exception exception)
        {
			return Task.FromResult(false);
        }

        private bool mPageIsBusy;
        public virtual bool PageIsBusy
        {
            get { return mPageIsBusy; }
            set
            {
                if (value != mPageIsBusy)
                {
                    mPageIsBusy = value;
                    OnPropertyChanged();
                }
            }
        }

		public bool LoadingViewVisible{
			get{
				return ObserveProperty (this, p => p.PageIsBusy) && !ObserveProperty (this, p => p.IsDataLoaded);
			}
		}

		public bool RetryViewVisible{
			get{
				return !ObserveProperty (this, p => p.PageIsBusy) && !ObserveProperty (this, p => p.IsDataLoaded);
			}
		}
	
		protected virtual Task<LoadDataResult> DoLoadData ()
		{
			return Task.FromResult (new LoadDataResult ());
		}

		protected async Task ShowMessage (string title, string message,string cancel)
		{
			await BaseApp.CurrentApp.PostForegroundAction(async () => await Page.DisplayAlert (title, message, cancel));
		}

		protected async Task<bool> AskQuestion (string title, string message,string yes,string no)
		{
			return await BaseApp.CurrentApp.PostForegroundAction(async () => await Page.DisplayAlert (title, message, yes, no));
		}

		protected async Task<string> ShowActionSheet(string title, string cancel, string destroy, params string[] buttons)
		{
			return await BaseApp.CurrentApp.PostForegroundAction(async () =>await Page.DisplayActionSheet (title, cancel, destroy, buttons));
		}



		public async Task<T> WebSafeCall<T>(Func<Task<T>> action,bool showPopoup = true){
			return await WebSafeCall<T> (action,null, null,null, showPopoup);
		}

		public async Task<T> WebSafeCall<T>(Func<Task<T>> action,string title,string webErrorMessage,string webTimeoutMessage, bool showPopoup = true){
            bool error = false;
            try{
				return await action.Invoke();
			}
			catch(WebException ex){
				Log.LogException (ex);
               	if (showPopoup) {
					await ShowMessage(title ?? LocalizationManager.Instance.GetText ("AppName"), webErrorMessage ?? LocalizationManager.Instance.GetText ("NoInternetConnectionTitle"), LocalizationManager.Instance.GetText("OK"));   
				}
			}
			catch (TaskCanceledException ex){
				Log.LogException (ex);
				if (showPopoup) {
					await ShowMessage(title ?? LocalizationManager.Instance.GetText ("AppName"), webTimeoutMessage ?? LocalizationManager.Instance.GetText ("InternetConnectionTimeout"), LocalizationManager.Instance.GetText("OK"));   
				}
			}
			catch (Exception ex)
			{
				Log.LogException(ex);
				if (showPopoup)
				{
					await ShowMessage(title ?? LocalizationManager.Instance.GetText("AppName"), webErrorMessage ?? LocalizationManager.Instance.GetText("InternetConnectionGeneralError"), LocalizationManager.Instance.GetText("OK"));
				}
			}

            return default(T);
		}
	}

}

