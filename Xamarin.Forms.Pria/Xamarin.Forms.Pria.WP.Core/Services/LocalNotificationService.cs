﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Pria.WP.Services;

[assembly: Dependency(typeof(LocalNotificationService))]
namespace Xamarin.Forms.Pria.WP.Services
{
    public class LocalNotificationService : ILocalNotificationService
    {

        public void RequestLocalNotifications()
        {
        }

        public void RequestRemoteNotifications()
        {
         
        }

        public void Notify(string title, string text, string action, string userData = null, string soundName = null)
        {
            ShellToast toast = new ShellToast();
            toast.Content = text;
            toast.Title = title;
            toast.Show();
        }
    }
}
