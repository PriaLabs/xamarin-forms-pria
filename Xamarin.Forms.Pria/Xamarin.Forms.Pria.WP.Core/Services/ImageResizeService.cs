﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Xamarin.Forms;
using Xamarin.Forms.Pria.WP.Core.Services;

[assembly: Dependency(typeof(ImageResizeService))]
namespace Xamarin.Forms.Pria.WP.Core.Services
{
    public class ImageResizeService : IImageResizeService
    {
        public Stream ResizeImage(MediaFile mediaFile, int maxPixelDimension)
        {
            int newWidth, newHeight;
            MediaUtils.ResizeBasedOnPixelDimension(maxPixelDimension, mediaFile.Exif.Width, mediaFile.Exif.Height, out newWidth, out newHeight);

            MemoryStream ms = new MemoryStream();
            BitmapImage bmp = new BitmapImage();
            bmp.SetSource(mediaFile.Source);
            WriteableBitmap writeableBitmap = new WriteableBitmap(bmp);
           
            Extensions.SaveJpeg(writeableBitmap, ms, newWidth, newHeight, 0, 100);

            ms.Seek(0, SeekOrigin.Begin);
           
            return ms;
        }
    }
}
