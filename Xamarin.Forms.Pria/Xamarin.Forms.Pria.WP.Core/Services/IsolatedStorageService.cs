﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Services;
using Xamarin.Forms.Pria.WP.Services;

[assembly: Dependency(typeof(IsolatedStorageService))]
namespace Xamarin.Forms.Pria.WP.Services
{
    public class IsolatedStorageService:IIsolatedStorage
    {
        IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication();
        public System.IO.Stream Create(string file)
        {
            return storage.CreateFile(file);
        }

        public System.IO.Stream OpenWrite(string file)
        {
            return storage.OpenFile(file, FileMode.OpenOrCreate, FileAccess.Write);
        }

        public System.IO.Stream OpenRead(string file)
        {
            return storage.OpenFile(file, FileMode.Open, FileAccess.Read);
        }

        public string[] GetNames(string folder)
        {

            return storage.GetFileNames();
        }

        public bool Exists(string file)
        {
            return storage.GetFileNames().Contains(file);
        }

        public bool Delete(string file)
        {
            try
            {
                storage.DeleteFile(file);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }


        public Stream OpenReadFullPath(string path)
        {
            throw new NotImplementedException();
        }

        public string GetFullPath(string file)
        {
            throw new NotImplementedException();
        }
    }
}
