﻿using System;

#if __UNIFIED__
using Foundation;
using UIKit;
using CoreGraphics;
#else
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
#endif

using MonoTouch.FBAudienceNetwork;

namespace FBAudienceNetworkSample
{
	public partial class FBAudienceNetworkSampleViewController : UIViewController, IFBAdViewDelegate, IFBInterstitialAdDelegate
	{
		public FBAudienceNetworkSampleViewController (IntPtr handle) : base (handle)
		{
		}

		FBAdView adView;
		FBInterstitialAd interstitialAd;
		const string YourPlacementId = "YOUR_PLACEMENT_ID";

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Create a banner's ad view with a unique placement ID (generate your own on the Facebook app settings).
			// Use different ID for each ad placement in your app.
			adView = new FBAdView (YourPlacementId, FBAdSizes.BannerHeight50, this) {
				Delegate = this
			};

			// When testing on a device, add its hashed ID to force test ads.
			// The hash ID is printed to console when running on a device.
//			FBAdSettings.AddTestDevice ("THE HASHED ID AS PRINTED TO CONSOLE");

			// Initiate a request to load an ad.
			adView.LoadAd ();

			// Reposition the adView to the bottom of the screen
			var viewSize = View.Bounds.Size;
			var bottomAlignedY = viewSize.Height - FBAdSizes.BannerHeight50.Size.Height;

			#if __UNIFIED__
			adView.Frame = new CGRect (0, bottomAlignedY, viewSize.Width, FBAdSizes.BannerHeight50.Size.Height);
			#else
			adView.Frame = new RectangleF (0, bottomAlignedY, viewSize.Width, FBAdSizes.BannerHeight50.Size.Height);
			#endif


			// Add adView to the view hierarchy.
			View.AddSubview (adView);
		}

		partial void btnIntersitial_TouchUpInside (UIButton sender)
		{
			lblInterstitalStatusLabel.Text = "Loading interstitial ad...";

			// Create the interstitial unit with a placement ID (generate your own on the Facebook app settings).
			// Use different ID for each ad placement in your app.
			interstitialAd = new FBInterstitialAd (YourPlacementId) {
				Delegate = this
			};

			interstitialAd.LoadAd ();
		}

		partial void btnAudience_TouchUpInside (UIButton sender)
		{
			if (interstitialAd == null || !interstitialAd.IsAdValid) {
				// Ad not ready to present.
				lblInterstitalStatusLabel.Text = "Ad not loaded. Click load to request an ad.";
			} else {
				lblInterstitalStatusLabel.Text = string.Empty;

				// Ad is ready, present it!
				interstitialAd.ShowAdFromRootViewController (this);
			}
		}

		#region IFBAdViewDelegate

		[Export ("adViewDidClick:")]
		public void AdViewDidClick (FBAdView adView)
		{
			ShowMessage ("Ad was clicked.");
		}

		[Export ("adViewDidFinishHandlingClick:")]
		public void AdViewDidFinishHandlingClick (FBAdView adView)
		{
			ShowMessage ("Ad did finish click handling.");
		}

		[Export ("adViewDidLoad:")]
		public void AdViewDidLoad (FBAdView adView)
		{
			lblAdViewStatus.Text = string.Empty;
			ShowMessage ("Ad was loaded.");

			// Now that the ad was loaded, show the view in case it was hidden before.
			adView.Hidden = false;
		}

		[Export ("adView:didFailWithError:")]
		public void AdViewDidFail (FBAdView adView, NSError error)
		{
			lblAdViewStatus.Text = "Ad failed to load.";
			ShowMessage ("Ad failed to load with error: " + error.Description);

			// Hide the unit since no ad is shown.
			adView.Hidden = true;
		}

		#endregion

		#region IFBInterstitialAdDelegate

		[Export ("interstitialAdDidLoad:")]
		public void InterstitialAdDidLoad (FBInterstitialAd interstitialAd)
		{
			ShowMessage ("Interstitial ad was loaded. Can present now.");
			lblInterstitalStatusLabel.Text = "Ad loaded. Click show to present!";
		}

		[Export ("interstitialAd:didFailWithError:")]
		public void IntersitialDidFail (FBInterstitialAd interstitialAd, NSError error)
		{
			ShowMessage ("Interstitial failed to load with error: " + error.Description);
			lblInterstitalStatusLabel.Text = "Interstitial ad failed to load.";
		}

		[Export ("interstitialAdDidClick:")]
		public void InterstitialAdDidClick (FBInterstitialAd interstitialAd)
		{
			ShowMessage ("Interstitial was clicked.");
		}

		[Export ("interstitialAdDidClose:")]
		public void InterstitialAdDidClose (FBInterstitialAd interstitialAd)
		{
			ShowMessage ("Interstitial closed.");

			// Optional, Cleaning up.
			interstitialAd = null;
		}

		[Export ("interstitialAdWillClose:")]
		public void InterstitialAdWillClose (FBInterstitialAd interstitialAd)
		{
			ShowMessage ("Interstitial will close.");
		}

		#endregion

		void ShowMessage (string message)
		{
			new UIAlertView ("Hey!", message, null, "Ok", null).Show ();
		}
	}
}

