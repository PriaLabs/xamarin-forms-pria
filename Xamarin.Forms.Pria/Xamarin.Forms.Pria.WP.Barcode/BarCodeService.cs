﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Xamarin.Forms;
using Xamarin.Forms.Pria.Services;
using Xamarin.Forms.Pria.WP.Services;
using ZXing.Mobile;

[assembly: Dependency(typeof(BarCodeService))]
namespace Xamarin.Forms.Pria.WP.Services
{
    public class BarCodeService:IBarcodeService
    {
        public bool IsAvailable()
        {
            return true;
        }

        ManualResetEvent scanEvent;
     
        public async Task<BarcodeResult> Scan(CameraFacing cameraFacing = CameraFacing.Rear)
        {
            MobileBarcodeScanner scanner = new MobileBarcodeScanner(System.Windows.Deployment.Current.Dispatcher);
            
            scanner.UseCustomOverlay = false;
            var result = await scanner.Scan(new MobileBarcodeScanningOptions() { UseFrontCameraIfAvailable = cameraFacing == CameraFacing.Front });
            if (result != null)
            {
                Console.WriteLine("Scanned Barcode: " + result.Text);
                return new BarcodeResult() { Code = result.Text, Type =(BarcodeType) result.BarcodeFormat };
            }
            return null;
        }
    }
}
